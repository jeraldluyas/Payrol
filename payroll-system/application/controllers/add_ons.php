<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Add_ons extends CI_Controller {

	public function __construct() {
        parent::__construct();
        if(!$this->ion_auth->logged_in()) {
            redirect('/auth/login');
        };
        if(!$this->ion_auth->in_group('admin') AND !$this->ion_auth->is_admin()) {
        	$this->session->set_flashdata('title', 'Permission denied');
        	$this->session->set_flashdata('heading', 'Permission Denied!');
        	$this->session->set_flashdata('message', 'You do not have enough permission to view the contents. <a href="'.base_url('/').'">Go back home.</a>');
        	redirect('/error/');
        };
    }

	public function index() {
		$this->load->model('add_ons_model', 'table');

		$data = array(
				'title'             => 'Add-ons List',
				'title_description' => '',
				'item'              => $this->table->get_add_ons(),
				'table'             => 'add_ons'
			);

		$this->load->view('header', $data);
		$this->load->view('add_ons', $data);
		$this->load->view('footer');
	}

	public function add() {
		$data = array(
				'title'             => 'Add Add ons',
				'title_description' => '',
				'table'             => 'add_ons',
				'label'       => array (
				                		'label' => 'Label *: ',
				                		'name'  => 'label',
				                		'value' => ''
				                	),
				'description' => array (
				                		'label' => 'Description: ',
				                		'name'  => 'description',
				                		'value' => ''
				                	),
			);

		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('_forms/add_ons_add', $data);
		$this->load->view('footer');
	}

	public function edit($id = null) {
		if(is_null($id)) {
			redirect('add_ons');
		};

		$this->load->model('add_ons_model', 'table');
		$input = $this->table->get_add_ons();
		$data = array(
				'title'             => 'Edit add_ons',
				'title_description' => '',
				'table'             => 'add_ons',
				'id'          => array (
				                		'name'  => 'id',
				                		'value' => $input[$id]['id']
				                	),
				'label'       => array (
				                		'label' => 'Label *: ',
				                		'name'  => 'label',
				                		'value' => $input[$id]['label']
				                	),
				'description' => array (
				                		'label' => 'Description: ',
				                		'name'  => 'description',
				                		'value' => $input[$id]['value']
				                	),
			);

		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('_forms/add_ons_add', $data);
		$this->load->view('footer');
	}

	public function delete($id = null) {
		if(is_null($id)) {
			redirect('add_ons');
		};

		//check if $id exists
		$this->load->model('add_ons_model', 'table');
		if(!$this->table->get_add_ons_list($id)) {
			redirect('add_ons');
		}
			
		$data = array(
				'title'             => 'Delete add_ons',
				'title_description' => 'are you sure you want to delete this? <br/> this is irreversible.',
				'table'             => 'add_ons',
				'id'          => array (
				                		'name'  => 'id',
				                		'value' => $id
				                	),
			);

		if(($input = $this->input->post())) {
			unset($input['submit']); //remove 'submit'	
			if(isset($input['id'])) {
				$affected_id = $this->table->delete($input);
			};

			if($affected_id) { //if success
				redirect('/add_ons');
			} else { //else if($id)
				$data['message'] = "Deleting Patient Type failed.";

				$this->load->helper('form');
				$this->load->view('header', $data);
				$this->load->view('_forms/add_ons_delete', $data);
				$this->load->view('footer');
			} //end if($id)
		} else {
			$this->load->helper('form');
			$this->load->view('header', $data);
			$this->load->view('_forms/add_ons_delete', $data);
			$this->load->view('footer');
		};
	}

	public function submit() {
		if(!($input = $this->input->post())) {
			redirect('/');
		};	

		unset($input['submit']); //remove 'submit'	

		$this->load->model('add_ons_model', 'table');
		if(isset($input['id'])) {
			$id = $this->table->update($input);
		} else {
			$id = $this->table->add($input);
		};

		if($id != -1) { //if success
			redirect('/add_ons');
		} else { //else if($id)
			$data['message'] = "Adding New Patient Type failed.";
			$data = array(
					'title'             => 'Patient Type',
					'title_description' => '',
					'table'             => 'add_ons',
					'label'       => array (
											'label' => 'Label *: ',
											'name'  => 'label',
											'value' => $input['label']
										),
					'description' => array (
											'label' => 'Description: ',
											'name'  => 'description',
											'value' => $input['value']
										),
				);

			$this->load->helper('form');
			$this->load->view('header', $data);
			$this->load->view('_forms/add_ons_add', $data);
			$this->load->view('footer');
		} //end if($id)
	}


}

/* End of file add_ons.php */
/* Location: ./application/controllers/add_ons.php */