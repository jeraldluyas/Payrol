<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {
        parent::__construct();
        if(!$this->ion_auth->logged_in()) {
            redirect('/auth/login');
        };
        if(!$this->ion_auth->in_group('admin') AND !$this->ion_auth->is_admin()) {
        	$this->session->set_flashdata('title', 'Permission denied');
        	$this->session->set_flashdata('heading', 'Permission Denied!');
        	$this->session->set_flashdata('message', 'You do not have enough permission to view the contents. <a href="'.base_url('/').'">Go back home.</a>');
        	redirect('/error/');
        };
    }

	public function index() {
		
		$data = array(
				'title'             => 'admin List',
				'title_description' => '',
				'item'              => '',
				'table'             => 'admin'
			);

		$this->load->view('header', $data);
		$this->load->view('side_bar', $data);
		$this->load->view('admin', $data);
		$this->load->view('footer');
	}

	public function add() {
		$data = array(
				'title'             => 'Add admin',
				'title_description' => '',
				'table'             => 'admin',
				'label'       => array (
				                		'label' => 'Label *: ',
				                		'name'  => 'label',
				                		'value' => ''
				                	),
				'description' => array (
				                		'label' => 'Description: ',
				                		'name'  => 'description',
				                		'value' => ''
				                	),
			);

		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('_forms/admin_add', $data);
		$this->load->view('footer');
	}

	public function edit($id = null) {
		if(is_null($id)) {
			redirect('admin');
		};

		$this->load->model('admin_model', 'table');
		$input = $this->table->get_admin();
		$data = array(
				'title'             => 'Edit admin',
				'title_description' => '',
				'table'             => 'admin',
				'id'          => array (
				                		'name'  => 'admin_id',
				                		'value' => $input[$id]['admin_id']
				                	),
				'label'       => array (
				                		'label' => 'Label *: ',
				                		'name'  => 'label',
				                		'value' => $input[$id]['label']
				                	),
				'description' => array (
				                		'label' => 'Description: ',
				                		'name'  => 'description',
				                		'value' => $input[$id]['value']
				                	),
			);

		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('_forms/admin_add', $data);
		$this->load->view('footer');
	}

	public function delete($id = null) {
		if(is_null($id)) {
			redirect('admin');
		};

		//check if $id exists
		$this->load->model('admin_model', 'table');
		if(!$this->table->get_admin_list($id)) {
			redirect('admin');
		}
			
		$data = array(
				'title'             => 'Delete admin',
				'title_description' => 'are you sure you want to delete this? <br/> this is irreversible.',
				'table'             => 'admin',
				'id'          => array (
				                		'name'  => 'admin_id',
				                		'value' => $id
				                	),
			);

		if(($input = $this->input->post())) {
			unset($input['submit']); //remove 'submit'	
			if(isset($input['admin_id'])) {
				$affected_id = $this->table->delete($input);
			};

			if($affected_id) { //if success
				redirect('/admin');
			} else { //else if($id)
				$data['message'] = "Deleting Patient Type failed.";

				$this->load->helper('form');
				$this->load->view('header', $data);
				$this->load->view('_forms/admin_delete', $data);
				$this->load->view('footer');
			} //end if($id)
		} else {
			$this->load->helper('form');
			$this->load->view('header', $data);
			$this->load->view('_forms/admin_delete', $data);
			$this->load->view('footer');
		};
	}

	public function submit() {
		if(!($input = $this->input->post())) {
			redirect('/');
		};	

		unset($input['submit']); //remove 'submit'	

		$this->load->model('admin_model', 'table');
		if(isset($input['admin_id'])) {
			$id = $this->table->update($input);
		} else {
			$id = $this->table->add($input);
		};

		if($id != -1) { //if success
			redirect('/admin');
		} else { //else if($id)
			$data['message'] = "Adding New Patient Type failed.";
			$data = array(
					'title'             => 'Patient Type',
					'title_description' => '',
					'table'             => 'admin',
					'label'       => array (
											'label' => 'Label *: ',
											'name'  => 'label',
											'value' => $input['label']
										),
					'description' => array (
											'label' => 'Description: ',
											'name'  => 'description',
											'value' => $input['value']
										),
				);

			$this->load->helper('form');
			$this->load->view('header', $data);
			$this->load->view('_forms/admin_add', $data);
			$this->load->view('footer');
		} //end if($id)
	}


}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */