<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deduction extends CI_Controller {

	public function __construct() {
        parent::__construct();
        if(!$this->ion_auth->logged_in()) {
            redirect('/auth/login');
        };
        if(!$this->ion_auth->in_group('admin') AND !$this->ion_auth->is_admin()) {
        	$this->session->set_flashdata('title', 'Permission denied');
        	$this->session->set_flashdata('heading', 'Permission Denied!');
        	$this->session->set_flashdata('message', 'You do not have enough permission to view the contents. <a href="'.base_url('/').'">Go back home.</a>');
        	redirect('/error/');
        };
    }

	public function index() {
		$this->load->model('deduction_model', 'table');

		$data = array(
				'title'             => 'Deduction List',
				'title_description' => '',
				'item'              => $this->table->get_deduction(),
				'table'             => 'deduction'
			);

		$this->load->view('header', $data);
		$this->load->view('side_bar', $data);
		$this->load->view('deduction', $data);
		$this->load->view('footer');
	}

	public function add() {
		$data = array(
				'title'             => 'Add Deduction',
				'title_description' => '',
				'table'             => 'deduction',
				'label'       => array (
				                		'label' => 'Label *: ',
				                		'name'  => 'label',
				                		'value' => ''
				                	),
				'value' => array (
				                		'label' => 'Value: ',
				                		'name'  => 'value',
				                		'value' => ''
				                	),
				'description' => array (
				                		'label' => 'Description: ',
				                		'name'  => 'description',
				                		'value' => ''
				                	),
			);

		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('_forms/deduction_add', $data);
		$this->load->view('footer');
	}

	public function edit($id = null) {
		if(is_null($id)) {
			redirect('deduction');
		};

		$this->load->model('deduction_model', 'table');
		$input = $this->table->get_deduction();
		$data = array(
				'title'             => 'Edit Deduction',
				'title_description' => '',
				'table'             => 'deduction',
				'id'          => array (
				                		'name'  => 'deduction_id',
				                		'value' => $input[$id]['deduction_id']
				                	),
				'label'       => array (
				                		'label' => 'Label *: ',
				                		'name'  => 'label',
				                		'value' => $input[$id]['label']
				                	),
				'value' => array (
				                		'label' => 'Value: ',
				                		'name'  => 'value',
				                		'value' => $input[$id]['value']
				                	),
				'description' => array (
				                		'label' => 'Description: ',
				                		'name'  => 'description',
				                		'value' => $input[$id]['value']
				                	),
			);

		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('_forms/deduction_add', $data);
		$this->load->view('footer');
	}

	public function delete($id = null) {
		if(is_null($id)) {
			redirect('deduction');
		};

		//check if $id exists
		$this->load->model('deduction_model', 'table');
		if(!$this->table->get_deduction_list($id)) {
			redirect('deduction');
		}
			
		$data = array(
				'title'             => 'Delete Deduction',
				'title_description' => 'are you sure you want to delete this? <br/> this is irreversible.',
				'table'             => 'deduction',
				'id'          => array (
				                		'name'  => 'deduction_id',
				                		'value' => $id
				                	),
			);

		if(($input = $this->input->post())) {
			unset($input['submit']); //remove 'submit'	
			if(isset($input['deduction_id'])) {
				$affected_id = $this->table->delete($input);
			};

			if($affected_id) { //if success
				redirect('/deduction');
			} else { //else if($id)
				$data['message'] = "Deleting Patient Type failed.";

				$this->load->helper('form');
				$this->load->view('header', $data);
				$this->load->view('_forms/deduction_delete', $data);
				$this->load->view('footer');
			} //end if($id)
		} else {
			$this->load->helper('form');
			$this->load->view('header', $data);
			$this->load->view('_forms/deduction_delete', $data);
			$this->load->view('footer');
		};
	}

	public function submit() {
		if(!($input = $this->input->post())) {
			redirect('/');
		};	

		unset($input['submit']); //remove 'submit'	

		$this->load->model('deduction_model', 'table');
		if(isset($input['deduction_id'])) {
			$id = $this->table->update($input);
		} else {
			$id = $this->table->add($input);
		};

		if($id != -1) { //if success
			redirect('/deduction');
		} else { //else if($id)
			$data['message'] = "Adding New Patient Type failed.";
			$data = array(
					'title'             => 'Patient Type',
					'title_description' => '',
					'table'             => 'deduction',
					'label'       => array (
											'label' => 'Label *: ',
											'name'  => 'label',
											'value' => $input['label']
										),
					'value' => array (
											'label' => 'Value: ',
											'name'  => 'value',
											'value' => $input['value']
										),
				);

			$this->load->helper('form');
			$this->load->view('header', $data);
			$this->load->view('_forms/deduction_add', $data);
			$this->load->view('footer');
		} //end if($id)
	}


}

/* End of file deduction.php */
/* Location: ./application/controllers/deduction.php */