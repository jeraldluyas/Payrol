<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department extends CI_Controller {

	public function __construct() {
        parent::__construct();
        if(!$this->ion_auth->logged_in()) {
            redirect('/auth/login');
        };
        if(!$this->ion_auth->in_group('admin') AND !$this->ion_auth->is_admin()) {
        	$this->session->set_flashdata('title', 'Permission denied');
        	$this->session->set_flashdata('heading', 'Permission Denied!');
        	$this->session->set_flashdata('message', 'You do not have enough permission to view the contents. <a href="'.base_url('/').'">Go back home.</a>');
        	redirect('/error/');
        };
    }

	public function index() {
		$this->load->model('department_model', 'table');

		$data = array(
				'title'             => 'department List',
				'title_description' => '',
				'item'              => $this->table->get_department(),
				'table'             => 'department'
			);

		$this->load->view('header', $data);
		$this->load->view('side_bar', $data);
		$this->load->view('department', $data);
		$this->load->view('footer');
	}

	public function add() {
		$data = array(
				'title'             => 'Add Department',
				'title_description' => '',
				'table'             => 'department',
				'label'       => array (
				                		'label' => 'Label *: ',
				                		'name'  => 'label',
				                		'value' => ''
				                	),
				'description' => array (
				                		'label' => 'Description: ',
				                		'name'  => 'description',
				                		'value' => ''
				                	),
			);

		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('_forms/department_add', $data);
		$this->load->view('footer');
	}

	public function edit($id = null) {
		if(is_null($id)) {
			redirect('department');
		};

		$this->load->model('department_model', 'table');
		$input = $this->table->get_department();
		$data = array(
				'title'             => 'Edit Department',
				'title_description' => '',
				'table'             => 'department',
				'id'          => array (
				                		'name'  => 'id',
				                		'value' => $input[$id]['id']
				                	),
				'label'       => array (
				                		'label' => 'Label *: ',
				                		'name'  => 'label',
				                		'value' => $input[$id]['label']
				                	),
				'description' => array (
				                		'label' => 'Description: ',
				                		'name'  => 'description',
				                		'value' => $input[$id]['description']
				                	),
			);

		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('_forms/department_add', $data);
		$this->load->view('footer');
	}

	public function delete($id = null) {
		if(is_null($id)) {
			redirect('department');
		};

		// //check if $id exists
		 $this->load->model('department_model', 'table');
		// if(!$this->table->get_department_list($id)) {
		// 	redirect('department');
		// }
			
		// $data = array(
		// 		'title'             => 'Delete department',
		// 		'title_description' => 'are you sure you want to delete this? <br/> this is irreversible.',
		// 		'table'             => 'department',
		// 		'id'          => array (
		// 		                		'name'  => 'department_id',
		// 		                		'value' => $id
		// 		                	),
		// 	);

		if((!is_null($id))) {
			//unset($input['submit']); //remove 'submit'	
			if(isset($id)) {
				$data = array('id' => $id, );
				$affected_id = $this->table->delete($data);
			};

			if($affected_id) { //if success
				redirect('/department');
			} else { //else if($id)

				$data['message'] = "Deleting Patient Type failed.";

				$this->load->helper('form');
				$this->load->view('header', $data);
				$this->load->view('_forms/department_delete', $data);
				$this->load->view('footer');
			} //end if($id)
		} else {
			redirect('/department');
			$this->load->helper('form');
			$this->load->view('header', $data);
			$this->load->view('_forms/department_delete', $data);
			$this->load->view('footer');
		};
	}

	public function submit() {
		if(!($input = $this->input->post())) {
			redirect('/');
		};	

		unset($input['submit']); //remove 'submit'	

		$this->load->model('department_model', 'table');
		if(isset($input['id'])) {
			$id = $this->table->update($input);
		} else {
			$id = $this->table->add($input);
		};

		if($id != -1) { //if success
			redirect('/department');
		} else { //else if($id)
			$data['message'] = "Adding New Patient Type failed.";
			$data = array(
					'title'             => 'Patient Type',
					'title_description' => '',
					'table'             => 'department',
					'label'       => array (
											'label' => 'Label *: ',
											'name'  => 'label',
											'value' => $input['label']
										),
					'description' => array (
											'label' => 'Description: ',
											'name'  => 'description',
											'value' => $input['value']
										),
				);

			$this->load->helper('form');
			$this->load->view('header', $data);
			$this->load->view('_forms/department_add', $data);
			$this->load->view('footer');
		} //end if($id)
	}


}

/* End of file department.php */
/* Location: ./application/controllers/department.php */