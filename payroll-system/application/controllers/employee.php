<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee extends CI_Controller {

	public function __construct() {
        parent::__construct();
        if(!$this->ion_auth->logged_in()) {
            redirect('/auth/login');
        };

        // if(!$this->ion_auth->in_group('admin') AND !$this->ion_auth->is_admin()) {
        // 	$this->session->set_flashdata('title', 'Permission denied');
        // 	$this->session->set_flashdata('heading', 'Permission Denied!');
        // 	$this->session->set_flashdata('message', 'You do not have enough permission to view the contents. <a href="'.base_url('/').'">Go back home.</a>');
        // 	redirect('/error/');
        // };
    }

	public function index() {
		$this->load->model('employee_model', 'table');

		$this->load->model('department_model', 'department');

		$this->load->model('salary_grade_model', 'salary_grade');

		$data = array(
				'title'             => 'Employee',
				'title_description' => '',
				'item'              => $this->table->get_employee(),
				'table'             => 'employee'
			);
// $start_date = new DateTime('2007-09-01 08:10:58');
// $since_start = $start_date->diff(new DateTime('2012-09-02 00:25:00'));
// echo $since_start->days.' days total<br>';
// echo $since_start->y.' years<br>';
// echo $since_start->m.' months<br>';
// echo $since_start->d.' days<br>';
// echo $since_start->h.' hours<br>';
// echo $since_start->i.' minutes<br>';
// echo $since_start->s.' seconds<br>';
// exit();
		$data['department_list']=$this->department->get_department_list();
		$data['salary_grade_list']=$this->salary_grade->get_salary_grade_list();
		$this->load->view('header', $data);
		$this->load->view('side_bar');
		$this->load->view('employee', $data);
		$this->load->view('footer');
	}

	public function search(){
		$this->load->model('employee_model', 'table');
		$data = array(
				'title'             => 'Employee List',
				'title_description' => '',
				'table'             => 'employee',
				'first_name'       => array (
				                		'label' => 'First Name *: ',
				                		'name'  => 'first_name',
				                		'value' => ''
				                	),
				'last_name' => array (
				                		'label' => 'Last Name: ',
				                		'name'  => 'last_name',
				                		'value' => ''
				                	),
				'position' => array (
				                		'label' => 'Position : ',
				                		'name'  => 'position',
				                		'value' => ''
				                	),
				'salary_grade' => array (
				                		'label' => 'Salary Grade : ',
				                		'name'  => 'salary_grade_id',
				                		'value' => ''
				                	),
				'department' => array (
				                		'label' => 'Department : ',
				                		'name'  => 'department_id',
				                		'value' => ''
				                	),
				'employee_id' => array (
				                		'label' => 'Employee ID : ',
				                		'name'  => 'employee_id',
				                		'value' => ''
				                	),
			);
		$data['item'] = $this->table->get_employee();
		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('employee_search', $data);
		$this->load->view('footer');
	}
	public function add() {
		$this->load->model('employee_model', 'employee');
		$emp_id = $this->employee->get_new_id();
		$data = array(
				'title'             => 'Add Employee',
				'title_description' => '',
				'table'             => 'employee',
				
				'first_name'       => array (
				                		'label' => 'First Name *: ',
				                		'name'  => 'first_name',
				                		'value' => ''
				                	),
				'last_name' => array (
				                		'label' => 'Last Name: ',
				                		'name'  => 'last_name',
				                		'value' => ''
				                	),
				'position' => array (
				                		'label' => 'Position : ',
				                		'name'  => 'position',
				                		'value' => ''
				                	),
				'salary_grade' => array (
				                		'label' => 'Salary Grade : ',
				                		'name'  => 'salary_grade_id',
				                		'value' => ''
				                	),
				'department' => array (
				                		'label' => 'Department : ',
				                		'name'  => 'department_id',
				                		'value' => ''
				                	),
				'employee_id' => array (
				                		'label' => 'Employee ID : ',
				                		'name'  => 'employee_id',
				                		'value' => $emp_id
				                	),
				'birthdate' => array (
				                		'label' => 'Date of birthday : ',
				                		'name'  => 'birthdate',
				                		'value' => ''
				                	),
				'city' => array (
				                		'label' => 'City : ',
				                		'name'  => 'city',
				                		'value' => ''
				                	),
				'mobile' => array (
				                		'label' => 'Mobile Number : ',
				                		'name'  => 'mobile',
				                		'value' => ''
				                	),
				'phone' => array (
				                		'label' => 'Phone Number : ',
				                		'name'  => 'phone',
				                		'value' => ''
				                	),
				'email' => array (
				                		'label' => 'Email Address : ',
				                		'name'  => 'email',
				                		'value' => ''
				                	),
				'civil_status' => array (
				                		'label' => 'Civil Status : ',
				                		'name'  => 'civil_status',
				                		'value' => ''
				                	),
				'sss_no' => array (
				                		'label' => 'SSS Number : ',
				                		'name'  => 'sss_no',
				                		'value' => ''
				                	),
				'philhealth_no' => array (
				                		'label' => 'Philhealth Number : ',
				                		'name'  => 'philhealth_no',
				                		'value' => ''
				                	),
				'pagibig_no' => array (
				                		'label' => 'Pagibig Number : ',
				                		'name'  => 'pagibig_no',
				                		'value' => ''
				                	),
				'tax_id_no' => array (
				                		'label' => 'Tax ID Number : ',
				                		'name'  => 'tax_id_no',
				                		'value' => ''
				                	),
				'birth_cert' => array (
				                		'label' => 'Birth Certificate : ',
				                		'name'  => 'birth_cert',
				                		'value' => ''
				                	),
				'baptismal' => array (
				                		'label' => 'Baptismal : ',
				                		'name'  => 'baptismal',
				                		'value' => ''
				                	),
				'marriage_cert' => array (
				                		'label' => 'Marriage Cert : ',
				                		'name'  => 'marriage_cert',
				                		'value' => ''
				                	),
					
				'brgy_clearance' => array (
				                		'label' => 'Brgy Clearance : ',
				                		'name'  => 'brgy_clearance',
				                		'value' => ''
				                	),
				'police_clearance' => array (
				                		'label' => 'Police Clearance : ',
				                		'name'  => 'police_clearance',
				                		'value' => ''
				                	),
				'nbi' => array (
				                		'label' => 'NBI : ',
				                		'name'  => 'nbi',
				                		'value' => ''
				                	),
				'xray' => array (
				                		'label' => 'Xray : ',
				                		'name'  => 'xray',
				                		'value' => ''
				                	),
				'result' => array (
				                		'label' => 'Result : ',
				                		'name'  => 'result',
				                		'value' => ''
				                	),
				'sputum' => array (
				                		'label' => 'Sputum : ',
				                		'name'  => 'sputum',
				                		'value' => ''
				                	),
				'fecalysis' => array (
				                		'label' => 'Fecalysis : ',
				                		'name'  => 'fecalysis',
				                		'value' => ''
				                	),
				'health_card' => array (
				                		'label' => 'Health Card : ',
				                		'name'  => 'health_card',
				                		'value' => ''
				                	),
				'drug_test' => array (
				                		'label' => 'Drug Test : ',
				                		'name'  => 'drug_test',
				                		'value' => ''
				                	),
				'eye_test' => array (
				                		'label' => 'Eye Test : ',
				                		'name'  => 'eye_test',
				                		'value' => ''
				                	),
				'bio_date' => array (
				                		'label' => 'BIO Date : ',
				                		'name'  => 'bio_date',
				                		'value' => ''
				                	),
				'resume' => array (
				                		'label' => 'BIO Date : ',
				                		'name'  => 'resume',
				                		'value' => ''
				                	),
				'id_picture' => array (
				                		'label' => 'ID Picture : ',
				                		'name'  => 'id_picture',
				                		'value' => ''
				                	),

				'info_sheet' => array (
				                		'label' => 'Info Sheet : ',
				                		'name'  => 'info_sheet',
				                		'value' => ''
				                	),
				'contract' => array (
				                		'label' => 'Contract : ',
				                		'name'  => 'contract',
				                		'value' => ''
				                	),
				'date_hired' => array (
				                		'label' => 'Date Hired : ',
				                		'name'  => 'date_hired',
				                		'value' => ''
				                	),
			);
		$data['civil_status_list'] = array(
			'1' => 'Single',
			'2' => 'Marriage',
			'3' => 'Divorced'
			);
		$this->load->model('department_model', 'department');
		$this->load->model('salary_grade_model', 'salary_grade');
		$data['department_list']=$this->department->get_department_list();
		$data['salary_grade_list']=$this->salary_grade->get_salary_grade_list();
		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('side_bar', $data);
		$this->load->view('_forms/employee_add', $data);
		$this->load->view('footer');
	}

	public function edit($id = null) {
		if(is_null($id)) {
			redirect('employee');
		};
		$this->load->model('employee_model', 'table');
		$this->load->model('department_model', 'department');
		$this->load->model('salary_grade_model', 'salary_grade');
		$input = $this->table->get_employee();
		$data = array(
				'title'             => 'Edit Employee',
				'title_description' => '',
				'table'             => 'employee',
				'id'          => array (
				                		'name'  => 'id',
				                		'value' => $input[$id]['id']

				                	),
				'first_name'       => array (
				                		'label' => 'First Name *: ',
				                		'name'  => 'first_name',
				                		'value' => $input[$id]['first_name']
				                	),
				'last_name' => array (
				                		'label' => 'Last Name: ',
				                		'name'  => 'last_name',
				                		'value' => $input[$id]['last_name']
				                	),
				'position' => array (
				                		'label' => 'Position : ',
				                		'name'  => 'position',
				                		'value' => $input[$id]['position']
				                	),
				'salary_grade' => array (
				                		'label' => 'Salary Grade : ',
				                		'name'  => 'salary_grade_id',
				                		'value' => $input[$id]['salary_grade_id']
				                	),
				'department' => array (
				                		'label' => 'Department : ',
				                		'name'  => 'department_id',
				                		'value' => $input[$id]['department_id']
				                	),
				'employee_id' => array (
									'label' => 'Employee ID : ',
									'name'  => 'employee_id',
									'value' => $input[$id]['employee_id']
								),
				'birthdate' => array (
				                		'label' => 'Date of birthday : ',
				                		'name'  => 'birthdate',
				                		'value' => $input[$id]['birthdate']
				                	),
				'city' => array (
				                		'label' => 'City : ',
				                		'name'  => 'city',
				                		'value' => $input[$id]['city']
				                	),
				'mobile' => array (
				                		'label' => 'Mobile Number : ',
				                		'name'  => 'mobile',
				                		'value' => $input[$id]['mobile']
				                	),
				'phone' => array (
				                		'label' => 'Phone Number : ',
				                		'name'  => 'phone',
				                		'value' => $input[$id]['phone']
				                	),
				'email' => array (
				                		'label' => 'Email Address : ',
				                		'name'  => 'email',
				                		'value' => $input[$id]['email']
				                	),
				'civil_status' => array (
				                		'label' => 'Civil Status : ',
				                		'name'  => 'civil_status',
				                		'value' => $input[$id]['civil_status']
				                	),
				'sss_no' => array (
				                		'label' => 'SSS Number : ',
				                		'name'  => 'sss_no',
				                		'value' => $input[$id]['sss_no']
				                	),
				'philhealth_no' => array (
				                		'label' => 'Philhealth Number : ',
				                		'name'  => 'philhealth_no',
				                		'value' => $input[$id]['philhealth_no']
				                	),
				'pagibig_no' => array (
				                		'label' => 'Pagibig Number : ',
				                		'name'  => 'pagibig_no',
				                		'value' => $input[$id]['pagibig_no']
				                	),
				'tax_id_no' => array (
				                		'label' => 'Tax ID Number : ',
				                		'name'  => 'tax_id_no',
				                		'value' => $input[$id]['tax_id_no']
				                	),
				'date_hired' => array (
				                		'label' => 'Date Hired : ',
				                		'name'  => 'date_hired',
				                		'value' => $input[$id]['date_hired']
				                	),
				'birth_cert' => array (
				                		'label' => 'Birth Certificate : ',
				                		'name'  => 'birth_cert',
				                		'value' => $input[$id]['birth_cert']
				                	),
				'baptismal' => array (
				                		'label' => 'Baptismal : ',
				                		'name'  => 'baptismal',
				                		'value' => $input[$id]['baptismal']
				                	),
				'marriage_cert' => array (
				                		'label' => 'Marriage Cert : ',
				                		'name'  => 'marriage_cert',
				                		'value' => $input[$id]['marriage_cert']
				                	),
				'brgy_clearance' => array (
				                		'label' => 'Brgy Clearance : ',
				                		'name'  => 'brgy_clearance',
				                		'value' => $input[$id]['brgy_clearance']
				                	),
				'police_clearance' => array (
				                		'label' => 'Police Clearance : ',
				                		'name'  => 'police_clearance',
				                		'value' => $input[$id]['police_clearance']
				                	),
				'nbi' => array (
				                		'label' => 'NBI : ',
				                		'name'  => 'nbi',
				                		'value' => $input[$id]['nbi']
				                	),
				'xray' => array (
				                		'label' => 'Xray : ',
				                		'name'  => 'xray',
				                		'value' => $input[$id]['xray']
				                	),
				'result' => array (
				                		'label' => 'Result : ',
				                		'name'  => 'result',
				                		'value' => $input[$id]['result']
				                	),
				'sputum' => array (
				                		'label' => 'Sputum : ',
				                		'name'  => 'sputum',
				                		'value' => $input[$id]['sputum']
				                	),
				'fecalysis' => array (
				                		'label' => 'Fecalysis : ',
				                		'name'  => 'fecalysis',
				                		'value' => $input[$id]['fecalysis']
				                	),
				'health_card' => array (
				                		'label' => 'Health Card : ',
				                		'name'  => 'health_card',
				                		'value' => $input[$id]['health_card']
				                	),
				'drug_test' => array (
				                		'label' => 'Drug Test : ',
				                		'name'  => 'drug_test',
				                		'value' => $input[$id]['drug_test']
				                	),
				'eye_test' => array (
				                		'label' => 'Eye Test : ',
				                		'name'  => 'eye_test',
				                		'value' => $input[$id]['eye_test']
				                	),
				'bio_date' => array (
				                		'label' => 'BIO Date : ',
				                		'name'  => 'bio_date',
				                		'value' => $input[$id]['bio_date']
				                	),
				'resume' => array (
				                		'label' => 'BIO Date : ',
				                		'name'  => 'resume',
				                		'value' => $input[$id]['resume']
				                	),
				'id_picture' => array (
				                		'label' => 'ID Picture : ',
				                		'name'  => 'id_picture',
				                		'value' => $input[$id]['id_picture']
				                	),

				'info_sheet' => array (
				                		'label' => 'Info Sheet : ',
				                		'name'  => 'info_sheet',
				                		'value' => $input[$id]['info_sheet']
				                	),
				'contract' => array (
				                		'label' => 'Contract : ',
				                		'name'  => 'contract',
				                		'value' => $input[$id]['contract']
				                	),				

			);
		$data['civil_status_list'] = array(
			'1' => 'Single',
			'2' => 'Marriage',
			'3' => 'Divorced'
			);
		$data['department_list']=$this->department->get_department_list();
		$data['salary_grade_list']=$this->salary_grade->get_salary_grade_list();
		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('side_bar', $data);
		$this->load->view('_forms/employee_add', $data);
		$this->load->view('footer');
	}
	public function view($id = null) {
		if(is_null($id)) {
			redirect('employee');
		};
		$this->load->model('reports_model', 'reports');
		$this->load->model('employee_model', 'table');
		$this->load->model('department_model', 'department');
		$this->load->model('salary_grade_model', 'salary_grade');
		$this->load->model('work_on_model', 'work_on');
		$input = $this->table->get_employee();
		$data = array(
				'title'             => 'Employee Detail',
				'title_description' => '',
				'table'             => 'employee',
				'id'          => array (
				                		'name'  => 'id',
				                		'value' => $input[$id]['id']

				                	),
				'first_name'       => array (
				                		'label' => 'First Name *: ',
				                		'name'  => 'first_name',
				                		'value' => $input[$id]['first_name']
				                	),
				'last_name' => array (
				                		'label' => 'Last Name: ',
				                		'name'  => 'last_name',
				                		'value' => $input[$id]['last_name']
				                	),
				'position' => array (
				                		'label' => 'Position : ',
				                		'name'  => 'position',
				                		'value' => $input[$id]['position']
				                	),
				'salary_grade' => array (
				                		'label' => 'Salary Grade : ',
				                		'name'  => 'salary_grade_id',
				                		'value' => $input[$id]['salary_grade_id']
				                	),
				'department' => array (
				                		'label' => 'Department : ',
				                		'name'  => 'department_id',
				                		'value' => $input[$id]['department_id']
				                	),
				'employee_id' => array (
				                		'label' => 'Employee ID : ',
				                		'name'  => 'employee_id',
				                		'value' => $input[$id]['employee_id']
				                	),
				'birthdate' => array (
				                		'label' => 'Date of birthday : ',
				                		'name'  => 'birthdate',
				                		'value' => $input[$id]['birthdate']
				                	),
				'city' => array (
				                		'label' => 'City : ',
				                		'name'  => 'city',
				                		'value' => $input[$id]['city']
				                	),
				'mobile' => array (
				                		'label' => 'Mobile Number : ',
				                		'name'  => 'mobile',
				                		'value' => $input[$id]['mobile']
				                	),
				'phone' => array (
				                		'label' => 'Phone Number : ',
				                		'name'  => 'phone',
				                		'value' => $input[$id]['phone']
				                	),
				'email' => array (
				                		'label' => 'Email Address : ',
				                		'name'  => 'email',
				                		'value' => $input[$id]['email']
				                	),
				'civil_status' => array (
				                		'label' => 'Civil Status : ',
				                		'name'  => 'civil_status',
				                		'value' => $input[$id]['civil_status']
				                	),
				'sss_no' => array (
				                		'label' => 'SSS Number : ',
				                		'name'  => 'sss_no',
				                		'value' => $input[$id]['sss_no']
				                	),
				'philhealth_no' => array (
				                		'label' => 'Philhealth Number : ',
				                		'name'  => 'philhealth_no',
				                		'value' => $input[$id]['philhealth_no']
				                	),
				'pagibig_no' => array (
				                		'label' => 'Pagibig Number : ',
				                		'name'  => 'pagibig_no',
				                		'value' => $input[$id]['pagibig_no']
				                	),
				'tax_id_no' => array (
				                		'label' => 'Tax ID Number : ',
				                		'name'  => 'tax_id_no',
				                		'value' => $input[$id]['tax_id_no']
				                	),
				'date_hired' => array (
				                		'label' => 'Date Hired : ',
				                		'name'  => 'date_hired',
				                		'value' => $input[$id]['date_hired']
				                	),
				'birth_cert' => array (
				                		'label' => 'Birth Certificate : ',
				                		'name'  => 'birth_cert',
				                		'value' => $input[$id]['birth_cert']
				                	),
				'baptismal' => array (
				                		'label' => 'Baptismal : ',
				                		'name'  => 'baptismal',
				                		'value' => $input[$id]['baptismal']
				                	),
				'marriage_cert' => array (
				                		'label' => 'Marriage Cert : ',
				                		'name'  => 'marriage_cert',
				                		'value' => $input[$id]['marriage_cert']
				                	),
				'brgy_clearance' => array (
				                		'label' => 'Brgy Clearance : ',
				                		'name'  => 'brgy_clearance',
				                		'value' => $input[$id]['brgy_clearance']
				                	),
				'police_clearance' => array (
				                		'label' => 'Police Clearance : ',
				                		'name'  => 'police_clearance',
				                		'value' => $input[$id]['police_clearance']
				                	),
				'nbi' => array (
				                		'label' => 'NBI : ',
				                		'name'  => 'nbi',
				                		'value' => $input[$id]['nbi']
				                	),
				'xray' => array (
				                		'label' => 'Xray : ',
				                		'name'  => 'xray',
				                		'value' => $input[$id]['xray']
				                	),
				'result' => array (
				                		'label' => 'Result : ',
				                		'name'  => 'result',
				                		'value' => $input[$id]['result']
				                	),
				'sputum' => array (
				                		'label' => 'Sputum : ',
				                		'name'  => 'sputum',
				                		'value' => $input[$id]['sputum']
				                	),
				'fecalysis' => array (
				                		'label' => 'Fecalysis : ',
				                		'name'  => 'fecalysis',
				                		'value' => $input[$id]['fecalysis']
				                	),
				'health_card' => array (
				                		'label' => 'Health Card : ',
				                		'name'  => 'health_card',
				                		'value' => $input[$id]['health_card']
				                	),
				'drug_test' => array (
				                		'label' => 'Drug Test : ',
				                		'name'  => 'drug_test',
				                		'value' => $input[$id]['drug_test']
				                	),
				'eye_test' => array (
				                		'label' => 'Eye Test : ',
				                		'name'  => 'eye_test',
				                		'value' => $input[$id]['eye_test']
				                	),
				'bio_date' => array (
				                		'label' => 'BIO Date : ',
				                		'name'  => 'bio_date',
				                		'value' => $input[$id]['bio_date']
				                	),
				'resume' => array (
				                		'label' => 'BIO Date : ',
				                		'name'  => 'resume',
				                		'value' => $input[$id]['resume']
				                	),
				'id_picture' => array (
				                		'label' => 'ID Picture : ',
				                		'name'  => 'id_picture',
				                		'value' => $input[$id]['id_picture']
				                	),

				'info_sheet' => array (
				                		'label' => 'Info Sheet : ',
				                		'name'  => 'info_sheet',
				                		'value' => $input[$id]['info_sheet']
				                	),
				'contract' => array (
				                		'label' => 'Contract : ',
				                		'name'  => 'contract',
				                		'value' => $input[$id]['contract']
				                	),

			);
		$data['report_attendances'] = $this->reports->get_reports_for_day($input[$id]['id'], $input[$id]['salary_grade_id']);
		$data['payslips'] = $this->reports->get_payslip($input[$id]['id']);
		$data['work_on'] = $this->work_on->get_work_on_list();

		$data['department_list']=$this->department->get_department_list();
		$data['salary_grade_list']=$this->salary_grade->get_salary_grade_list();
		$data['salary_grade_detail']=$this->salary_grade->get_salary_grade($input[$id]['salary_grade_id']);
		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('side_bar', $data);
		$this->load->view('employee_view', $data);
		$this->load->view('footer');
	}
	public function salary_detail($id) {

		$this->load->model('reports_model', 'reports');
		$this->load->model('employee_model', 'table');
		$this->load->model('department_model', 'department');
		$this->load->model('salary_grade_model', 'salary_grade');
		$this->load->model('work_on_model', 'work_on');
		$payslip = $this->reports->get_salary_details($id);
		$data['payslip'] = $payslip[$id];
		$input = $this->table->get_employee($data['payslip']['emp_id']);
		
		$data = array(
				'title'             => 'Salary Detail',
				'title_description' => '',
				'table'             => 'employee',
				'id'          => array (
				                		'name'  => 'id',
				                		'value' => $input[$data['payslip']['emp_id']]['id']

				                	),
				'first_name'       => array (
				                		'label' => 'First Name *: ',
				                		'name'  => 'first_name',
				                		'value' => $input[$data['payslip']['emp_id']]['first_name']
				                	),
				'last_name' => array (
				                		'label' => 'Last Name: ',
				                		'name'  => 'last_name',
				                		'value' => $input[$data['payslip']['emp_id']]['last_name']
				                	),
				'position' => array (
				                		'label' => 'Position : ',
				                		'name'  => 'position',
				                		'value' => $input[$data['payslip']['emp_id']]['position']
				                	),
				'salary_grade' => array (
				                		'label' => 'Salary Grade : ',
				                		'name'  => 'salary_grade_id',
				                		'value' => $input[$data['payslip']['emp_id']]['salary_grade_id']
				                	),
				'department' => array (
				                		'label' => 'Department : ',
				                		'name'  => 'department_id',
				                		'value' => $input[$data['payslip']['emp_id']]['department_id']
				                	),
				'employee_id' => array (
				                		'label' => 'Employee ID : ',
				                		'name'  => 'employee_id',
				                		'value' => $input[$data['payslip']['emp_id']]['employee_id']
				                	),
			);
		$data['work_on'] = $this->work_on->get_work_on_list();

		$data['payslip'] = $payslip[$id];
		$data['department_list']=$this->department->get_department_list();
		$data['salary_grade_list']=$this->salary_grade->get_salary_grade_list();
		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('side_bar', $data);
		$this->load->view('employee_salary', $data);
		$this->load->view('footer');	
	}
	public function updateStatus($id) {
		if(is_null($id)) {
			redirect('employee');
		};
	}
	public function delete($id = null) {
		if(is_null($id)) {
			redirect('employee');
		};

		//check if $id exists
		$this->load->model('employee_model', 'table');
		if(!$this->table->get_employee_list($id)) {
			redirect('employee');
		}
			
		$data = array(
				'title'             => 'Delete Employee',
				'title_description' => 'are you sure you want to delete this? <br/> this is irreversible.',
				'table'             => 'employee',
				'id'          => array (
				                		'name'  => 'id',
				                		'value' => $id
				                	),
			);

		if(($input = $this->input->post())) {
			unset($input['submit']); //remove 'submit'
			if(isset($input['id'])) {
				$affected_id = $this->table->update_status($input);
				redirect('/employee');
			};

			if($affected_id) { //if success
				redirect('/employee');
			} else { //else if($id)
				$data['message'] = "Deleting Employee failed.";

				$this->load->helper('form');
				$this->load->view('header', $data);
				$this->load->view('_forms/employee_delete', $data);
				$this->load->view('footer');
			} //end if($id)
		} else {
			$this->load->helper('form');
			$this->load->view('header', $data);
			$this->load->view('_forms/employee_delete', $data);
			$this->load->view('footer');
		};
	}
	public function add_time_record($id = null) {
		if(is_null($id)) {
			redirect('employee');
		};
		$this->load->model('employee_model', 'table');
		$this->load->model('department_model', 'department');
		$this->load->model('salary_grade_model', 'salary_grade');
		$this->load->model('deduction_model', 'deduction');
		$this->load->model('add_ons_model', 'add_ons');
		$this->load->model('work_on_model', 'work_on');
		$this->load->model('work_on_rates_model', 'work_on_rates');
		$input = $this->table->get_employee();
		$data = array(
				'title'             => 'Edit Employee',
				'title_description' => '',
				'table'             => 'employee',
				'id'          => array (
				                		'name'  => 'id',
				                		'value' => $input[$id]['id']

				                	),
				'first_name'       => array (
				                		'label' => 'First Name *: ',
				                		'name'  => 'first_name',
				                		'value' => $input[$id]['first_name']
				                	),
				'last_name' => array (
				                		'label' => 'Last Name: ',
				                		'name'  => 'last_name',
				                		'value' => $input[$id]['last_name']
				                	),
				'position' => array (
				                		'label' => 'Position : ',
				                		'name'  => 'position',
				                		'value' => $input[$id]['position']
				                	),
				'salary_grade' => array (
				                		'label' => 'Salary Grade : ',
				                		'name'  => 'salary_grade_id',
				                		'value' => $input[$id]['salary_grade_id']
				                	),
				'department' => array (
				                		'label' => 'Department : ',
				                		'name'  => 'department_id',
				                		'value' => $input[$id]['department_id']
				                	),
				'employee_id' => array (
				                		'label' => 'Employee ID : ',
				                		'name'  => 'employee_id',
				                		'value' => $input[$id]['employee_id']
				                	),
				'work_on' => array (
				                		'label' => '',
				                		'name'  => 'work_on',
				                		'class' => 'class="form-control"',
				                		'value' => '',
				                	),'birthdate' => array (
				                		'label' => 'Date of birthday : ',
				                		'name'  => 'birthdate',
				                		'value' => $input[$id]['birthdate']
				                	),
				'city' => array (
				                		'label' => 'City : ',
				                		'name'  => 'city',
				                		'value' => $input[$id]['city']
				                	),
				'mobile' => array (
				                		'label' => 'Mobile Number : ',
				                		'name'  => 'mobile',
				                		'value' => $input[$id]['mobile']
				                	),
				'phone' => array (
				                		'label' => 'Phone Number : ',
				                		'name'  => 'phone',
				                		'value' => $input[$id]['phone']
				                	),
				'email' => array (
				                		'label' => 'Email Address : ',
				                		'name'  => 'email',
				                		'value' => $input[$id]['email']
				                	),
				'civil_status' => array (
				                		'label' => 'Civil Status : ',
				                		'name'  => 'civil_status',
				                		'value' => $input[$id]['civil_status']
				                	),
				'sss_no' => array (
				                		'label' => 'SSS Number : ',
				                		'name'  => 'sss_no',
				                		'value' => $input[$id]['sss_no']
				                	),
				'philhealth_no' => array (
				                		'label' => 'Philhealth Number : ',
				                		'name'  => 'philhealth_no',
				                		'value' => $input[$id]['philhealth_no']
				                	),
				'pagibig_no' => array (
				                		'label' => 'Pagibig Number : ',
				                		'name'  => 'pagibig_no',
				                		'value' => $input[$id]['pagibig_no']
				                	),
				'tax_id_no' => array (
				                		'label' => 'Tax ID Number : ',
				                		'name'  => 'tax_id_no',
				                		'value' => $input[$id]['tax_id_no']
				                	),
				'date_hired' => array (
				                		'label' => 'Date Hired : ',
				                		'name'  => 'date_hired',
				                		'value' => $input[$id]['date_hired']
				                	),
			);
		$data['work_on_id'] = $this->work_on->get_work_on_list();
		$data['department_list']=$this->department->get_department_list();
		$data['salary_grade_list']=$this->salary_grade->get_salary_grade_list();
		$data['deduction_list']=$this->deduction->get_deduction_list();
		$data['add_ons_list']=$this->add_ons->get_add_ons_list();
		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('side_bar', $data);
		$this->load->view('employee_time_record', $data);
		$this->load->view('footer');
	}

	public function submit() {
		if(!($input = $this->input->post())) {
			redirect('/');
		};	

		unset($input['submit']); //remove 'submit'


		$this->load->model('employee_model', 'table');

		if(isset($input['id'])) {
			if(!isset($input['birth_cert'])) {
				$input['birth_cert']=0;
			}
			if(!isset($input['baptismal'])) {
				$input['baptismal']=0;
			}
			if(!isset($input['marriage_cert'])) {
				$input['marriage_cert']=0;
			}
			if(!isset($input['brgy_clearance'])) {
				$input['brgy_clearance']=0;
			}
			if(!isset($input['police_clearance'])) {
				$input['police_clearance']=0;
			}
			if(!isset($input['nbi'])) {
				$input['nbi']=0;
			}
			if(!isset($input['xray'])) {
				$input['xray']=0;
			}
			if(!isset($input['result'])) {
				$input['result']=0;
			}
			if(!isset($input['sputum'])) {
				$input['sputum']=0;
			}
			if(!isset($input['fecalysis'])) {
				$input['fecalysis']=0;
			}
			if(!isset($input['health_card'])) {
				$input['health_card']=0;
			}
			if(!isset($input['drug_test'])) {
				$input['drug_test']=0;
			}
			if(!isset($input['drug_test'])) {
				$input['drug_test']=0;
			}
			if(!isset($input['eye_test'])) {
				$input['eye_test']=0;
			}
			if(!isset($input['bio_date'])) {
				$input['bio_date']=0;
			}
			if(!isset($input['resume'])) {
				$input['resume']=0;
			}
			if(!isset($input['id_picture'])) {
				$input['id_picture']=0;
			}
			if(!isset($input['contract'])) {
				$input['contract']=0;
			}

			$id = $this->table->update($input);
		} else {
			$id = $this->table->add($input);
		};

		if($id != -1) { //if success
			redirect('/employee');
		} else { //else if($id)
			$data['message'] = "Adding New Employee failed.";
			$data = array(
					'title'             => 'Employee',
					'title_description' => '',
					'table'             => 'employee',
					'label'       => array (
											'label' => 'Label *: ',
											'name'  => 'label',
											'value' => $input['label']
										),
					'description' => array (
											'label' => 'Description: ',
											'name'  => 'description',
											'value' => $input['description']
										),
				);

			$this->load->helper('form');
			$this->load->view('header', $data);
			$this->load->view('navigation_top', array('include_search' => 0));
			$this->load->view('_forms/employee_add', $data);
			$this->load->view('footer');
		} //end if($id)
	}
	public function calculate() {
		if(!($input = $this->input->post())) {
			redirect('/');
		};
		unset($input['submit']);
		//exit();
		$this->load->model('work_on_rates_model', 'work_on_rates');
		$this->load->model('gross_for_day_model', 'gross_for_day');
		$this->load->model('work_on_total_model', 'work_on_total');
		$this->load->model('temp_payslip_model', 'temp_payslip');
		$this->load->model('deduction_total_model', 'deduction');
		$this->load->model('gross_for_pieces_model', 'gross_for_piece');
		$this->load->model('add_ons_total_model', 'add_ons');
		$this->load->model('salary_grade_model', 'salary_grade');
		$time_reports = $input['time_report'];
		$total_pay_for_month = 0;
		$deduction_total = 0;
		$add_ons_total = 0;
		$value = 0;
		$value2 = 0;
		$date_start = date('Y-m-d', strtotime(str_replace("/","-",$input['date_start'])));
		$date_end = date('Y-m-d', strtotime(str_replace("/","-",$input['date_end'])));
		$temp_pay_slip = array(
			'emp_id' => $input['emp_id'], 
			);
		$salary_grade_list = $this->salary_grade->get_salary_grade_rate();
		$temp_pay_slip_id1 = $this->temp_payslip->add($temp_pay_slip);
		foreach ($time_reports as $key => $time_report) {
			$temp_pay_slip_id = $temp_pay_slip_id1;
			if ($time_report['salary_grade_id'] == '1' OR $time_report['salary_grade_id'] == '2') {
				$gross = array(
					'emp_id' => $input['emp_id'],
					'temp_payslip_id' => $temp_pay_slip_id,
					'date_starts' => $date_start,
					'date_ends' => $date_end,
				);
				$gross_for_pieces_id = $this->gross_for_piece->add($gross);
				foreach ($time_reports as $key => $time_report) { 
					$work_on_id = $time_report['work_on'];
					$value = $value + $time_report['value'];
					$value_per_piece = $time_report['value'];
					$gross = array(
						'gross_for_pieces_id' => $gross_for_pieces_id,
						'day' => $time_report['day'],
						'values' => $value_per_piece,
						'work_on_id' => $work_on_id,
					);
					$this->gross_for_piece->add_per_day($gross);
				}
				if ($time_report['salary_grade_id'] == 'parer') {
					if($value <= 10500) {
						$value1 = $value;
						$value2 = 0;
					} else {
						$value1 = 10500;
						$value2 = $value - 10500;
					}
					$total_pay_for_month = $value * $salary_grade_list[$input['salary_grade_id']];

					unset($gross); 
					$gross = array(
						'value1' => $value1,
						'value2' => $value2,
					);
					$this->gross_for_piece->update($gross, $gross_for_pieces_id);
					
				}
				else if ($time_report['salary_grade_id'] == 'sheller') {
					if($value <= 10500) {
						$value1 = $value;
						$value2 = 0;
					} else {
						$value1 = 10500;
						$value2 = $value - 10500;
					}
					$total_pay_for_month = $value * $salary_grade_list[$input['salary_grade_id']];

					unset($gross); 
					$gross = array(
						'value1' => $value1,
						'value2' => $value2,
					);
					$this->gross_for_piece->update($gross, $gross_for_pieces_id);
				}
				
				//update
				$total_gross = array(
					'gross_total' => $total_pay_for_month, );
				$temp_pay_slip_id = $this->temp_payslip->update($total_gross,$temp_pay_slip_id);
				// deduction
				$deductions = $input['deduction'];
				foreach ($deductions as $key => $deduction) {
					if ($deduction == '' OR $deduction == '0') {
						$deduction = 0;
					}
					$deduct = array(
						'emp_id' => $input['emp_id'], 
						'temp_payslip_id' => $temp_pay_slip_id, 
						'date_start' => $date_start, 
						'date_end' => $date_end, 
						'deduction_id' => $key,
						'value' => $deduction, 
						);
					$this->deduction->add($deduct);
					$deduction_total = $deduction_total + $deduction;
					unset($deduct);
				}
				//extras
				$add_ons = $input['add-on'];
				foreach ($add_ons as $key_ao => $add_on_value) {
					if ($add_on_value == '' OR $add_on_value == '0') {
						$add_on_value = 0;
					}
					$add_on = array(
						'emp_id' => $input['emp_id'], 
						'temp_payslip_id' => $temp_pay_slip_id, 
						'date_start' => $date_start, 
						'date_end' => $date_end, 
						'add_ons_id' => $key_ao,
						'value' => $add_on_value, 
						);

					$add_ons_total = $add_ons_total + $add_on_value;
					$this->add_ons->add($add_on);
					unset($add_on);
				}
				if ($deduction_total = '' OR $deduction_total = '0') {
					$deduction_total = '0';
				}
				if ($add_ons_total = '' OR $add_ons_total = '0') {
					$add_ons_total = '0';
				}
				$total_gross = array(
					'deduction_total' => $deduction_total,
					'extras_total' => $add_ons_total, 
				);
				$temp_pay_slip_id = $this->temp_payslip->update($total_gross,$temp_pay_slip_id);
			} else {
				$time_out = $time_report['time_out'];
				$time_in = $time_report['time_in'];
				$date = DateTime::createFromFormat( 'H:i A', $time_out);
			 	$time_out = $date->format('H:i:s');
				$date = DateTime::createFromFormat( 'H:i A', $time_in);
				$time_in = $date->format( 'H:i:s');
				$work_on_id = $time_report['work_on'];
				$work_ons = $this->work_on_rates->get_work_on_rates_list_based_on_id($work_on_id);
				foreach ($work_ons as $id => $work_on) {
					if ($work_on['description'] == 'payequals' AND $work_on['shift'] == '1') {
						$payequals1r = $work_on['value']/100;
					}
					else if ($work_on['description'] == 'payequals' AND $work_on['shift'] == '0') {
						$payequals2r = $work_on['value']/100;
					}
					else if ($work_on['description'] == 'overtime' AND $work_on['shift'] == '1') {
						$ot1r = $work_on['value']/100;
					}
					else if ($work_on['description'] == 'overtime' AND $work_on['shift'] == '0') {
						$ot2r = $work_on['value']/100;
					}
				}
				$payequals1t = $input{'rt_' . $time_report['day']}; // rt
			 	$payequals2t = $input{'totalhours_' . $time_report['day']}; // total
			 	$ot1t =$input{'ot_' . $time_report['day']}; //ot
			 	$ot2t = $input{'otnight_' . $time_report['day']};
// $start_date = new DateTime('2007-09-01 08:10:58');
// $since_start = $start_date->diff(new DateTime('2012-09-02 00:25:00'));
// echo $since_start->days.' days total<br>';
// echo $since_start->y.' years<br>';
// echo $since_start->m.' months<br>';
// echo $since_start->d.' days<br>';
// echo $since_start->h.' hours<br>';
// echo $since_start->i.' minutes<br>';
// echo $since_start->s.' seconds<br>';

				$pay_equals1 = $payequals1t * ($salary_grade_list[$input['salary_grade_id']] / 8) * $payequals1r;
				$pay_equals2 = $payequals2t * ($salary_grade_list[$input['salary_grade_id']] / 8) * $payequals2r;
				$overtime1 = $ot1t * ($salary_grade_list[$time_report['salary_grade_id']] / 8)  * $ot1r;
				$overtime0 = $ot2t * ($salary_grade_list[$time_report['salary_grade_id']] / 8) * $ot2r;
				
				$total_pay_for_day = ($pay_equals1 + $pay_equals2 + $overtime1 + $overtime0);
				if(!isset($gross)) {
					unset($gross);
				}
				$gross = array(
					'day' => $time_report['day'],
					'temp_payslip_id' => $temp_pay_slip_id,
					'work_on_id' => $work_on_id,
					'pay_equals1' => $pay_equals1,
					'pay_equals0' => $pay_equals2,
					'overtime1' => $overtime1,
					'overtime0' => $overtime0,
					'pay_equals1_hours' => $payequals1t,
					'pay_equals0_hours' => $payequals2t,
					'overtime1_hours' => $ot1t,
					'overtime0_hours' => $ot2t,
					'total_pay_for_day' => $total_pay_for_day,
					'date_starts' => $date_start,
					'date_ends' => $date_end,
					'time_starts' => $time_out,
					'time_ends' => $time_in,
					);

				$this->gross_for_day->add($gross);			
				//total_pay_gross


				$total_pay_for_month = $total_pay_for_month + $total_pay_for_day; 
				$work_on_per_days[] = array(
					'work_on_id' => $time_report['work_on'], 
					'pay_equals1' => $input{'rt_' . $time_report['day']},
					'pay_equals0' => $input{'totalhours_' . $time_report['day']}, 
					'overtime1' => $input{'ot_' . $time_report['day']},
					'overtime0' => $input{'otnight_' . $time_report['day']},
					);
				$total_work_on = array();
				for($i=0; $i<count($work_on_per_days); $i++)
				{
					$id = $work_on_per_days[$i]['work_on_id'];
					if(array_key_exists($id, $total_work_on))
				    {
				        // Sum it.
				        $total_work_on[$id]['work_on_id'] = $id;
				        $total_work_on[$id]['temp_payslip_id'] = $temp_pay_slip_id;
				        $total_work_on[$id]['pay_equals1'] += $work_on_per_days[$i]['pay_equals1'];
				        $total_work_on[$id]['pay_equals0'] += $work_on_per_days[$i]['pay_equals0'];
				        $total_work_on[$id]['overtime1'] += $work_on_per_days[$i]['overtime1'];
				        $total_work_on[$id]['overtime0'] += $work_on_per_days[$i]['overtime0'];
				    }
				    // If not...
				    else
				    {
				        // Initialize it.
				        $total_work_on[$id]['work_on_id'] = $id;
				        $total_work_on[$id]['temp_payslip_id'] = $temp_pay_slip_id;
				        $total_work_on[$id]['pay_equals1'] = $work_on_per_days[$i]['pay_equals1'];
				        $total_work_on[$id]['pay_equals0'] = $work_on_per_days[$i]['pay_equals0'];
				        $total_work_on[$id]['overtime1'] = $work_on_per_days[$i]['overtime1'];
				        $total_work_on[$id]['overtime0'] = $work_on_per_days[$i]['overtime0'];
				    }
				}
							foreach ($total_work_on as $key => $two) {
				$this->work_on_total->add($two);
			}
			$total_gross = array(
				'gross_total' => $total_pay_for_month, );
			$temp_pay_slip_id = $this->temp_payslip->update($total_gross,$temp_pay_slip_id);
			// deduction
			$deductions = $input['deduction'];
			foreach ($deductions as $key => $deduction) {
				if ($deduction == '' OR $deduction == '0') {
					$deduction = 0;
				}
				$deduct = array(
					'emp_id' => $input['emp_id'], 
					'temp_payslip_id' => $temp_pay_slip_id, 
					'date_start' => $date_start, 
					'date_end' => $date_end, 
					'deduction_id' => $key,
					'value' => $deduction, 
					);
				$this->deduction->add($deduct);
				$deduction_total = $deduction_total + $deduction;
				unset($deduct);
			}
			//extras
			$add_ons = $input['add-on'];
			foreach ($add_ons as $key_ao => $add_on_value) {
				if ($add_on_value == '' OR $add_on_value == '0') {
					$add_on_value = 0;
				}
				$add_on = array(
					'emp_id' => $input['emp_id'], 
					'temp_payslip_id' => $temp_pay_slip_id, 
					'date_start' => $date_start, 
					'date_end' => $date_end, 
					'add_ons_id' => $key_ao,
					'value' => $add_on_value, 
					);

				$add_ons_total = $add_ons_total + $add_on_value;
				$this->add_ons->add($add_on);
				unset($add_on);
			}

			$total_gross = array(
				'deduction_total' => $deduction_total,
				'extras_total' => $add_ons_total, 
			);
			$temp_pay_slip_id = $this->temp_payslip->update($total_gross,$temp_pay_slip_id);

			}
		}



		redirect('/employee');
		exit();
		if ($input['salary_grade_id'] == 'parer' OR $input['salary_grade_id'] == 'sheller') {
		} else{
			foreach ($time_reports as $key => $time_report) {
				
			} // foreach ($time_reports as $key => $time_report) {


		}
		
		
		//wo
		echo 'good';
				redirect('/employee');
		exit();

		//unset($input['submit']); //remove 'submit'	

		$this->load->model('employee_model', 'table');
		if(isset($input['id'])) {
			$id = $this->table->update($input);
		} else {
			$id = $this->table->add($input);
		};

		if($id != -1) { //if success
			redirect('/employee');
		} else { //else if($id)
			$data['message'] = "Adding New Employee failed.";
			$data = array(
					'title'             => 'Employee',
					'title_description' => '',
					'table'             => 'employee',
					'label'       => array (
											'label' => 'Label *: ',
											'name'  => 'label',
											'value' => $input['label']
										),
					'description' => array (
											'label' => 'Description: ',
											'name'  => 'description',
											'value' => $input['description']
										),
				);

			$this->load->helper('form');
			$this->load->view('header', $data);
			$this->load->view('navigation_top', array('include_search' => 0));
			$this->load->view('_forms/employee_add', $data);
			$this->load->view('footer');
		} //end if($id)
	}


}

/* End of file employee.php */
/* Location: ./application/controllers/employee.php */