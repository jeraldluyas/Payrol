<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function __construct() {
        parent::__construct();
        if(!$this->ion_auth->logged_in()) {
            redirect('/auth/login');
        };
        if(!$this->ion_auth->in_group('admin') AND !$this->ion_auth->is_admin()) {
        	$this->session->set_flashdata('title', 'Permission denied');
        	$this->session->set_flashdata('heading', 'Permission Denied!');
        	$this->session->set_flashdata('message', 'You do not have enough permission to view the contents. <a href="'.base_url('/').'">Go back home.</a>');
        	redirect('/error/');
        };
    }

	public function index() {
		$this->load->model('reports_model', 'reports');
		$this->load->model('employee_model', 'employee');
		$employee_lists = $this->employee->get_employee_reports();
		$report_attendances = $this->reports->get_reports_for_day();
		$data = array(
				'title'             => 'Reports List',
				'title_description' => '',
				'table'             => 'reports'
			);
		$data['employee_lists'] = $employee_lists;

		$data['report_attendances'] = $report_attendances;
		$this->load->view('header', $data);
		$this->load->view('side_bar', $data);
		$this->load->view('reports', $data);
		$this->load->view('footer');
	}
	public function regular_employee() {
		
		$this->load->model('reports_model', 'reports');
		$this->load->model('employee_model', 'employee');
		$employee_lists = $this->employee->get_employee_reports();
		$report_attendances = $this->reports->get_reports_for_day();
		$data = array(
				'title'             => 'Reports List',
				'title_description' => '',
				'table'             => 'reports'
			);
		$data['months'] = array(
			'1' => 'January',
			'2' => 'February',
			'3' => 'March',
			'4' => 'April',
			'5' => 'May',
			'6' => 'June',
			'7' => 'July', 
			'8' => 'August', 
			'9' => 'September', 
			'10' => 'October', 
			'11' => 'November',
			'12' => 'December', 
		);
		$data['employee_lists'] = $employee_lists;
		$year= date("Y");
		$years = array();
		// $y = $year;
		// $y = $year + 1;
		// for ($x=$y; $x >= $year ; $x++) { 
		// 	$years[] = array($x => $x, );
		// }
		$current_month=date('m');
		$data['month'] = $current_month;
		$firstYear = (int)date('Y') - 1;
		$lastYear = $firstYear + 1;
		for($i=$firstYear;$i<=$lastYear;$i++)
		{
		    $years[$i] = $i;
		}

		$data['years'] = $years;
		$data['year'] = date('Y');
		if(($input = $this->input->post())) { 
			$data['year'] = $input['year'];
			$data['month'] = $input['month'];
		}
		$data['day'] = days_in_month($data['month'] , $data['year']);
		$this->load->helper('form');
		$data['report_attendances'] = $report_attendances;
		$this->load->view('header', $data);
		$this->load->view('side_bar', $data);
		$this->load->view('regular_employee', $data);
		$this->load->view('footer');
	}
	public function payslip() {
		$this->load->model('reports_model', 'reports');
		$this->load->model('employee_model', 'employee');
		$this->load->model('salary_grade_model', 'salary_grade');

		$employee_lists = $this->employee->get_employee_sheller_reports();
		$report_attendances = $this->reports->get_reports_for_day();
		$data = array(
				'title'             => 'Reports List',
				'title_description' => '',
				'table'             => 'reports'
			);
		$data['employee_lists'] = $employee_lists;

		$data['report_attendances'] = $report_attendances;
		
		$data['salary_grade_list']=$this->salary_grade->get_salary_grade();
		$this->load->view('header', $data);
		$this->load->view('side_bar', $data);
		$this->load->view('payslip', $data);
		$this->load->view('footer');
	}

	public function add() {
		$data = array(
				'title'             => 'Add reports',
				'title_description' => '',
				'table'             => 'reports',
				'label'       => array (
				                		'label' => 'Label *: ',
				                		'name'  => 'label',
				                		'value' => ''
				                	),
				'description' => array (
				                		'label' => 'Description: ',
				                		'name'  => 'description',
				                		'value' => ''
				                	),
			);

		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('_forms/reports_add', $data);
		$this->load->view('footer');
	}

	public function edit($id = null) {
		if(is_null($id)) {
			redirect('reports');
		};

		$this->load->model('reports_model', 'table');
		$input = $this->table->get_reports();
		$data = array(
				'title'             => 'Edit reports',
				'title_description' => '',
				'table'             => 'reports',
				'id'          => array (
				                		'name'  => 'reports_id',
				                		'value' => $input[$id]['reports_id']
				                	),
				'label'       => array (
				                		'label' => 'Label *: ',
				                		'name'  => 'label',
				                		'value' => $input[$id]['label']
				                	),
				'description' => array (
				                		'label' => 'Description: ',
				                		'name'  => 'description',
				                		'value' => $input[$id]['value']
				                	),
			);

		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('_forms/reports_add', $data);
		$this->load->view('footer');
	}

	public function delete($id = null) {
		if(is_null($id)) {
			redirect('reports');
		};

		//check if $id exists
		$this->load->model('reports_model', 'table');
		if(!$this->table->get_reports_list($id)) {
			redirect('reports');
		}
			
		$data = array(
				'title'             => 'Delete reports',
				'title_description' => 'are you sure you want to delete this? <br/> this is irreversible.',
				'table'             => 'reports',
				'id'          => array (
				                		'name'  => 'reports_id',
				                		'value' => $id
				                	),
			);

		if(($input = $this->input->post())) {
			unset($input['submit']); //remove 'submit'	
			if(isset($input['reports_id'])) {
				$affected_id = $this->table->delete($input);
			};

			if($affected_id) { //if success
				redirect('/reports');
			} else { //else if($id)
				$data['message'] = "Deleting Patient Type failed.";

				$this->load->helper('form');
				$this->load->view('header', $data);
				$this->load->view('_forms/reports_delete', $data);
				$this->load->view('footer');
			} //end if($id)
		} else {
			$this->load->helper('form');
			$this->load->view('header', $data);
			$this->load->view('_forms/reports_delete', $data);
			$this->load->view('footer');
		};
	}

	public function submit() {
		if(!($input = $this->input->post())) {
			redirect('/');
		};	

		unset($input['submit']); //remove 'submit'	

		$this->load->model('reports_model', 'table');
		if(isset($input['reports_id'])) {
			$id = $this->table->update($input);
		} else {
			$id = $this->table->add($input);
		};

		if($id != -1) { //if success
			redirect('/reports');
		} else { //else if($id)
			$data['message'] = "Adding New Patient Type failed.";
			$data = array(
					'title'             => 'Patient Type',
					'title_description' => '',
					'table'             => 'reports',
					'label'       => array (
											'label' => 'Label *: ',
											'name'  => 'label',
											'value' => $input['label']
										),
					'description' => array (
											'label' => 'Description: ',
											'name'  => 'description',
											'value' => $input['value']
										),
				);

			$this->load->helper('form');
			$this->load->view('header', $data);
			$this->load->view('_forms/reports_add', $data);
			$this->load->view('footer');
		} //end if($id)
	}


}

/* End of file reports.php */
/* Location: ./application/controllers/reports.php */