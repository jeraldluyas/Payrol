<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Salary_grade extends CI_Controller {

	public function __construct() {
        parent::__construct();
        if(!$this->ion_auth->logged_in()) {
            redirect('/auth/login');
        };
        if(!$this->ion_auth->in_group('admin') AND !$this->ion_auth->is_admin()) {
        	$this->session->set_flashdata('title', 'Permission denied');
        	$this->session->set_flashdata('heading', 'Permission Denied!');
        	$this->session->set_flashdata('message', 'You do not have enough permission to view the contents. <a href="'.base_url('/').'">Go back home.</a>');
        	redirect('/error/');
        };
    }

	public function index() {
		$this->load->model('salary_grade_model', 'table');

		$data = array(
				'title'             => 'Salary Grade List',
				'title_description' => '',
				'item'              => $this->table->get_salary_grade(),
				'table'             => 'salary_grade'
			);

		$this->load->view('header', $data);
		$this->load->view('side_bar', $data);
		$this->load->view('salary_grade', $data);
		$this->load->view('footer');
	}

	public function add() {
		$data = array(
				'title'             => 'Add Salary Grade',
				'title_description' => '',
				'table'             => 'salary_grade',
				'bracket'       => array (
				                		'label' => 'Bracket *: ',
				                		'name'  => 'bracket',
				                		'value' => ''
				                	),
				'rate_per_day' => array (
				                		'label' => 'Rate Per Day: ',
				                		'name'  => 'rate_per_day',
				                		'value' => ''
				                	),
				'months_13' => array (
				                		'label' => '13 Months: ',
				                		'name'  => 'months_13',
				                		'value' => ''
				                	),
				'sil_day_314' => array (
				                		'label' => 'sil/day 314: ',
				                		'name'  => 'sil_day_314',
				                		'value' => ''
				                	),
				'sil_day_313' => array (
				                		'label' => 'sil/day 313: ',
				                		'name'  => 'sil_day_313',
				                		'value' => ''
				                	),
				'remarks' => array (
				                		'label' => 'remarks: ',
				                		'name'  => 'remarks',
				                		'value' => ''
				                	),
			);

		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('_forms/salary_grade_add', $data);
		$this->load->view('footer');
	}

	public function edit($id = null) {
		if(is_null($id)) {
			redirect('salary_grade');
		};

		$this->load->model('salary_grade_model', 'table');
		$input = $this->table->get_salary_grade();
		$data = array(
				'title'             => 'Edit salary_grade',
				'title_description' => '',
				'table'             => 'salary_grade',
				'id'          => array (
				                		'name'  => 'id',
				                		'value' => $input[$id]['id']
				                	),
				'bracket'       => array (
				                		'label' => 'Bracket *: ',
				                		'name'  => 'bracket',
				                		'value' => $input[$id]['bracket']
				                	),
				'rate_per_day' => array (
				                		'label' => 'Rate Per Day: ',
				                		'name'  => 'rate_per_day',
				                		'value' => $input[$id]['rate_per_day']
				                	),
				'months_13' => array (
				                		'label' => '13 Months: ',
				                		'name'  => 'months_13',
				                		'value' => $input[$id]['months_13']
				                	),
				'sil_day_314' => array (
				                		'label' => 'sil/day 314: ',
				                		'name'  => 'sil_day_314',
				                		'value' => $input[$id]['sil_day_314']
				                	),
				'sil_day_313' => array (
				                		'label' => 'sil/day 313: ',
				                		'name'  => 'sil_day_313',
				                		'value' => $input[$id]['sil_day_313']
				                	),
				'remarks' => array (
				                		'label' => 'remarks: ',
				                		'name'  => 'remarks',
				                		'value' => $input[$id]['remarks']
				                	),
			);

		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('_forms/salary_grade_add', $data);
		$this->load->view('footer');
	}

	public function delete($id = null) {
		if(is_null($id)) {
			redirect('salary_grade');
		};

		// //check if $id exists
		 $this->load->model('salary_grade_model', 'table');
		// if(!$this->table->get_salary_grade_list($id)) {
		// 	redirect('salary_grade');
		// }
			
		// $data = array(
		// 		'title'             => 'Delete salary_grade',
		// 		'title_description' => 'are you sure you want to delete this? <br/> this is irreversible.',
		// 		'table'             => 'salary_grade',
		// 		'id'          => array (
		// 		                		'name'  => 'salary_grade_id',
		// 		                		'value' => $id
		// 		                	),
		// 	);

		if(!is_null($id)) {
			//unset($input['submit']); //remove 'submit'	
			if(isset($id)) {
				$data = array('id' => $id, );
				$affected_id = $this->table->delete($data);
			};

			if($affected_id) { //if success
				redirect('/salary_grade');
			} else { //else if($id)
				$data['message'] = "Deleting Patient Type failed.";

				$this->load->helper('form');
				$this->load->view('header', $data);
				$this->load->view('_forms/salary_grade_delete', $data);
				$this->load->view('footer');
			} //end if($id)
		} else {
				redirect('/salary_grade');
			
			$this->load->helper('form');
			$this->load->view('header', $data);
			$this->load->view('_forms/salary_grade_delete', $data);
			$this->load->view('footer');
		};
	}

	public function submit() {
		if(!($input = $this->input->post())) {
			redirect('/');
		};	

		unset($input['submit']); //remove 'submit'	

		$this->load->model('salary_grade_model', 'table');
		if(isset($input['id'])) {
			$id = $this->table->update($input);
		} else {
			$id = $this->table->add($input);
		};

		if($id != -1) { //if success
			redirect('/salary_grade');
		} else { //else if($id)
			$data['message'] = "Adding New Patient Type failed.";
			$data = array(
					'title'             => 'Patient Type',
					'title_description' => '',
					'table'             => 'salary_grade',
					'label'       => array (
											'label' => 'Label *: ',
											'name'  => 'label',
											'value' => $input['label']
										),
					'description' => array (
											'label' => 'Description: ',
											'name'  => 'description',
											'value' => $input['value']
										),
				);

			$this->load->helper('form');
			$this->load->view('header', $data);
			$this->load->view('_forms/salary_grade_add', $data);
			$this->load->view('footer');
		} //end if($id)
	}


}

/* End of file salary_grade.php */
/* Location: ./application/controllers/salary_grade.php */