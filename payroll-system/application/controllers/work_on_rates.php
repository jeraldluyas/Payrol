<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Work_on_rates extends CI_Controller {

	public function __construct() {
        parent::__construct();
        if(!$this->ion_auth->logged_in()) {
            redirect('/auth/login');
        };
        if(!$this->ion_auth->in_group('admin') AND !$this->ion_auth->is_admin()) {
        	$this->session->set_flashdata('title', 'Permission denied');
        	$this->session->set_flashdata('heading', 'Permission Denied!');
        	$this->session->set_flashdata('message', 'You do not have enough permission to view the contents. <a href="'.base_url('/').'">Go back home.</a>');
        	redirect('/error/');
        };
    }

	public function index() {
		$this->load->model('work_on_model', 'work_on');
		$this->load->model('work_on_rates_model', 'table');

		$data = array(
				'title'             => 'Work On Rates',
				'title_description' => '',
				'item'              => $this->table->get_work_on_rates(),
				'table'             => 'work_on_rates'
			);

		$data['work_on']=$this->work_on->get_work_on_list();
		$this->load->view('header', $data);
		$this->load->view('side_bar', $data);
		$this->load->view('work_on_rates', $data);
		$this->load->view('footer');
	}

	public function add() {

		$this->load->model('work_on_model', 'work_on');
		$data = array(
				'title'             => 'Add Work On Rates',
				'title_description' => '',
				'table'             => 'work_on_rates',
				'work_on_id'       => array (
										'label' => 'Work_on_id *: ',
				                		'name'  => 'work_on_id',
				                		'value' => ''
				                	),
				'shift' => array (
				                		'label' => 'Shift: ',
				                		'name'  => 'shift',
				                		'value' => ''
				                	),
				'values' => array (
				                		'label' => 'Value: ',
				                		'name'  => 'values',
				                		'value' => ''
				                	),
			);
		$data['work_on']=$this->work_on->get_work_on_list();
		$data['shifting'] = array(
							'1' => 'Dayshift',
							'0' => 'Nightshift',
						 );
		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('_forms/work_on_rates_add', $data);
		$this->load->view('footer');
	}

	public function edit($id = null) {
		if(is_null($id)) {
			redirect('work_on_rates');
		};

		$this->load->model('work_on_rates_model', 'table');
		$this->load->model('work_on_model', 'work_on');
		$input = $this->table->get_work_on_rates();
		$data = array(
				'title'             => 'Edit Work On Rates',
				'title_description' => '',
				'table'             => 'work_on_rates',
				'id'          => array (
				                		'name'  => 'id',
				                		'value' => $input[$id]['id']
				                	),
				'work_on_id'       => array (
										'label' => 'Work_on_id *: ',
				                		'name'  => 'work_on_id',
				                		'value' => $input[$id]['work_on_id']
				                	),
				'shift' => array (
				                		'label' => 'Shift: ',
				                		'name'  => 'shift',
				                		'value' => $input[$id]['shift']
				                	),
				'values' => array (
				                		'label' => 'Value: ',
				                		'name'  => 'values',
				                		'value' => $input[$id]['values']
				                	),
			);

		$data['work_on']=$this->work_on->get_work_on_list();
		$data['shifting'] = array(
							'1' => 'Dayshift',
							'0' => 'Nightshift',
						 );
		$this->load->helper('form');
		$this->load->view('header', $data);
		$this->load->view('_forms/work_on_rates_add', $data);
		$this->load->view('footer');
	}

	public function delete($id = null) {
		if(is_null($id)) {
			redirect('work_on_rates');
		};

		// //check if $id exists
		$this->load->model('work_on_rates_model', 'table');
		// if(!$this->table->get_work_on_rates_list($id)) {
		// 	redirect('work_on_rates');
		// }
			
		// $data = array(
		// 		'title'             => 'Delete Work On',
		// 		'title_description' => 'are you sure you want to delete this? <br/> this is irreversible.',
		// 		'table'             => 'work_on_rates',
		// 		'id'          => array (
		// 		                		'name'  => 'id',
		// 		                		'value' => $id
		// 		                	),
		// 	);

		if(!is_null($id)) {
			//unset($input['submit']); //remove 'submit'	
			if(isset($id)) {
				$data = array('id' => $id, );
				$affected_id = $this->table->delete($data);
			};

			if($affected_id) { //if success
				redirect('/work_on_rates');
			} else { //else if($id)
				$data['message'] = "Deleting Work On failed.";

				$this->load->helper('form');
				$this->load->view('header', $data);
				$this->load->view('_forms/work_on_rates_delete', $data);
				$this->load->view('footer');
			} //end if($id)
		} else {
				redirect('/work_on_rates');
			
			$this->load->helper('form');
			$this->load->view('header', $data);
			$this->load->view('_forms/work_on_rates_delete', $data);
			$this->load->view('footer');
		};
	}

	public function submit() {
		if(!($input = $this->input->post())) {
			redirect('/');
		};	

		unset($input['submit']); //remove 'submit'	

		$this->load->model('work_on_rates_model', 'table');
		if(isset($input['id'])) {
			$id = $this->table->update($input);
		} else {
			$id = $this->table->add($input);
		};

		if($id != -1) { //if success
			redirect('/work_on_rates');
		} else { //else if($id)
			$data['message'] = "Adding New Work On failed.";
			$data = array(
					'title'             => 'Work On',
					'title_description' => '',
					'table'             => 'work_on_rates',
					'label'       => array (
											'label' => 'Label *: ',
											'name'  => 'label',
											'value' => $input['label']
										),
					'description' => array (
											'label' => 'Description: ',
											'name'  => 'description',
											'value' => $input['description']
										),
				);

			$this->load->helper('form');
			$this->load->view('header', $data);
			$this->load->view('_forms/work_on_rates_add', $data);
			$this->load->view('footer');
		} //end if($id)
	}


}

/* End of file work_on_rates.php */
/* Location: ./application/controllers/work_on_rates.php */