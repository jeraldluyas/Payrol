<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * MY Constructor Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Francis Luigi Magpantay
 */

// ------------------------------------------------------------------------

/**
* Clean text for URLs 
*
* Strips strings for urls
*
* @access	public
* @param	string	string to transform
* @param	string	space separator (ie: - or _ for [What-the-Pack or What_the_Pack])
* @param	boolean	transforms string to lowercase (ie: What-the-Pack to what-the-pack or What_the_Pack to what_the_pack)
*/	
if ( ! function_exists('input_to_url'))
{
	function input_to_url($string, $separator = '_', $to_lowercase = TRUE)
	{
		return  url_title($string, $separator, $to_lowercase); //form_helper needed
	}
}

// ------------------------------------------------------------------------

/**
* Input Hidden Text Generator
*
* Generates hidden input text
* Same parameter syntax as input_text
*
* @access	public
* @param	
* @param	string	name of the text element being created
* @param	string	default value of text element
* @param	
* @param	string  string that will be added as atrributes on the element (ie: autocomplete="off" onClick="this.show()")
*/	
if ( ! function_exists('input_hidden'))
{
	function input_hidden($l = null, $name, $value = '', $s = null, $additional_attributes = null)
	{
		//derived from form_input in form_helper
		$defaults = array('type' => 'hidden', 'name' => $name, 'value' => $value);
		$extra = 'id="'.$name.'" '.$additional_attributes;
		
		return "<input "._parse_form_attributes($name, $defaults).$extra." />";
	}
}

// ------------------------------------------------------------------------

/**
* Input Text Generator
*
* Generates Label and Input text combo
*
* @access	public
* @param	string	label text (text inside label element)
* @param	string	name of the text element being created
* @param	string	default value of text element
* @param	string	any string that can be placed between the label and input elements (ie: <br/>, </div><div>, <hr/>)
* @param	string  string that will be added as atrributes on the element (ie: autocomplete="off" onClick="this.show()")
*/	
if ( ! function_exists('input_text'))
{
	function input_text($label = '', $name, $value = '', $separator = '', $additional_attributes = null)
	{
		if($label != '') {
			$label = form_label($label, $name);
		};
		return $label . $separator . form_input($name, $value, 'id="'.$name.'" '.$additional_attributes); //form_helper needed
	}
}

// ------------------------------------------------------------------------

/**
* Input Password Generator
*
* Generates Label and Input Password text combo
*
* @access	public
* @param	string	label text (text inside label element)
* @param	string	name of the text element being created
* @param	string	default value of text element
* @param	string	any string that can be placed between the label and input elements (ie: <br/>, </div><div>, <hr/>)
* @param	string  string that will be added as atrributes on the element (ie: autocomplete="off" onClick="this.show()")
*/	
if ( ! function_exists('input_password'))
{
	function input_password($label = '', $name, $value = '', $separator = '', $additional_attributes = null)
	{
		if($label != '') {
			$label = form_label($label, $name);
		};
		return $label . $separator . form_password($name, $value, 'id="'.$name.'" '.$additional_attributes); //form_helper needed
	}
}

// ------------------------------------------------------------------------

/**
* Input File Generator
*
* Generates Label and Input File text combo
*
* @access	public
* @param	string	label text (text inside label element)
* @param	string	name of the text element being created
* @param	string	default value of text element
* @param	string	any string that can be placed between the label and input elements (ie: <br/>, </div><div>, <hr/>)
*/	
if ( ! function_exists('input_file'))
{
	function input_file($label = '', $name, $value = '', $separator = '')
	{
		if($label != '') {
			$label = form_label($label, $name);
		};
		return $label . $separator . form_upload($name, $value, 'id="'.$name.'"'); //form_helper needed
	}
}

// ------------------------------------------------------------------------

/**
* Input Textarea Generator
*
* Generates Label and Input Textarea combo
*
* @access	public
* @param	string	label text (text inside label element)
* @param	string	name of the text element being created
* @param	string	default value of Textarea element
* @param	string	any string that can be placed between the label and Textarea elements (ie: <br/>, </div><div>, <hr/>)
* @param	int	value for the rows height of the textarea
* @param	int	value for the columns width of the textarea
* @param	string  string that will be added as atrributes on the element (ie: autocomplete="off" onClick="this.show()")
*/	
if ( ! function_exists('input_textarea'))
{
	function input_textarea($label = '', $name, $value = '', $separator = '', $rows = 4, $cols = 60, $additional_attributes = null)
	{
		$data = array (
				'id'    => $name,
				'name'  => $name,
				'value' => $value,
				'rows'  => $rows,
				'cols'  => $cols
			);
		if($label != '') {
			$label = form_label($label, $name);
		};
		return $label . $separator . form_textarea($data, $value, $additional_attributes); //form_helper needed
	}
}

// ------------------------------------------------------------------------

/**
* Select Generator
*
* Generates option list for selection with the supplied array of [value] => displayed text
*
* @access	public
* @param	string	label text (text inside label element)
* @param	string	name of the text element being created
* @param	array	values and displayed text
* @param	string	any string that can be placed between the label and input elements (ie: <br/>, </div><div>, <hr/>)
* @param	string	default selected value
* @param	boolean	if blanks are included
* @param	string	displayed text on blank
*/	
if ( ! function_exists('input_select'))
{
	function input_select($label = '', $name, $values, $separator = '', $selected_value = '', $include_blank = TRUE, $blank_text = '')
	{
		if($include_blank == TRUE) {
			$tmp = $values; 
			$values = array("" => $blank_text) + $tmp;
		};
		if($label != '') {
			$label = form_label($label, $name);
		};
		return $label . $separator . form_dropdown($name, $values, $selected_value, 'id="'.$name.'"'); //form_helper needed
	}
}

// ------------------------------------------------------------------------

/**
* Checkbox Generator
*
* Generates Label and Checkboxes combo
*
* @access	public
* @param	string	label text (text inside label element)
* @param	string	name of the text element being created
* @param	array	array label-value of the checkbox element
* @param	string	any string that can be placed between the label and checkboxes (ie: <br/>, </div><div>, <hr/>)
* @param	string	any string that can be placed between the checkboxes (ie: <br/>, </div><div>, <hr/>)
* @param	array	array of selected values (ie: array(1, 2, 4, 6) )
* @param	bool	TRUE if label will be placed before the checkbox
* @param	string	space-separated additional classes for the generated checkboxes
*/	
if ( ! function_exists('input_checkbox'))
{
	function input_checkbox($label = '', $name, $labels_values = array(), $separator = '', $checkbox_separator = '', $selected_values = array(), $label_left = FALSE, $custom_classes = 'input_checkboxes')
	{
		$class_name = $name;
		$name .= '[]';
		$keys = array_keys($labels_values);
		$first_id = input_to_url($name.'-'.$labels_values[$keys[0]].'-'.$keys[0]);
		$data = array (
				'name'    => $name,
				'class'   => $class_name.'_checkboxes'.' '.$custom_classes
			);
		$checkboxes = '';
		foreach($labels_values as $v => $l) { //loop checkbox values
			$class_id_name = input_to_url($name.'-'.$l.'-'.$v);
			$data['id']    = $class_id_name;
			$data['value'] = (string) $v;
			if(in_array($v, $selected_values)) {
				$data['checked'] = TRUE;
			} else {
				$data['checked'] = FALSE;
			};
			if($label_left) {
				$checkboxes .= "\n".form_label($l, $class_id_name, array('class' => $data['class'])) . "\n" . form_checkbox($data) . $checkbox_separator;
			} else {
				$checkboxes .= "\n".form_checkbox($data) . "\n" . form_label($l, $class_id_name, array('class' => $data['class'])) . $checkbox_separator;
			};
		}
		if($label != '') {
			$label = form_label($label, $first_id);
		};
		return $label . $separator . $checkboxes; //form_helper needed
	}
}

// ------------------------------------------------------------------------

/**
* Radio button Generator
*
* Generates Label and Radio button combo
*
* @access	public
* @param	string	label text (text inside label element)
* @param	string	name of the text element being created
* @param	array	array values of the radio button element
* @param	string	any string that can be placed between the label and radio button elements (ie: <br/>, </div><div>, <hr/>)
* @param	string	any string that can be placed between the radio buttons (ie: <br/>, </div><div>, <hr/>)
* @param	string	selected value
* @param	bool	TRUE if label will be placed before the radio button
* @param	string	space-separated additional classes for the generated radio buttons
*/	
if ( ! function_exists('input_radio'))
{
	function input_radio($label = '', $name, $labels_values = array(), $separator = '', $checkbox_separator = '', $selected_value = '', $label_left = FALSE, $custom_classes = 'input_radios')
	{
//	var_dump($labels_values);
//	var_dump($selected_value);
		$keys = array_keys($labels_values);
		$first_id = input_to_url($name.'-'.$labels_values[$keys[0]].'-'.$keys[0]);
		$data = array (
				'name'    => $name,
				'class'   => $name.'_radios'.' '.$custom_classes
			);
		$checkboxes = '';
		foreach($labels_values as $v => $l) { //loop checkbox values
			$class_id_name = input_to_url($name.'-'.$l.'-'.$v);
			$data['id']    = $class_id_name;
			$data['value'] = (string) $v; //explicitly define as string for instances of values are numbers

			if ($selected_value === $data['value']) { //is_string to check if bool selected values are sent STRING 0 or 1 instead of BOOLEAN type
				$data['checked'] = TRUE;
			} else {
				$data['checked'] = FALSE;
			};
			if($label_left) {
				$checkboxes .= form_label($l, $class_id_name, array('class' => $data['class'])) . form_radio($data) . $checkbox_separator;
			} else {
				$checkboxes .= form_radio($data) . form_label($l, $class_id_name, array('class' => $data['class'])) . $checkbox_separator;
			};
		}
		if($label != '') {
			$label = form_label($label, $first_id);
		};
		return $label . $separator . $checkboxes; //form_helper needed
	}
}

// ------------------------------------------------------------------------

/**
* Truncate sentences per word
*
* Cut the paragraph into the first few number of characters entered.
* Cuts after the last space provided.
* @author http://www.sitepoint.com/forums/showthread.php?372879-echo-first-few-words-of-a-string&s=d9dc21e2b64b926084bf8d93b5ce8dc9&p=2683764&viewfull=1#post2683764
*
* @access	public
* @param	string	string to be delimited
* @param	integer	maximum length of characters per paragraph
* @param	string	delimiter
* @param	string	string to put to the end of the cut paragraph
*
*/	
if ( ! function_exists('truncate_string'))
{
    function truncate_string ($string = null, $maxlength = 160, $delimiter = ' ', $extension = ' ...') {
    
        // Set the replacement for the "string break" in the wordwrap function
        $cutmarker = "**cut_here**";
    
        // Checking if the given string is longer than $maxlength
        if (strlen($string) > $maxlength) {
    
    	    // Using wordwrap() to set the cutmarker
    	    // NOTE: wordwrap (PHP 4 >= 4.0.2, PHP 5)
    	    $string = wordwrap($string, $maxlength, $cutmarker);

    	    // Exploding the string at the cutmarker, set by wordwrap()
    	    $string = explode($cutmarker, $string);

    	    // Adding $extension to the first value of the array $string, returned by explode()
    	    $string = $string[0] . $extension;
        }
    
        // returning $string
        return $string;
    }
}

// ------------------------------------------------------------------------

/**
* Remove query from url
*
* Remove any query from the URL supplied
*
* @access	public
* @param	string	URL
*
*/	
if ( ! function_exists('remove_query_from_url'))
{
    function remove_query_from_url($url) {
		$p = parse_url($url);
		return $p['scheme'].'://'.$p['host'].$p['path'];
    }
}

// ------------------------------------------------------------------------

/**
* Salutation Select
*
* Generates dropdown list of salutations for selection
*
* @access	public
* @param	string	name of the element being created
* @param	string	selected value
*/	
if ( ! function_exists('salutation_select'))
{
	function salutation_select($name = 'salutation', $selected = '')
	{
		$arr = array(
				''      => '',
				'Mr.'   => 'Mr.',
				'Ms.'   => 'Ms.',
				'Mrs.'  => 'Mrs.',
				'Dr.'   => 'Dr.',
				'Dra.'  => 'Dra.',
				'Atty.' => 'Atty.',
				'Engr.' => 'Engr.'
			);
		return form_dropdown($name, $arr, $selected, 'id="'.$name.'"'); //form_helper needed
	}
}

// ------------------------------------------------------------------------

/**
* Script
*
* Generates a script inclusion of a JavaScript file
* Based on the CodeIgniters original Link Tag.
*
* Author(s): Isern Palaus <ipalaus@ipalaus.es>, Viktor Rutberg <wishie@gmail.com>
* Modified: Francis Luigi Magpantay
* Modification: aesthetics on displayed code
*
* @access	public
* @param	mixed	javascript sources or an array
* @param	string	language
* @param	string	type
* @param	boolean	should index_page be added to the javascript path
* @return	string
*/	
if ( ! function_exists('script_tag'))
{
	function script_tag($src = '', $language = 'javascript', $type = 'text/javascript', $index_page = FALSE)
	{
		$CI =& get_instance();
		$script = '<script ';
		if(is_array($src))
		{
			foreach($src as $v)
			{
				if ($k == 'src' AND strpos($v, '://') === FALSE)
				{
					if ($index_page === TRUE)
					{
						$script .= 'src="'.$CI->config->site_url($v).'"';
					}
					else
					{
						$script .= 'src="'.$CI->config->slash_item('base_url').$v.'"';
					}
				}
				else
				{
					$script .= "$k=\"$v\"";
				}
			}
			$script .= ">\n";
		}
		else
		{
			if ( strpos($src, '://') !== FALSE)
			{
				$script .= 'src="'.$src.'" ';
			}
			elseif ($index_page === TRUE)
			{
				$script .= 'src="'.$CI->config->site_url($src).'" ';
			}
			else
			{
				$script .= 'src="'.$CI->config->slash_item('base_url').$src.'" ';
			}
				
			$script .= 'type="'.$type.'"';
			$script .= '>';
		}
		$script .= '</script>'."\n";

		return $script;
	}
}

// ------------------------------------------------------------------------

/**
* Jquery Open Script Tag
*
* Generates a script element with jQuery document ready script
*
* Author: Francis Luigi Magpantay
*
* @access	public
* @return	string
*/	
if ( ! function_exists('jquery_script_open'))
{
	function jquery_script_open()
	{
		return "<script type='text/javascript'>\n$(function() {\n";
	}
}

// ------------------------------------------------------------------------

/**
* Jquery Close Script Tag 
*
* Generates a script element with jQuery document ready script
*
* Author: Francis Luigi Magpantay
*
* @access	public
* @return	string
*/	
if ( ! function_exists('jquery_script_close'))
{
	function jquery_script_close()
	{
		return "\n});\n</script>";
	}
}

// ------------------------------------------------------------------------

/**
* Generate Big, Small, or Loading Throbber image
*
* Generates throbber image for loading
*
* @access	public
* @param	string	type of throbber to load
* @param	string	id name
* @param	string	class name
* @param	string	styles to be added
* @return	string	throbber image
*/	
if ( ! function_exists('insert_throbber'))
{
	function insert_throbber($type = 'loading', $id = '', $class = 'throbber' , $style = 'display: block')
	{
		$img = 'throbber_loading.gif'; //default
		switch($type) {
			case 'loading':
				$img = 'throbber_loading.gif';
				break;
			case 'big':
				$img = 'throbber_big.gif';
				break;
			case 'small':
				$img = 'throbber_small.gif';
				break;
		}
		return "<img src='".base_url()."pub/img/".$img."' id='$id' class='$class' style='$style' />";
	}
} 

// ------------------------------------------------------------------------

/**
* Format the name for display
*
* Formats the name for display in various places
*
* @access	public
* @param	string	first name
* @param	string	middle name
* @param	string	last name
* @param	bool    if 1, last name first
* @param	bool    if 1, full name will be used. middle name will be full
* @param	bool    if 1, first name and middle name will only display the initials
* @param	bool    if 1, last name will be capitalized
* @return	string	formatted name
*/	
if ( ! function_exists('format_name'))
{
	function format_name($fname, $mname, $lname, $surname_first = 1, $full = null, $initials = null, $capitalize = null) {
		$fname = trim($fname);
		$mname = trim($mname);
		$lname = trim($lname);
		if($initials) {
			$first_name = '';
			$words = preg_split("/\s+/", $fname);
			foreach ($words as $w) {
				$first_name .= strtoupper(substr($w, 0, 1)).'. ';
			}
		} else {
			$first_name = $fname.' ';
		}
		if(trim($mname) != "") {
			if($full && is_null($initials)) {
				$middle_name = $mname.' ';
			} else {
				$middle_name = strtoupper(substr($mname, 0, 1)).'. ';
			}
		} else {
			$middle_name = '';
		}
		$last_name = ($capitalize) ? strtoupper($lname) : $lname;
		
		if($surname_first) {
			$name = $last_name.', '.$first_name.$middle_name;
		} else {
			$name = $first_name.$middle_name.$last_name;
		}
		return $name;
	}
} 

//
//get dentist for list
//
if ( ! function_exists('get_dentist_list'))
{
	function get_dentist_list($id = null, $surname_first = 1, $full = null, $initials = null, $capitalize = null)
	{
		$CI =& get_instance();
		$CI->load->model('Dentist_model', 'dentist');
		return $CI->dentist->get_dentist_list($id, $surname_first, $full, $initials, $capitalize);
	}
} 

//
//get dentist for list for search
//
if ( ! function_exists('get_dentist_for_search'))
{
	function get_dentist_for_search($id = null, $surname_first = 1, $full = null, $initials = null, $capitalize = null)
	{
		$CI =& get_instance();
		$CI->load->model('Dentist_model', 'dentist');
		return $CI->dentist->get_dentist_for_search($id, $surname_first, $full, $initials, $capitalize);
	}
} 


//
//get rate of dentist
//
if ( ! function_exists('get_dentist_grid_rate'))
{
	function get_dentist_grid_rate($dentist_id, $total_net)
	{
		$CI =& get_instance();
		$CI->load->model('Dentist_grid_rate_model', 'dentist_grid_rate');
		return $CI->dentist_grid_rate->get_rate($dentist_id, $total_net, 0);
	}
} 
//
//remove special characters
//
if( ! function_exists('clean'))
{
	function clean($string) {
	$string = str_replace(',', '', $string); // Replaces all , with hyphens.
 

   //return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	return $string; 
  }
}
//
//remove special characters
//
if( ! function_exists('cleanspace'))
{
	function cleanspace($string) {
   $string = str_replace(' ', '', $string); // Replaces all space in string.
 

   return $string; 
}
}
/*
//
//get list of projects for list
//
	
if ( ! function_exists('get_survey_list'))
{
	function get_survey_list($id = null, $hidden = null)
	{
		$CI =& get_instance();
		$CI->load->model('Survey_model', 'survey');
		return $CI->survey->get_survey_list($id, $hidden);
	}
} 

// 
//get list of projects
//
	
if ( ! function_exists('get_survey_info'))
{
	function get_survey_info($id = null, $hidden = null)
	{
		$CI =& get_instance();
		$CI->load->model('Survey_model', 'survey');
		return $CI->survey->get_survey($id, $hidden);
	}
} 
/**/

// ------------------------------------------------------------------------

/**
* Get the start and end date ranges
*
* @access	public
* @param	string	format, 1 = monthly, 2 = weekly, 3 = bi-monthly
* @param	string	year
* @param	string	month
* @param	string	day
* @return	array	from and to dates formatted as Y-m-d
*/	
if ( ! function_exists('date_range'))
{
	function date_range($format, $year, $month, $day) {
		switch($format) {
			case 1: //RD monthly
				if(!empty($month) && !empty($year)) {		
					$oht_from = $year.'-'.$month.'-01';
					$oht_to   = $year.'-'.$month.'-'.date("d", strtotime("last day of previous month"));
				} else {
					$oht_from = date("Y-m-d", strtotime("first day of previous month"));
					$oht_to   = date("Y-m-d", strtotime("last day of previous month"));
				};
				break;
			case 2: //PT weekly
				if(!empty($month) && !empty($day) && !empty($year)) {
					$oht_from = date("Y-m-d", strtotime("last saturday $year-$month-$day +1 day"));			
					$oht_to   = date("Y-m-d", strtotime("$oht_from +6 days"));
				} else {
					$oht_from = date("Y-m-d", strtotime('last saturday -6 days'));
					$oht_to   = date("Y-m-d", strtotime("last saturday"));
				};
				break;
			case 3: //CS bi-monthly
				if(!empty($month) && !empty($day) && !empty($year)) {
					//$oht_from = date("Y-m-d", strtotime("last saturday $year-$month-$day +1 day"));			
					//$oht_to   = date("Y-m-d", strtotime("$oht_from +13 days"));
					if(((int)$day) <= 15) {
						$oht_from = $year.'-'.$month.'-01';
						$oht_to   = $year.'-'.$month.'-15';
					} else {
						$oht_from = $year.'-'.$month.'-16';
						$oht_to   = date("Y-m-t", strtotime($oht_from));
					}
				} else {
					//$oht_from = date("Y-m-d", strtotime('last saturday -13 days'));
					//$oht_to   = date("Y-m-d", strtotime("last saturday"));
					if(date("j") <= 15) {
						$oht_from = date("Y-m-01");
						$oht_to   = date("Y-m-15");
					} else {
						$oht_from = date("Y-m-16");
						$oht_to   = date("Y-m-t");
					}
				};
				break;
		};
		$y = date('Y', strtotime($oht_from));
		$m = date('m', strtotime($oht_from));
		$d = date('d', strtotime($oht_from));
		return array($oht_from, $oht_to, $y, $m, $d);
	}
}

// 
// get if has for approval 
// returns 1 or 0 because it takes a lot of resources to count the reports because it takes into consideration the dentist_category_id..
//
	
if ( ! function_exists('get_for_approval'))
{
	function get_for_approval($dentist_id = null) {
		$CI =& get_instance();
		$CI->load->model('Operation_model', 'operation');
		if($CI->ion_auth->in_group('dentist')) { //override "just in case"...
			$dentist_id = $CI->ion_auth->user($CI->ion_auth->user()->row()->id)->row()->dentist_id;
		};
		$operation_dates = $CI->operation->get_available_operation($dentist_id);
		if(!empty($operation_dates)) {
			return 1;
		} else {
			return 0;
		};
	}
} 

function PHPexecInBG($cmd) { 
    if (substr(php_uname(), 0, 7) == "Windows"){ 
        pclose(popen("start /B D:/wamp/bin/php/php5.5.12/php.exe ". $cmd, "r"));  
    } 
    else { 
        exec('php '. $cmd . " > /dev/null &");
    } 
} 


if ( ! function_exists('email_sender')){

	function email_sender($dentist_upload,$object){
		$object->load->model('excel_upload_model');
		$object->load->model('Dentist_model', 'dentist');
		$output = '';		
		$error_code='';
		$user_id= $object->ion_auth->user()->row()->id;
		$user_email= $object->ion_auth->user()->row()->email;
		$send_email=TRUE;	
		$object->load->helper('url');
		foreach($dentist_upload as $dentist_id){
			$today = date("Y-m-d H:i:s");
			$data['date'] = $today;
			$month_today=date("m");
			$emails=$object->excel_upload_model->search_dentist_emails($dentist_id);
			$dentist_details = $object->dentist->get_dentist($dentist_id);
			$data['dentist_name'] = 'Dr. '.format_name($dentist_details[$dentist_id]['first_name'], $dentist_details[$dentist_id]['middle_name'], $dentist_details[$dentist_id]['last_name'], 0, 1);
			$data['dentist_category_id'] = $dentist_category_id = $data['dentist_details'][$dentist_id]['dentist_category_id'];
			switch($dentist_category_id) {
				case '1': //RD
					$date = explode('-', $object->excel_upload_model->search_treatment_date($dentist_id,$month_today));
					$year = $date[0];
					$month   = $date[1];
					$day  = $date[2];
					$object->excel_accounting($dentist_id, $year, $month, $day, null, 1);
					break;
				case '2'://PT
					$getdate = explode('-', $today);
					$year=$getdate[0];
					$month=$getdate[1];
					$days=getMondays($year,$month);
					foreach($days as $d){
						$object->excel_accounting($dentist_id, $year, $month, $d, null, 1);
					}
					break;
				case '3'://CS
				$date = explode('-', $object->excel_upload_model->search_treatment_date($dentist_id,$month_today));
				$year = $date[0];
				$month   = $date[1];
				$day  = $date[2];
				if(($day>=1) && ($day<=15)){
					$day  = 1;					
					$object->excel_accounting($dentist_id, $year, $month, $day, null, 1);
					$day  = 16;	
					$object->excel_accounting($dentist_id, $year, $month, $day, null, 1);
				}else{
					$day  = 16;
					$object->excel_accounting($dentist_id, $year, $month, $day, null, 1);
				}
				break;
					};					
			if(empty($emails)){
			}else{
				if(is_null($emails[$dentist_id])){
					// no email include
					$send_email=FALSE;
				}else{
					$email_to=$emails[$dentist_id];

					$send_email=TRUE;
				}
			}
			if($send_email==FALSE){
				$status=0;
				$error_code='DENTIST HAS NO EMAIL ADDRESS  ASSIGNED';
				$email_to='';
			}else{
				$object->email->clear();
				$object->email->from('support@leongroup.me', 'LEON SUPPORT');
				$object->email->to(''.$email_to.''); 
				//$this->email->cc('trimantra@gmail.com');
				$object->email->subject('PLEASE VIEW AND APPROVE YOUR COMMISSION');	
				$data['link']=base_url("/commission/for_approval/$dentist_id");
				$data['link']=base_url("/commission/for_approval/$dentist_id");
				$object->email->attach(getcwd() . '/zip_reports/report'.$year.$month_today.'.zip');
				$html = $object->load->view('emailer', $data, $send_email);		
				//$this->email->message('TO VIEW AND APPROVE YOUR COMMISSION PLEASE CLICK THE LINK BELOW: <br><br>'.base_url("/commission/view/$dentist_id").' <br> <br> THANKS');    
				$object->email->message($html);    
				$status=1;
				$error_code=$object->email->send();
				if (!$error_code){
					// Generate error
					$status=0;		
				}
				//print_r($this->email->print_debugger());
			
			};//if statement
			$data = array(
				"dentist_id" => $dentist_id,
				"email" => $email_to,
				"status" => $status,
				"error_code" => $error_code,
				"created_on" => $today,
				"created_by" => $user_id,
			);
			
			if($send_email==FALSE){ //no email
				$output .= "DENTIST HAS NO EMAIL ADDRESS  ASSIGNED<br>";
			}else if($status == 0){//error in sending
				$output .= "HAVING PROBLEM SENDING EMAIL TO ".$data['dentist_name'].'-Email['.$email_to."]<br>";
			}else{
				$output .= "EMAIL HAS BEEN SUCCESSFULLY SEND TO ".$data['dentist_name'].'-Email['.$email_to."]<br>";
			}
			//in the loop nageexport na siya sa server
			//after all the dentist are emailed send email in the uploader 
			//
			//
			$object->session->set_flashdata('output', $output);		
			$object->excel_upload_model->add_dentist_email_notification($data);
		};//foreach end   	
		//zipping
		$filename = realpath(dirname(__FILE__)."/../../comsoft/zip_reports/").'/report'.$year.$month.'.zip';
		$folder_to_zip = realpath(dirname(__FILE__)."/../../comsoft/reports/");
		PHPexecInBG(realpath(dirname(__FILE__).'/../../application/hooks/').'/zip_reports.php '.$filename.' '.$folder_to_zip);
		$files = glob($folder_to_zip.'/*'); // get all file names
		foreach($files as $file){ // iterate files
		  if(is_file($file))
			unlink($file); // delete file
		}
		//email admin
		$object->email->clear();
		$object->email->from('support@leongroup.me', 'LEON SUPPORT');
		$object->email->to(''.$user_email.''); 
		//$this->email->cc('trimantra@gmail.com');
		$object->email->subject('EMAIL SUCCESS');	
		$data['is_admin']=true;
		$data['output']=$output;
		$data['link']=base_url("/commission/for_approval/");
		$html = $object->load->view('emailer', $data, $send_email);		
		$object->email->message($html);    
		$status=1;
		$error_code=$object->email->send();
		if (!$error_code){
			// Generate error
			$status=0;		
		};
		return $output;
	}

}

if ( ! function_exists('email_admin')){

	function email_admin($dentist_id=null,$object){
		$object->load->model('Dentist_model', 'dentist');
		$today = date("Y-m-d H:i:s");
		$data['date'] = $today;
		$output = '';		
		$error_code='';
		$user_id= $object->ion_auth->user()->row()->id;
		$user_email= $object->ion_auth->user()->row()->email;
		$send_email=TRUE;	
		$object->load->helper('url');
		//email admin
		$object->email->clear();
		$object->email->from('support@leongroup.me', 'LEON SUPPORT');
		$object->email->to(''.$user_email.''); 
		//$this->email->cc('trimantra@gmail.com');
		$object->email->subject('EMAIL SUCCESS');	
		$dentist_details = $object->dentist->get_dentist($dentist_id);
		$data['dentist_name'] = 'Dr. '.format_name($dentist_details[$dentist_id]['first_name'], $dentist_details[$dentist_id]['middle_name'], $dentist_details[$dentist_id]['last_name'], 0, 1);
			
		$data['is_admin']=true;
		$data['for_approval']=true;
		$data['output']=$output;
		$data['link']=base_url("/commission/view/".$dentist_id."");
		$html = $object->load->view('emailer', $data, $send_email);		
		$object->email->message($html);    
		$status=1;
		$error_code=$object->email->send();
		if (!$error_code){
			// Generate error
			$status=0;		
		};
		return $output;
	}

}

//get every mondays of a month
if ( ! function_exists('getMondays')){

	function getMondays($year, $month) {
		$mondays = array();
		# First weekday in specified month: 1 = monday, 7 = sunday
		$firstDay = date('N', mktime(0, 0, 0, $month, 1, $year));
		/* Add 0 days if monday ... 6 days if tuesday, 1 day if sunday
			to get the first monday in month */
		$addDays = (8 - $firstDay) % 7;
		$mondays[] = date('d', mktime(0, 0, 0, $month, 1 + $addDays, $year));
		
		$nextMonth = mktime(0, 0, 0, $month + 1, 1, $year);

		# Just add 7 days per iteration to get the date of the subsequent week
		for ($week = 1, $time = mktime(0, 0, 0, $month, 1 + $addDays + $week * 7, $year);$time < $nextMonth; ++$week, $time = mktime(0, 0, 0, $month, 1 + $addDays + $week * 7, $year)) {
			$mondays[] = date('d', $time);
		}
		
		return $mondays;
}

}

//get every mondays of a month
if ( ! function_exists('days_in_month')){ 

	function days_in_month($month, $year) 
	{ 
	// calculate number of days in a month 
	return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31); 
	} 
}

/* End of file constructor_helper.php */
/* Location: ./application/helpers/constructor_helper.php */