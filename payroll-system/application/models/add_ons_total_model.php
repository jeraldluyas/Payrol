<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Add_ons_total_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_add_ons_total($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('add_ons_total');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'emp_id'             => $r['emp_id'],
					'temp_payslip_id'        => $r['temp_payslip_id'],
					'date_start'           => $r['date_start'],
					'date_end'         => $r['date_end'],
					'add_ons_id'         => $r['add_ons_id'],
					'value'         => $r['value'],
				);
		}
		return $result;
	}

	function get_add_ons_total_list($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('add_ons_total');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = $r['add_ons_total_total'];
		}
		return $result;
	}

	function add($data)
	{
		$this->db->insert('add_ons_total', $data); 
		return $this->db->insert_id();
	}

	function update($data) {
		$this->db->where('id', $data['id']);
		$this->db->update('add_ons_total', $data);
		return $this->db->affected_rows();
	}

	function delete($data) {
		$this->db->delete('add_ons_total', $data);
		return $this->db->affected_rows();
	}
}

/* End of file add_ons_total_model.php */
/* Location: ./application/models/add_ons_total_model.php */