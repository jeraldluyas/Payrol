<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deduction_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_deduction($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('deduction');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'label'                 => $r['label'],
					'description'                 => $r['description'],					
					'value'           => $r['value']
				);
		}
		return $result;
	}

	function get_deduction_list($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('deduction');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'description' => $r['description'],
					'id' => $r['id'], 
					);
		}
		return $result;
	}

	function add($data)
	{
		$this->db->insert('deduction', $data); 
		return $this->db->insert_id();
	}

	function update($data) {
		$this->db->where('id', $data['id']);
		$this->db->update('deduction', $data);
		return $this->db->affected_rows();
	}

	function delete($data) {
		$this->db->delete('deduction', $data);
		return $this->db->affected_rows();
	}
}

/* End of file deduction_model.php */
/* Location: ./application/models/deduction_model.php */