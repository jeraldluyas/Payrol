<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deduction_total_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_deduction_total($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('deduction_total');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'emp_id'             => $r['emp_id'],
					'temp_payslip_id'        => $r['temp_payslip_id'],
					'date_start'           => $r['date_start'],
					'date_end'         => $r['date_end'],
					'deduction_id'         => $r['deduction_id'],
					'value'         => $r['value'],
				);
		}
		return $result;
	}

	function get_deduction_total_list($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('deduction_total');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = $r['extras_total'];
		}
		return $result;
	}

	function add($data)
	{
		$this->db->insert('deduction_total', $data); 
		return $this->db->insert_id();
	}

	function update($data) {
		$this->db->where('id', $data['id']);
		$this->db->update('deduction_total', $data);
		return $this->db->affected_rows();
	}

	function delete($data) {
		$this->db->delete('deduction_total', $data);
		return $this->db->affected_rows();
	}
}

/* End of file deduction_total_model.php */
/* Location: ./application/models/deduction_total_model.php */