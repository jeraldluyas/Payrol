<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class department_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_department($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('department');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'label'                 => $r['label'],
					'description'                 => $r['description'],
				);
		}
		return $result;
	}

	function get_department_list($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('department');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = $r['label'];
		}
		return $result;
	}

	function add($data)
	{
		$this->db->insert('department', $data); 
		return $this->db->insert_id();
	}

	function update($data) {
		$id=$data['id'];
		unset($data['id']);
		$this->db->where('id', $id);
		$this->db->update('department', $data);
		return $this->db->affected_rows();
	}

	function delete($data) {
		$this->db->delete('department', $data);
		return $this->db->affected_rows();
	}
}

/* End of file department_model.php */
/* Location: ./application/models/department_model.php */