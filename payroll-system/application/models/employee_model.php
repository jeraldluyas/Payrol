<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_employee($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('employee');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'first_name'                 => $r['first_name'],
					'last_name'           => $r['last_name'],
					'position'           => $r['position'],
					'salary_grade_id'           => $r['salary_grade_id'],					
					'department_id'           => $r['department_id'],				
					'status'           => $r['status'],				
					'employee_id'           => $r['employee_id'],	
					'birthdate'           => $r['birthdate'],	
					'city'           => $r['city'],	
					'mobile'           => $r['mobile'],
					'phone'           => $r['phone'],
					'email'           => $r['email'],
					'civil_status'           => $r['civil_status'],
					'sss_no'           => $r['sss_no'],
					'philhealth_no'           => $r['philhealth_no'],
					'pagibig_no'           => $r['pagibig_no'],
					'tax_id_no'           => $r['tax_id_no'],
					'birth_cert'           => $r['birth_cert'],
					'baptismal'           => $r['baptismal'],
					'marriage_cert'           => $r['marriage_cert'],
					'brgy_clearance'           => $r['brgy_clearance'],
					'police_clearance'           => $r['police_clearance'],
					'nbi'           => $r['nbi'],
					'xray'           => $r['xray'],
					'result'           => $r['result'],
					'sputum'           => $r['sputum'],
					'fecalysis'           => $r['fecalysis'],
					'health_card'           => $r['health_card'],
					'drug_test'           => $r['drug_test'],
					'eye_test'           => $r['eye_test'],
					'bio_date'           => $r['bio_date'],
					'resume'           => $r['resume'],
					'id_picture'           => $r['id_picture'],
					'info_sheet'           => $r['info_sheet'],
					'contract'           => $r['contract'],
					'date_hired'           => $r['date_hired'],

				);
		}
		return $result;
	}

	function get_employee2($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('employee');
		if($id) {
			$this->db->where('employee_id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['employee_id']] = array(
					'id' => $r['id'],
					'first_name'                 => $r['first_name'],
					'last_name'           => $r['last_name'],
					'position'           => $r['position'],
					'salary_grade_id'           => $r['salary_grade_id'],					
					'department_id'           => $r['department_id'],				
					'status'           => $r['status'],				
					'employee_id'           => $r['employee_id'],	
					'birthdate'           => $r['birthdate'],	
					'city'           => $r['city'],	
					'mobile'           => $r['mobile'],
					'phone'           => $r['phone'],
					'email'           => $r['email'],
					'civil_status'           => $r['civil_status'],
					'sss_no'           => $r['sss_no'],
					'philhealth_no'           => $r['philhealth_no'],
					'pagibig_no'           => $r['pagibig_no'],
					'tax_id_no'           => $r['tax_id_no'],
					'birth_cert'           => $r['birth_cert'],
					'baptismal'           => $r['baptismal'],
					'brgy_clearance'           => $r['brgy_clearance'],
					'marriage_cert'           => $r['marriage_cert'],
					'police_clearance'           => $r['police_clearance'],
					'nbi'           => $r['nbi'],
					'xray'           => $r['xray'],
					'result'           => $r['result'],
					'sputum'           => $r['sputum'],
					'fecalysis'           => $r['fecalysis'],
					'health_card'           => $r['health_card'],
					'drug_test'           => $r['drug_test'],
					'eye_test'           => $r['eye_test'],
					'bio_date'           => $r['bio_date'],
					'resume'           => $r['resume'],
					'id_picture'           => $r['id_picture'],
					'info_sheet'           => $r['info_sheet'],
					'contract'           => $r['contract'],
					'date_hired'           => $r['date_hired'],
					
				);
		}
		return $result;
	}

	public function get_new_id(){
		$flager = false;
		$sql = "SELECT id FROM employee ORDER BY id DESC LIMIT 1";
		$query = $this->db->query($sql);
			if($query->num_rows() > 0){	
						foreach($query->result() as $row){
						$userid = intval($row->id)+1;
							while(!$flager){
								$len = strlen($userid);
								for($i=$len;$i<5;$i++){
									$userid="0".$userid;
								}
								if(!$this->checkdup($userid)){
									$userid = intval($userid)+1;
									$flager = false;
								}else{
									$flager = true;
								}
							}
								return date("y").'-'.$userid;
						}
				
			
			}
		return $query->free_result();
	}

	public function checkdup($id){
		$sql = "SELECT id FROM employee where id = ?";
		$query = $this->db->query($sql,array($id));
		
			if($query->num_rows() > 0){
				return false;
			}else{
				return true;
			}
	}
	function get_employee_reports($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('employee');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'first_name'                 => $r['first_name'],
					'last_name'           => $r['last_name'],
					'salary_grade_id'           => $r['salary_grade_id'],					
					'department_id'           => $r['department_id'],				
					'employee_id'           => $r['employee_id'],
				);
		}
		return $result;
	}
	function get_employee_sheller_reports($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('employee');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->where('salary_grade_id =', '1');
		$this->db->or_where('salary_grade_id =', '2');
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'first_name'                 => $r['first_name'],
					'last_name'           => $r['last_name'],
					'salary_grade_id'           => $r['salary_grade_id'],					
					'department_id'           => $r['department_id'],				
					'employee_id'           => $r['employee_id'],
				);
		}
		return $result;
	}
	function get_employee_report($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('employee');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'first_name'                 => $r['first_name'],
					'last_name'           => $r['last_name'],
					'salary_grade_id'           => $r['salary_grade_id'],					
					'department_id'           => $r['department_id'],				
					'employee_id'           => $r['employee_id'],
				);
		}
		return $result;
	}

	function get_employee_list($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('employee');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = $r['first_name'].' '.$r['last_name'];
		}
		return $result;
	}

	function add($data)
	{
		$this->db->insert('employee', $data); 
		return $this->db->insert_id();
	}

	function update($data) {
		$this->db->where('id', $data['id']);
		$this->db->update('employee', $data);
		return $this->db->affected_rows();
	}

	function delete($data) {
		$this->db->delete('employee', $data);
		return $this->db->affected_rows();
	}
	function update_status($data) {
		$this->db->where('id', $data['id']);
		unset($data['status']);
		$data['status'] = 0;
		$this->db->update('employee', $data);
		return $this->db->affected_rows();
	}
}

/* End of file employee_model.php */
/* Location: ./application/models/employee_model.php */