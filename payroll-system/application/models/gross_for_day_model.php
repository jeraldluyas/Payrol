<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gross_for_day_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_gross_for_day($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('gross_for_day');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'temp_emp_id'             => $r['temp_emp_id'],
					'work_on_id'        => $r['work_on_id'],
					'pay_equals1'           => $r['pay_equals1'],
					'pay_equals0'         => $r['pay_equals0'],
					'overtime1'         => $r['overtime1'],
					'overtime0'         => $r['overtime0'],
					'gross_day'         => $r['gross_day'],
					'date_starts'         => $r['date_starts'],
					'date_ends'         => $r['date_ends'],
				);
		}
		return $result;
	}

	function get_gross_for_day_list($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('gross_for_day');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = $r['extras_total'];
		}
		return $result;
	}

	function add($data)
	{
		$this->db->insert('gross_for_day', $data); 
		return $this->db->insert_id();
	}

	function update($data) {
		$this->db->where('id', $data['id']);
		$this->db->update('gross_for_day', $data);
		return $this->db->affected_rows();
	}

	function delete($data) {
		$this->db->delete('gross_for_day', $data);
		return $this->db->affected_rows();
	}
}

/* End of file gross_for_day_model.php */
/* Location: ./application/models/gross_for_day_model.php */