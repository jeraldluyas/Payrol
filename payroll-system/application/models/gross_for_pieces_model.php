<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class gross_for_pieces_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_gross_for_pieces($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('gross_for_pieces');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'temp_emp_id'             => $r['temp_emp_id'],
					'work_on_id'        => $r['work_on_id'],
					'date_starts'         => $r['date_starts'],
					'date_ends'         => $r['date_ends'],
					'time_starts'         => $r['time_starts'],
					'time_ends'         => $r['time_ends'],
					'value'         => $r['value'],
				);
		}
		return $result;
	}

	function get_gross_for_pieces_list($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('gross_for_pieces');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = $r['extras_total'];
		}
		return $result;
	}

	function add($data)
	{
		$this->db->insert('gross_for_pieces', $data); 
		return $this->db->insert_id();
	}
	function add_per_day($data)
	{
		$this->db->insert('gross_for_piece_for_day', $data); 
		return $this->db->insert_id();
	}

	function update($data, $id) {
		$this->db->where('id', $id);
		$this->db->update('gross_for_pieces', $data);
		return $this->db->affected_rows();
	}

	function delete($data) {
		$this->db->delete('gross_for_pieces', $data);
		return $this->db->affected_rows();
	}
}

/* End of file gross_for_pieces_model.php */
/* Location: ./application/models/gross_for_pieces_model.php */