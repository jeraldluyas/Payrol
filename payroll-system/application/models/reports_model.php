<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}
	function get_salary_details($id) {
		$result = array();
		$this->db->from('temp_payslip');
		if($id) {
			$this->db->where('id', $id);
		}
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'emp_id' => $r['emp_id'],
					'gross_total' => $r['gross_total'],
					'deduction_total' => $r['deduction_total'],
					'extras_total' => $r['extras_total'],
			);
		}
		return $result;	
	}
	function get_payslip($id = null, $salary_grade_id = null) {
		$result = array();
		$gross_for_pieces = false;
		$this->db->select('temp_payslip.id, temp_payslip.emp_id, temp_payslip.gross_total, temp_payslip.deduction_total, temp_payslip.extras_total, gross_for_pieces.value1, gross_for_pieces.value2, gross_for_pieces.date_starts, gross_for_pieces.date_ends, gross_for_piece_for_day.day ,gross_for_piece_for_day.values, gross_for_day.day , gross_for_day.pay_equals1_hours , gross_for_day.pay_equals1, gross_for_day.pay_equals0_hours, gross_for_day.pay_equals0, gross_for_day.overtime1_hours, gross_for_day.overtime1, gross_for_day.overtime0_hours, gross_for_day.pay_equals0, gross_for_day.gross_day, gross_for_day.date_starts, gross_for_day.date_ends, gross_for_day.date_ends, gross_for_day.total_pay_for_day, gross_for_day.time_starts, gross_for_day.time_ends');

		$this->db->from('temp_payslip');
		//$this->db->order_by('label', 'id');
		$this->db->join('employee', 'employee.id = temp_payslip.emp_id', 'inner');
		// if($salary_grade_id) {
		// 	if($salary_grade_id == 1 OR $salary_grade_id == 2) {
		// 		$gross_for_pieces = true;
		// 		}
		// 	else {
		// 		$gross_for_pieces = false;
		// 		$this->db->join('gross_for_day', 'temp_payslip.id = gross_for_day.temp_payslip_id');
		// 	}
		// } else {
		// 	$this->db->join('gross_for_day', 'temp_payslip.id = gross_for_day.temp_payslip_id');	
		// }
		$this->db->join('gross_for_pieces', 'temp_payslip.id = gross_for_pieces.temp_payslip_id' , 'left');
		$this->db->join('gross_for_piece_for_day', 'gross_for_pieces.id = gross_for_piece_for_day.gross_for_pieces_id' , 'left');
		$this->db->join('gross_for_day', 'temp_payslip.id = gross_for_day.temp_payslip_id', 'inner');
		if($id) {
			$this->db->where('employee.id', $id);
		}		
		$this->db->group_by("temp_payslip.id"); 
		$query = $this->db->get();
		
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'emp_id' => $r['emp_id'],
					'gross_total' => $r['gross_total'],
					'deduction_total' => $r['deduction_total'],
					'extras_total' => $r['extras_total'],
					'value1' => $r['value1'],
					'value2' => $r['value2'],
					'date_starts' => $r['date_starts'],
					'date_ends' => $r['date_ends'],
					'day' => $r['day'],
					'values' => $r['values'],
					'pay_equals1_hours' => $r['pay_equals1_hours'],
					'pay_equals1' => $r['pay_equals1'],
					'pay_equals0_hours' => $r['pay_equals0_hours'],
					'overtime1_hours' => $r['overtime1_hours'],
					'overtime1' => $r['overtime1'],
					'overtime0_hours' => $r['overtime0_hours'],
					'gross_day' => $r['gross_day'],
					'total_pay_for_day' => $r['total_pay_for_day'],
					'time_starts' => $r['time_starts'],
					'time_ends' => $r['time_ends'],
				);
			}
		return $result;
	}
	function get_payslip2($id = null, $salary_grade_id = null) {
		$result = array();
		$gross_for_pieces = false;
		$this->db->select('temp_payslip.id, temp_payslip.emp_id, temp_payslip.gross_total, temp_payslip.deduction_total, temp_payslip.extras_total, gross_for_pieces.value1, gross_for_pieces.value2, gross_for_pieces.date_starts, gross_for_pieces.date_ends, gross_for_piece_for_day.day ,gross_for_piece_for_day.values, gross_for_day.day , gross_for_day.pay_equals1_hours , gross_for_day.pay_equals1, gross_for_day.pay_equals0_hours, gross_for_day.pay_equals0, gross_for_day.overtime1_hours, gross_for_day.overtime1, gross_for_day.overtime0_hours, gross_for_day.pay_equals0, gross_for_day.gross_day, gross_for_day.date_starts, gross_for_day.date_ends, gross_for_day.date_ends, gross_for_day.total_pay_for_day, gross_for_day.time_starts, gross_for_day.time_ends');

		$this->db->from('temp_payslip');
		//$this->db->order_by('label', 'id');
		$this->db->join('employee', 'employee.id = temp_payslip.emp_id', 'inner');
		// if($salary_grade_id) {
		// 	if($salary_grade_id == 1 OR $salary_grade_id == 2) {
		// 		$gross_for_pieces = true;
		// 		}
		// 	else {
		// 		$gross_for_pieces = false;
		// 		$this->db->join('gross_for_day', 'temp_payslip.id = gross_for_day.temp_payslip_id');
		// 	}
		// } else {
		// 	$this->db->join('gross_for_day', 'temp_payslip.id = gross_for_day.temp_payslip_id');	
		// }
		$this->db->join('gross_for_pieces', 'temp_payslip.id = gross_for_pieces.temp_payslip_id' , 'left');
		$this->db->join('gross_for_piece_for_day', 'gross_for_pieces.id = gross_for_piece_for_day.gross_for_pieces_id' , 'left');
		$this->db->join('gross_for_day', 'temp_payslip.id = gross_for_day.temp_payslip_id', 'inner');
		if($id) {
			$this->db->where('employee.id', $id);
		}		
		$this->db->group_by("temp_payslip.id"); 
		$query = $this->db->get();
		// echo $this->db->last_query();

		// exit();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'emp_id' => $r['emp_id'],
					'gross_total' => $r['gross_total'],
					'deduction_total' => $r['deduction_total'],
					'extras_total' => $r['extras_total'],
					'date_starts' => $r['date_starts'],
					'date_ends' => $r['date_ends'],
					'day' => $r['day'],
					'values' => $r['values'],
					'pay_equals1_hours' => $r['pay_equals1_hours'],
					'pay_equals1' => $r['pay_equals1'],
					'pay_equals0_hours' => $r['pay_equals0_hours'],
					'overtime1_hours' => $r['overtime1_hours'],
					'overtime1' => $r['overtime1'],
					'overtime0_hours' => $r['overtime0_hours'],
					'gross_day' => $r['gross_day'],
					'total_pay_for_day' => $r['total_pay_for_day'],
					'time_starts' => $r['time_starts'],
					'time_ends' => $r['time_ends'],
				);
			}
		return $result;
	}
	function get_reports_for_day($id = null, $salary_grade_id = null) {
		$result = array();
		$gross_for_pieces = false;
		$this->db->select();
		$this->db->from('employee');
		// $this->db->where('employee.salary_grade_id <>', '1');
		// $this->db->where('employee.salary_grade_id <>', '2');
		if($id) {
			$this->db->where('temp_payslip.emp_id', $id);
		}
		//$this->db->order_by('label', 'id');
		$this->db->join('temp_payslip', 'employee.id = temp_payslip.emp_id');
		$this->db->join('gross_for_day', 'temp_payslip.id = gross_for_day.temp_payslip_id');
		
		$query = $this->db->get();
//		echo $this->db->last_query();
			foreach ($query->result_array() as $r) {
			$result[$r['emp_id']] = array(
					'id' => $r['id'],
					'emp_id' => $r['emp_id'],
					'date_starts' => $r['date_starts'],
					'date_ends' => $r['date_ends'],
					'first_name' => $r['first_name'],
					'last_name' => $r['last_name'],
					'pay_equals1_hours' => $r['pay_equals1_hours'],
					'pay_equals0_hours' => $r['pay_equals0_hours'],
					'overtime1_hours' => $r['overtime1_hours'],
					'overtime0_hours' => $r['overtime0_hours'],
					'salary_grade_id' => $r['salary_grade_id'],
					'work_on_id' => $r['work_on_id'],
					'day' => $r['day'],
				);
			}
		return $result;
	}

	function get_reports_list($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('reports');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = $r['description'];
		}
		return $result;
	}

	function add($data)
	{
		$this->db->insert('reports', $data); 
		return $this->db->insert_id();
	}

	function update($data) {
		$this->db->where('id', $data['id']);
		$this->db->update('reports', $data);
		return $this->db->affected_rows();
	}

	function delete($data) {
		$this->db->delete('reports', $data);
		return $this->db->affected_rows();
	}
}

/* End of file reports_model.php */
/* Location: ./application/models/reports_model.php */