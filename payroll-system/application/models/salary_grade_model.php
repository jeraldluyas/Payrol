<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class salary_grade_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_salary_grade($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('salary_grade');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'bracket'             => $r['bracket'],
					'rate_per_day'        => $r['rate_per_day'],
					'months_13'           => $r['months_13'],
					'sil_day_314'         => $r['sil_day_314'],
					'sil_day_313'         => $r['sil_day_313'],
					'remarks'             => $r['remarks'],
				);
		}
		return $result;
	}

	function get_salary_grade_list($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('salary_grade');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = $r['bracket'];
		}
		return $result;
	}
	function get_salary_grade_rate($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('salary_grade');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['bracket']] = $r['rate_per_day'];
		}
		return $result;
	}

	function add($data)
	{
		$this->db->insert('salary_grade', $data); 
		return $this->db->insert_id();
	}

	function update($data) {
		$this->db->where('id', $data['id']);
		$this->db->update('salary_grade', $data);
		return $this->db->affected_rows();
	}

	function delete($data) {
		$this->db->delete('salary_grade', $data);
		return $this->db->affected_rows();
	}
}

/* End of file salary_grade_model.php */
/* Location: ./application/models/salary_grade_model.php */