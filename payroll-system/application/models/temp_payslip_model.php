<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Temp_payslip_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_temp_payslip($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('temp_payslip');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'emp_id'             => $r['bracket'],
					'gross_total'        => $r['gross_total'],
					'deduction_total'           => $r['deduction_total'],
					'extras_total'         => $r['extras_total'],
				);
		}
		return $result;
	}

	function get_temp_payslip_list($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('temp_payslip');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = $r['extras_total'];
		}
		return $result;
	}

	function add($data)
	{
		$this->db->query("SET FOREIGN_KEY_CHECKS = 0");
		$this->db->insert('temp_payslip', $data); 
		//$this->db->query("SET FOREIGN_KEY_CHECKS = 1");
		//echo $this->db->last_query();
		 $id=$this->db->insert_id();
		 return $id;
	}

	function update($data, $id) {
		$this->db->where('id', $id);
		$this->db->update('temp_payslip', $data);
		return $this->db->affected_rows();
	}

	function delete($data) {
		$this->db->delete('temp_payslip', $data);
		return $this->db->affected_rows();
	}
}

/* End of file temp_payslip_model.php */
/* Location: ./application/models/temp_payslip_model.php */