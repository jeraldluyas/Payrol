<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Work_on_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_work_on($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('work_on');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'label'                 => $r['label'],
					'description'           => $r['description']
				);
		}
		return $result;
	}

	function get_work_on_list($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('work_on');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = $r['description'];
		}
		return $result;
	}

	function add($data)
	{
		$this->db->insert('work_on', $data); 
		return $this->db->insert_id();
	}

	function update($data) {
		$this->db->where('id', $data['id']);
		$this->db->update('work_on', $data);
		return $this->db->affected_rows();
	}

	function delete($data) {
		$this->db->delete('work_on', $data);
		return $this->db->affected_rows();
	}
}

/* End of file work_on_model.php */
/* Location: ./application/models/work_on_model.php */