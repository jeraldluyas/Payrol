<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Work_on_rates_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_work_on_rates($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('work_on_rates');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'work_on_id'                 => $r['work_on_id'],
					'shift'           => $r['shift'],
					'description'           => $r['description'],
					'value'           => $r['value']
				);
		}
		return $result;
	}

	function get_work_on_rates_list($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('work_on_rates');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('description', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = $r['description'];
		}
		return $result;
	}
	function get_work_on_rates_list_based_on_id($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('work_on_rates');
		if($id) {
			$this->db->where('work_on_id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {

				$result[] = array(
					'id' => $r['id'],
					'work_on_id'                 => $r['work_on_id'],
					'shift'           => $r['shift'],
					'description'           => $r['description'],
					'value'           => $r['value']
				);


		}
		return $result;
	}

	function add($data)
	{
		$this->db->insert('work_on_rates', $data); 
		return $this->db->insert_id();
	}

	function update($data) {
		$this->db->where('id', $data['id']);
		$this->db->update('work_on_rates', $data);
		return $this->db->affected_rows();
	}

	function delete($data) {
		$this->db->delete('work_on_rates', $data);
		return $this->db->affected_rows();
	}
}

/* End of file work_on_rates_model.php */
/* Location: ./application/models/work_on_rates_model.php */