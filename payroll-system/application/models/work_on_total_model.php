<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Work_on_total_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function get_work_on_total($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('work_on_total');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = array(
					'id' => $r['id'],
					'temp_payslip_id' => $r['temp_payslip_id'],
					'work_on_id' => $r['work_on_id'],
					'pay_equals1' => $r['pay_equals1'],
					'pay_equals2' => $r['pay_equals2'],					
					'overtime1' => $r['overtime1'],				
					'overtime0' => $r['overtime0'],				
				);
		}
		return $result;
	}

	function get_work_on_total_list($id = null) {
		$result = array();
		$this->db->select();
		$this->db->from('work_on_total');
		if($id) {
			$this->db->where('id', $id);
		}
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$result[$r['id']] = $r['first_name'].' '.$r['last_name'];
		}
		return $result;
	}

	function add($data)
	{
		$this->db->insert('work_on_total', $data); 
		return $this->db->insert_id();
	}

	function update($data) {
		$this->db->where('id', $data['id']);
		$this->db->update('work_on_total', $data);
		return $this->db->affected_rows();
	}

	function delete($data) {
		$this->db->delete('work_on_total', $data);
		return $this->db->affected_rows();
	}
	function update_status($data) {
		$this->db->where('id', $data['id']);
		unset($data['status']);
		$data['status'] = 0;
		$this->db->update('work_on_total', $data);
		return $this->db->affected_rows();
	}
}

/* End of file work_on_total_model.php */
/* Location: ./application/models/work_on_total_model.php */