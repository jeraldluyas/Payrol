<section id="content">
	<section class="hbox stretch">
		<aside class="aside-md bg-white b-r" id="subNav">
			<div class="wrapper b-b header">
				Admin Menu
			</div>
			<ul class="nav">
				<li class="b-b b-light"><a href="<?php echo base_url('auth'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Admin Accounts</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('/department'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Department</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('salary_grade'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Salary Grade</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('work_on'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work On</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('work_on_rates'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work On rates</a></li>
			</ul>
		</aside>
		<aside>
			<section class="vbox">
				<section class="scrollable wrapper w-f">
					<section class="panel panel-default">
						<div class="table-responsive">
							<table class="table table-striped m-b-none">
								<thead>
									<tr>
										
				<th>Bracket</th>
				<th>Rate Per Day</th>
				<th>13 Months</th>
				<th>sil/day 314</th>
				<th>sil/day 313</th>
				<th>remarks</th>
				<th>Action</th>

								</tr>
								</thead>
								<tbody>
		<?php 
		if(!isset($item)) {
			?>
			<tr>
				<td colspan='3' align='center' style='padding: 15px'><i>No item to display.</i></td>
			</tr>	
			<?php
		} else {
			$table_id = 'id';
			foreach ($item as $i) { ?>
			<tr>
				<td><?php echo $i['bracket']; ?></td>
				<td><?php echo $i['rate_per_day']; ?></td>
				<td><?php echo $i['months_13']; ?></td>
				<td><?php echo $i['sil_day_314']; ?></td>
				<td><?php echo $i['sil_day_313']; ?></td>
				<td><?php echo $i['remarks']; ?></td>
				<td align='center'>
					<?php echo anchor("/$table/edit/".$i[$table_id], 'Edit'); ?> |
					<?php echo anchor("/$table/delete/".$i[$table_id], 'Delete'); ?>
				</td>
			</tr>
				<?php 
				}; //end foreach($item as $i)
				}; //end if(!is_array($item))
				?>
								</tbody>
							</table>
						</div>
					</section>
				</section>
				<footer class="footer bg-white b-t">
					
		<div style='height: 25px; margin-top: 15px;'>
			<div style='float: left'><?php echo anchor('/lookup_table', 'Back'); ?></div>
			<div style='float: right'><?php echo anchor("/$table/add", 'Add'); ?></div>
		</div>
				</footer>
			</section>
		</aside>
	</section>
	<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
</section>
