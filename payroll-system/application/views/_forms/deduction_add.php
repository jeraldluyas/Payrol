<div class= "content container">
	<div class= "row"> 
		<h1><?php echo $title;?></h1><hr/>
		<p><?php echo $title_description;?></p><br/>

		<?php if(isset($message)) { ?>
		<div class='warning'>
			<?php echo $message; ?>
		</div>
		<?php }; ?>

		<?php echo form_open_multipart("$table/submit", array('class' => 'form')); ?>

		<?php
		if(isset($id)) { 
			echo form_hidden($id['name'], $id['value']);
		};
		?>
		<div class=''>
			<div class='add_form'>
				<div class='form_row'>
					<?php echo input_text($label['label'], $label['name'], $label['value']); ?>
				</div>
				<div class='form_row'>
					<?php echo input_text($description['label'], $description['name'], $description['value']); ?>
				</div>
				<div class='form_row'>
					<?php echo input_text($value['label'], $value['name'], $value['value']); ?>
				</div>
			</div><!-- div.add_form -->
		</div><!-- div. -->

		<div class='form_actions'>
			<?php echo form_submit('submit', 'Submit'); ?> <button><?php echo anchor("/$table", 'Cancel', array('class' => 'action_link')); ?></button>
		</div>
		<?php echo form_close(); ?>		
	</div>
</div>
<div id='contents'>

</div><!-- end div#contents -->
