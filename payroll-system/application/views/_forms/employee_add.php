<script>

$( document ).ready(function() { 
$('.datepicker-input').datepicker()

});
</script>
<section id="content">
    <section class="vbox">
        <section class="scrollable padder">
            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><i class="fa fa-home"></i> Home</li>
                <li>Employee</li>
                <li><?php echo $title; ?></li>
            </ul>
            <div class="row">
                <div class="wrap-fpanel">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <strong><?php echo $title; ?></strong>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo form_open_multipart("$table/submit", array('class' => 'form')); ?>
                <?php
                if(isset($id)) { 
                echo form_hidden($id['name'], $id['value']);
                };
                ?>
                <!-- ************************ Personal Information Panel Start ************************-->
                <div class="col-sm-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">Personal Details</h4>
                        </div>
                        <div class="panel-body ">
                            <div class="">
                                <label class="control-label">First Name <span class="required" aria-required="true"> *</span></label>
                                <input type="text" class="form-control" name="<?php echo $first_name['name']; ?>" value="<?php echo $first_name['value']; ?>" placeholder="<?php echo $first_name['label']; ?>"
                            </div>
                            <div class="">
                                <label class="control-label">Last Name <span class="required" aria-required="true"> *</span></label>
                                <input type="text" class="form-control" name="<?php echo $last_name['name']; ?>" value="<?php echo $last_name['value']; ?>" placeholder="<?php echo $last_name['label']; ?>" >
                            </div>
                            <div class="">
                                <label class="control-label">Date of birthday <span class="required" aria-required="true"> *</span></label>
                                <input type="text" class="datepicker-input form-control" value="<?php echo $birthdate['value']; ?>" name="<?php echo $birthdate['name']; ?>"  data-date-format="yyyy/mm/dd" placeholder="<?php echo $birthdate['label']; ?>">
                            </div>
                            <div class="">
                                <label class="control-label">City <span class="required" aria-required="true"> *</span></label>
                                <input type="text" class="form-control" name="<?php echo $city['name']; ?>" value="<?php echo $city['value']; ?>" placeholder="<?php echo $city['label']; ?>" >
                            </div>
                            <div class="">
                                <label class="control-label">Mobile Number  <span class="required" aria-required="true"> *</span></label>
                                <input type="text" class="form-control" name="<?php echo $mobile['name']; ?>" value="<?php echo $mobile['value']; ?>" placeholder="<?php echo $mobile['label']; ?>" >
                            </div>
                            <div class="">
                                <label class="control-label">Phone Number  <span class="required" aria-required="true"> *</span></label>
                                <input type="text" class="form-control" name="<?php echo $phone['name']; ?>" value="<?php echo $phone['value']; ?>" placeholder="<?php echo $phone['label']; ?>" >
                            </div>
                            <div class="">
                                <label class="control-label">Email Address  <span class="required" aria-required="true"> *</span></label>
                                <input type="text" class="form-control" name="<?php echo $email['name']; ?>" value="<?php echo $email['value']; ?>" placeholder="<?php echo $email['label']; ?>" >
                            </div>
                            <div class="">
                                <label class="control-label">Civil Status  <span class="required" aria-required="true"> *</span></label>
                                <?php echo form_dropdown($civil_status['name'], $civil_status_list, $civil_status['value'], 'class="form-control"'); ?>
                            </div>

<!--                             <div class="">
                                <label class="control-label">Date of Birth <span class="required" aria-required="true"> *</span></label>
                                <div class="input-group">
                                    <input type="text" name="date_of_birth" value="" class="form-control datepicker" data-format="yyy-mm-dd">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="entypo-calendar"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <label class="control-label">Gender  <span class="required" aria-required="true"> *</span></label>
                                <select name="gender" class="form-control">
                                    <option value="">Select Gender ...</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div> -->
<!--                             <div class="">
                                <label class="control-label">Maratial Status<span class="required" aria-required="true"> *</span></label>
                                <select name="maratial_status" class="form-control">
                                    <option value="">Select Status ...</option>
                                    <option value="Married">Married</option>
                                    <option value="Un-Married">Un-Married</option>
                                    <option value="Widowed">Widowed</option>
                                    <option value="Divorced">Divorced</option>
                                </select>
                            </div> -->
<!--                             <div class="">
                                <label class="control-label">City<span class="required" aria-required="true"> *</span></label>
                                <input type="text" name="city" value="" class="form-control">
                            </div>
                            <div class="">
                                <label class="control-label">Mobile<span class="required" aria-required="true"> *</span></label>
                                <input type="text" name="mobile" value="" class="form-control">
                            </div>
                            <div class="">
                                <label class="control-label">Phone</label>
                                <input type="text" name="phone" value="" class="form-control">
                            </div> -->
                            <!-- <div class="">
                                <label class="control-label">Email <span class="required" aria-required="true"> *</span></label>
                                <input type="email" name="email" value="" class="form-control">
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 margin pull-right">
                    <br>
                    <button id="btn_emp" type="submit" class="btn btn-primary btn-block">Save</button>
                </div>
            </div>

                <!-- ************************ Personal Information Panel End ************************-->       
                <div class="col-sm-6">
                    <!-- ************************** official status column Start  ****************************-->
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">Official Status</h4>
                        </div>
                        <div class="panel-body">
                            <?php echo form_hidden($salary_grade['name'], '1'); ?>
                            <div class="">
                                <label for="field-1" class="control-label">Employee ID <span class="required" aria-required="true">*</span><small id="id_error_msg"></small></label>
                                <?php echo $employee_id['value']; ?>
                                <input type="hidden" class="form-control" name="<?php echo $employee_id['name']; ?>" value="<?php echo $employee_id['value']; ?>" placeholder="<?php echo $employee_id['label']; ?>">
                            </div>
                            <div class="">
                                <label class="control-label">Department <span class="required" aria-required="true">*</span></label>
                                <?php echo form_dropdown( $department['name'], $department_list, $department['value'], 'class="form-control" '); ?>
                            </div>
                            <div class="">
                                <label class="control-label">Position<span class="required" aria-required="true"> *</span></label>
                                <input type="text" class="form-control" name="<?php echo $position['name']; ?>" value="<?php echo $position['value']; ?>" placeholder="<?php echo $position['label']; ?>">
                            </div>
                            <div class="">
                                <label class="control-label">Date Hired<span class="required" aria-required="true"> *</span></label>        
                                <input type="text" class="datepicker-input form-control" value="<?php echo $date_hired['value']; ?>" name="<?php echo $date_hired['name']; ?>"  data-date-format="yyyy/mm/dd" placeholder="<?php echo $date_hired['label']; ?>">
                            </div>
                            <div class="">
                                <label class="control-label">SSS No.<span class="required" aria-required="true"> *</span></label>
                                <input type="text" class="form-control" name="<?php echo $sss_no['name']; ?>" value="<?php echo $sss_no['value']; ?>" placeholder="<?php echo $sss_no['label']; ?>">                                
                            </div>
                            <div class="">
                                <label class="control-label">Philhealth No.<span class="required" aria-required="true"> *</span></label>
                                <input type="text" class="form-control" name="<?php echo $philhealth_no['name']; ?>" value="<?php echo $philhealth_no['value']; ?>" placeholder="<?php echo $philhealth_no['label']; ?>">                                
                            </div>
                            <div class="">
                                <label class="control-label">Pagibig No.<span class="required" aria-required="true"> *</span></label>
                                <input type="text" class="form-control" name="<?php echo $pagibig_no['name']; ?>" value="<?php echo $pagibig_no['value']; ?>" placeholder="<?php echo $pagibig_no['label']; ?>">                                
                            </div>
                            <div class="">
                                <label class="control-label">Tax ID No.<span class="required" aria-required="true"> *</span></label>
                                <input type="text" class="form-control" name="<?php echo $tax_id_no['name']; ?>" value="<?php echo $tax_id_no['value']; ?>" placeholder="<?php echo $tax_id_no['label']; ?>">                                
                            </div>
                            <div class="">
                                <label class="control-label">Birth Certificate<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $birth_cert['name'] ?>" id="<?php echo $birth_cert['name'] ?>" value="1" <?php if($birth_cert['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                            <div class="">
                                <label class="control-label">Baptismal<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $baptismal['name'] ?>" id="<?php echo $baptismal['name'] ?>" value="1" <?php if($baptismal['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                            <div class="">
                                <label class="control-label">Merriage Certification<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $marriage_cert['name'] ?>" id="<?php echo $marriage_cert['name'] ?>" value="1" <?php if($marriage_cert['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                            <div class="">
                                <label class="control-label">Brgy Clearance<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $brgy_clearance['name'] ?>" id="<?php echo $brgy_clearance['name'] ?>" value="1" <?php if($brgy_clearance['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                            <div class="">
                                <label class="control-label">Police Clearance<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $police_clearance['name'] ?>" id="<?php echo $police_clearance['name'] ?>" value="1" <?php if($police_clearance['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                            <div class="">
                                <label class="control-label">NBI<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $nbi['name'] ?>" id="<?php echo $nbi['name'] ?>" value="1" <?php if($nbi['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                            <div class="">
                                <label class="control-label">XRAY<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $xray['name'] ?>" id="<?php echo $xray['name'] ?>" value="1" <?php if($xray['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                            <div class="">
                                <label class="control-label">Result<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $result['name'] ?>" id="<?php echo $result['name'] ?>" value="1" <?php if($result['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                            <div class="">
                                <label class="control-label">sputum<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $sputum['name'] ?>" id="<?php echo $sputum['name'] ?>" value="1" <?php if($sputum['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                            <div class="">
                                <label class="control-label">FECALYSIS<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $fecalysis['name'] ?>" id="<?php echo $fecalysis['name'] ?>" value="1" <?php if($fecalysis['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                            <div class="">
                                <label class="control-label">Health Card<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $health_card['name'] ?>" id="<?php echo $health_card['name'] ?>" value="1" <?php if($health_card['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                            <div class="">
                                <label class="control-label">Drug Test<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $drug_test['name'] ?>" id="<?php echo $drug_test['name'] ?>" value="1" <?php if($drug_test['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                             <div class="">
                                <label class="control-label">Eye Test<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $eye_test['name'] ?>" id="<?php echo $eye_test['name'] ?>" value="1" <?php if($eye_test['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                             <div class="">
                                <label class="control-label">Bio Date<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $bio_date['name'] ?>" id="<?php echo $bio_date['name'] ?>" value="1" <?php if($bio_date['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                             <div class="">
                                <label class="control-label">Resume<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $resume['name'] ?>" id="<?php echo $resume['name'] ?>" value="1" <?php if($resume['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                             <div class="">
                                <label class="control-label">ID Picture<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $id_picture['name'] ?>" id="<?php echo $id_picture['name'] ?>" value="1" <?php if($id_picture['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                             <div class="">
                                <label class="control-label">Info Sheet<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $info_sheet['name'] ?>" id="<?php echo $info_sheet['name'] ?>" value="1" <?php if($info_sheet['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                             <div class="">
                                <label class="control-label">Contract<span class="required" aria-required="true"> *</span></label>
                                <input type="checkbox" name="<?php echo $contract['name'] ?>" id="<?php echo $contract['name'] ?>" value="1" <?php if($contract['value'] == 1){ echo 'checked';}  ?>>
                            </div>
                        </div>
                    </div>
                </div>
        </section>

        <?php echo form_close(); ?>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
</section>
<script src="<?php echo base_url('common/js/datepicker/bootstrap-datepicker.js')?>" ></script>