<?php $this->load->view('side_bar'); ?>
<?php
	if(isset($id)) { 
	  echo form_hidden($id['name'], $id['value']);
	};
	?>
<section id="content">
	<section class="hbox stretch">
		<aside class="aside-md bg-white b-r" id="subNav">
			<div class="wrapper b-b header">
				Admin Menu
			</div>
			<ul class="nav">
				<li class="b-b b-light"><a href="<?php echo base_url('auth'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Admin Accounts</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('/department'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Department</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('salary_grade'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Salary Grade</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('work_on'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work On</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('work_on_rates'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work On rates</a></li>
			</ul>
		</aside>
		<section class="vbox">
			<section class="scrollable padder">
				<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
					<li><i class="fa fa-home"></i> Home</li>
					<li>Admin</li>
					<li>Salary Grade</li>
					<li><?php echo $title;?></li>
				</ul>
				<div class="row">
					<div class="wrap-fpanel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="panel-title">
									<strong><?php echo $title;?></strong>
								</div>
							</div>
						</div>
					</div>
					<p><?php echo $title_description;?></p>
					<br/>
					<?php if(isset($message)) { ?>
					<div class='warning'>
						<?php echo $message; ?>
					</div>
					<?php }; ?>
					<?php echo form_open_multipart("$table/submit", array('class' => 'form')); ?>
					<?php
						if(isset($id)) { 
						  echo form_hidden($id['name'], $id['value']);
						};
						?>
					<!-- ************************ Personal Information Panel End ************************-->       
					<div class="col-sm-12">
						<!-- ************************** official status column Start  ****************************-->
						<div class="panel panel-info">
							<div class="panel-heading">
								<h4 class="panel-title">Salary Grade Information</h4>
							</div>
							<div class="panel-body">
								<p>Please enter the group information below.</p>
								<div class="col-sm-6">
									<p>
										<strong><?php echo $bracket['label']; ?></strong>
										<br>
										<?php echo input_text('', $bracket['name'], $bracket['value']); ?>
									</p>
									<p>
										<strong><?php echo $rate_per_day['label']; ?></strong>
										<br>
										<?php echo input_text('', $rate_per_day['name'], $rate_per_day['value']); ?>
									</p>
									<p>
										<strong><?php echo $months_13['label']; ?></strong>
										<br>
										<?php echo input_text('', $months_13['name'], $months_13['value']); ?>
									</p>
								</div>
								<div class="col-sm-6">
									<p>
										<strong><?php echo $sil_day_314['label']; ?></strong>
										<br>
										<?php echo input_text('', $sil_day_314['name'], $sil_day_314['value']); ?>
									</p>
									<p>
										<strong><?php echo $sil_day_313['label']; ?></strong>
										<br>
										<?php echo input_text('', $sil_day_313['name'], $sil_day_313['value']); ?>
									</p>
									<p>
										<strong><?php echo $remarks['label']; ?></strong>
										<br>
										<?php echo input_text('', $remarks['name'], $remarks['value']); ?>
									</p>
								</div>
							</div>
							<div class='form_row'>
								<!--                             <div class="">
									<label for="field-1" class="control-label">Joining Date <span class="required" aria-required="true">*</span></label>
									<div class="input-group">
									    <input type="text" class="form-control datepicker" name="joining_date" value="" data-format="yyyy/mm/dd">
									    <div class="input-group-addon">
									        <a href="#"><i class="entypo-calendar"></i></a>
									    </div>
									</div>
									</div> -->
							</div>
						</div>
					</div>
					<div class="col-sm-12 margin pull-right">
						<input type="submit" name="submit" class="btn btn-primary btn-block" value="Submit">
					</div>
				</div>
			</section>
		</section>
		<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
	</section>
</section>
<?php echo form_close();?>