
<div class= "content container">
	<div class= "row">
		<h1><?php echo $title;?></h1><hr/>
		<p><?php echo $title_description;?></p><br/>

		<?php if(isset($message)) { ?>
		<div id="infoMessage"><p><?php echo $message;?></p></div>
		<?php }; //end if($message) ?>

		<table cellpadding=0 cellspacing=10 width="100%">
			<tr>
				<th>Label</th>
				<th>Description</th>
				<th>Action</th>
			</tr>
		<?php 
		if(!isset($item)) {
			?>
			<tr>
				<td colspan='3' align='center' style='padding: 15px'><i>No item to display.</i></td>
			</tr>	
			<?php
		} else {
			$table_id = 'id';
			foreach ($item as $i) { ?>
			<tr>
				<td><?php echo $i['label']; ?></td>
				<td><?php echo $i['description']; ?></td>
				<td align='center'>
					<?php echo anchor("/$table/edit/".$i[$table_id], 'Edit'); ?> |
					<?php echo anchor("/$table/delete/".$i[$table_id], 'Delete'); ?>
				</td>
			</tr>
				<?php 
				}; //end foreach($item as $i)
				}; //end if(!is_array($item))
				?>
		</table>

		<div style='height: 25px; margin-top: 50px;'>
			<div style='float: left'><?php echo anchor('/lookup_table', 'Back'); ?></div>
			<div style='float: right'><?php echo anchor("/$table/add", 'Add'); ?></div>
		</div>

	</div>
</div>	
<div id='contents'>
	
</div><!-- div#contents -->
