<section id="content">
	<section class="hbox stretch">
		<aside class="aside-md bg-white b-r" id="subNav">
			<div class="wrapper b-b header">
				Admin Menu
			</div>
			<ul class="nav">
				<li class="b-b b-light"><a href="<?php echo base_url('auth'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Admin Accounts</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('/department'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Department</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('salary_grade'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Salary Grade</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('work_on'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work On</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('work_on_rates'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work On rates</a></li>
<!-- 				<li class="b-b b-light"><a href="<?php echo base_url('deduction'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Deduction</a></li>
 -->			</ul>
		</aside>
		<aside>
			<section class="vbox">
				<section class="scrollable wrapper w-f">
					<section class="panel panel-default">
						<div class="table-responsive">
						
						</div>
					</section>
				</section>
				<footer class="footer bg-white b-t">
				</footer>
			</section>
		</aside>
	</section>
	<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
</section>