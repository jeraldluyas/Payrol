<?php $this->load->view('header'); ?>
<?php $this->load->view('side_bar'); ?>

<section id="content">
<section class="hbox stretch">
  <aside class="aside-md bg-white b-r" id="subNav">
      <div class="wrapper b-b header">
        Admin Menu
      </div>
      <ul class="nav">
        <li class="b-b b-light"><a href="<?php echo base_url('auth'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Admin Accounts</a></li>
        <li class="b-b b-light"><a href="<?php echo base_url('/department'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Department</a></li>
        <li class="b-b b-light"><a href="<?php echo base_url('salary_grade'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Salary Grade</a></li>
        <li class="b-b b-light"><a href="<?php echo base_url('work_on'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work On</a></li>
        <li class="b-b b-light"><a href="<?php echo base_url('work_on_rates'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work On rates</a></li>
      </ul>
    </aside>
  <section class="vbox">
    <section class="scrollable padder">
      <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
        <li><i class="fa fa-home"></i> Home</li>
        <li>Admin</li>
        <li><?php echo lang('deactivate_heading');?></li>
      </ul>
      <div class="row">
        <div class="wrap-fpanel">
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-title">
                <strong><?php echo lang('deactivate_heading');?></strong>

              </div>
            </div>
          </div>
        </div>
		<?php echo form_open("auth/deactivate/".$user->id);?>

        <!-- ************************ Personal Information Panel End ************************-->       
        <div class="col-sm-12">
          <!-- ************************** official status column Start  ****************************-->
          <div class="panel panel-info">
            <div class="panel-heading">
              <h4 class="panel-title"><?php echo lang('deactivate_heading');?></h4>
            </div>
            <div class="panel-body">
              <p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>



  <p>
  	<?php echo lang('deactivate_confirm_y_label', 'confirm');?>
    <input type="radio" name="confirm" value="yes" checked="checked" />
    <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
    <input type="radio" name="confirm" value="no" />
  </p>

                          
<!--                             <div class="">
                                <label for="field-1" class="control-label">Joining Date <span class="required" aria-required="true">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" name="joining_date" value="" data-format="yyyy/mm/dd">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="entypo-calendar"></i></a>
                                    </div>
                                </div>
                              </div> -->
                            </div>
                          </div>
                        </div>
                            <div class="col-sm-12 margin pull-right">
                        <input type="submit" name="submit" class="btn btn-primary btn-block"  value="Submit">
                        </div>
                      </div>
                    </section>
                  </section>
                  <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
                </section>
              </section>





  <?php echo form_hidden($csrf); ?>
  <?php echo form_hidden(array('id'=>$user->id)); ?>


<?php echo form_close();?>

<?php $this->load->view('footer'); ?>
