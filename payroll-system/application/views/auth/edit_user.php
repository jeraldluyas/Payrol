<?php $this->load->view('header'); ?>
<?php $this->load->view('side_bar'); ?>



<section id="content">
  <section class="vbox">
    <section class="scrollable padder">
      <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
        <li><i class="fa fa-home"></i> Home</li>
        <li>Employee</li>
        <li><?php echo $title; ?></li>
      </ul>
      <div class="row">
        <div class="wrap-fpanel">
          <div class="panel panel-default">
            <div class="panel-heading">
              <div class="panel-title">

                <strong><?php echo lang('edit_user_heading');?></strong>

              </div>
            </div>
          </div>
        </div>
        <?php echo form_open(uri_string());?>

        <!-- ************************ Personal Information Panel End ************************-->       
        <div class="col-sm-6">
          <!-- ************************** official status column Start  ****************************-->
          <div class="panel panel-info">
            <div class="panel-heading">
              <h4 class="panel-title">User Details</h4>
            </div>
            <div class="panel-body">
              <p><?php echo lang('edit_user_subheading');?></p>

              <div id="infoMessage"><?php echo $message;?></div>

              <div class="">
                <label for="field-1" class="control-label">First Name:  <span class="required" aria-required="true">*</span><small id="id_error_msg"></small></label>
                <?php echo form_input($first_name);?>
              </div>

              <div class="">
                <label class="control-label">Last Name: <span class="required" aria-required="true">*</span></label>
                <?php echo form_input($last_name);?>
              </div>
              <div class="">
                <label class="control-label">Company :<span class="required" aria-required="true"> *</span></label>
                <?php echo form_input($company);?>
              </div>
              <div class="">
                <label class="control-label">Phone :<span class="required" aria-required="true"> *</span></label>
                <?php echo form_input($phone);?>
              </div>
              <div class="">
                <label class="control-label">Password: (if changing password) <span class="required" aria-required="true"> *</span></label>
                <?php echo form_input($password);?>
              </div>
              <div class="">
                <label class="control-label">Confirm Password: (if changing password)<span class="required" aria-required="true"> *</span></label>
                <?php echo form_input($password_confirm);?>
              </div>
              <?php if ($this->ion_auth->is_admin()): ?>

              <h3><?php echo lang('edit_user_groups_heading');?></h3>
              <?php foreach ($groups as $group):?>
              <label class="checkbox">
                <?php
                $gID=$group['id'];
                $checked = null;
                $item = null;
                foreach($currentGroups as $grp) {
                  if ($gID == $grp->id) {
                    $checked= ' checked="checked"';
                    break;
                  }
                }
                ?>
                <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
                <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
              </label>
            <?php endforeach?>

          <?php endif ?>

          <?php echo form_hidden('id', $user->id);?>
          <?php echo form_hidden($csrf); ?>



<!--                             <div class="">
                                <label for="field-1" class="control-label">Joining Date <span class="required" aria-required="true">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" name="joining_date" value="" data-format="yyyy/mm/dd">
                                    <div class="input-group-addon">
                                        <a href="#"><i class="entypo-calendar"></i></a>
                                    </div>
                                </div>
                              </div> -->
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6 margin pull-right">
                          <input type="submit" name="submit" class="btn btn-primary btn-block" value="Save User">
                        </div>
                      </div>
                    </section>
                  </section>
                  <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
                </section>


                <?php echo form_close();?>

                <?php $this->load->view('footer'); ?>