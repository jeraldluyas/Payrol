<?php $this->load->view('header'); ?>
<?php $this->load->view('side_bar'); ?>
<link rel="stylesheet" href="<?php echo base_url('common/js/datatables/datatables.css'); ?>" type="text/css" cache="false">

<section id="content">
	<section class="hbox stretch">
		<aside class="aside-md bg-white b-r" id="subNav">
			<div class="wrapper b-b header">
				Admin Menu
			</div>
			<ul class="nav">
				<li class="b-b b-light"><a href="<?php echo base_url('admin/accounts'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Admin Accounts</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('/department'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Department</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('salary_grade'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Salary Grade</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('work_on'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work On</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('work_on_rates'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work On rates</a></li>
			</ul>
		</aside>
		<aside>
			<section class="vbox">
				<section class="scrollable wrapper w-f">

						<h4><a href="<?php echo base_url('auth/create_user'); ?>"><i class="fa fa-plus"></i> Add User</a></h4>
			<br>
			
					<section class="panel panel-default">
  <div class="panel-heading">
				Admin List  	
					</div>
				<div class="table-responsive">
					<table class="table table-striped b-t b-light text-sm" id="dataTables-example5">
						<thead>
							<tr role="row">
<th class="col-sm-1 sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="EMP ID: activate to sort column ascending" style="width: 152px;">ID</th>
								
								<th class="col-sm-1 sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="EMP ID: activate to sort column ascending" style="width: 152px;">First Name</th>
								<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Employee: activate to sort column ascending" style="width: 152px;">Last Name</th>
								<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Dept. &amp;gt; Designations: activate to sort column ascending" style="width: 131px;">Email</th>
<!-- 								<th class="show_print sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Email: activate to sort column ascending" style="width: 115px;">Groups</th>
								<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 56px;">Status</th>  -->
								<th class="col-sm-2 hidden-print sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending" style="width: 300px;">Action</th>
							</tr>
						</thead>
						<tbody>

							
						</tbody>
					</table>
				</div>
					</section>
				</section>
				<footer class="footer bg-white b-t">
					<h5 style="font-size:12px;"><a href="<?php echo base_url('auth/create_group'); ?>"><i class="fa fa-plus"></i> Add Groups</a></h5>
			<br>
				</footer>
			</section>
		</aside>
	</section>
	<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
</section>


<?php $this->load->view('footer'); ?>
<script src="<?php echo base_url(); ?>common/asset/js/jquery-1.10.1.min.js"></script>

<!-- DataTables JS -->
<script src="<?php echo base_url(); ?>common/asset/js/jquery.dataTables.min.js"></script>
<Script>
		jQuery(document).ready(function() {
//////////////////////////////////////////////////////////////////////	

      var oTable=  $('#dataTables-example5').dataTable({  
		responsive: true,		
      bProcessing: true,
        bServerSide: true,
		deferRender: true,
		sPaginationType : "full_numbers",
        sAjaxSource: "<?php echo base_url(); ?>common/asset/data/admin-list.php",
		 bFilter: true,
		bLengthChange: true,
        bSort: true,
        bAutoWidth: false,
fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {	
var id= aData[0];
var table="<?php echo base_url()."auth/edit_group/";?>";
var table2="<?php echo base_url()."auth/deactivate/";?>";

$('td:eq(4)', nRow).html(' <a href="'+table+id+'" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil-square-o"></i> Edit</a>	<a href="'+table2+id+'" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top"  data-original-title="Delete"><i class="fa fa-trash-o"></i>Delete</a>');
 
  return nRow;
			},
aoColumns: [ null,null, /* name*/ null, null, { "bSortable": false,}],   
        });	
		
} );
</script>

