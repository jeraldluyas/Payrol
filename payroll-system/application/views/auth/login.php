<!DOCTYPE html><html lang="en" class="bg-dark">
<head>
<meta charset="utf-8"/>
<title>Notebook | Web Application</title>
<meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
<link rel="stylesheet" href="<?php echo base_url('common/css/app.v2.css'); ?>" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url('common/css/font.css'); ?>" type="text/css" cache="false"/>
<!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
</head>
<body>
<section id="content" class="m-t-lg wrapper-md animated fadeInUp">
<div class="container aside-xxl">
  <a class="navbar-brand block" href="<?php base_url(); ?>">KBL Payroll</a><section class="panel panel-default bg-white m-t-lg"><header class="panel-heading text-center"><strong>Sign in</strong></header>
  <?php echo form_open("auth/login", 'class="panel-body wrapper-lg"');?>
    <div id="infoMessage"><?php echo $message;?></div>
    <div class="form-group">
      <label class="control-label">Email</label>
      <input name="identity" value=""  type="email" placeholder="user@example.com" class="form-control input-lg">
    </div>
    <div class="form-group">
      <label class="control-label">Password</label><input name="password" value=""  type="password" id="inputPassword" placeholder="Password" class="form-control input-lg">
    </div>
    <div class="checkbox">

      <label><input type="checkbox" name="remember" value="1" id="remember"> Keep me logged in </label>
    </div>
    <a href="forgot_password" class="pull-right m-t-xs"><small>Forgot password?</small></a><button type="submit" class="btn btn-primary">Sign in</button>
    <div class="line line-dashed">
    </div>
  <?php echo form_close();?>
  </section>
</div>
</section>
<!-- footer -->
<footer id="footer">
<div class="text-center padder">
  <p>
    <small>Payroll System<br>
    &copy; 2015</small>
  </p>
</div>
</footer>
<!-- / footer -->
<script src="<?php base_url('common/js/app.v2.js'); ?>"></script>
<!-- Bootstrap -->
<!-- App -->

</body>
</html>
