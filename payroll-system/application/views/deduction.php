<section id="content">
	<section class="hbox stretch">
		<aside class="aside-md bg-white b-r" id="subNav">
			<div class="wrapper b-b header">
				Admin Menu
			</div>
			<ul class="nav">
				<li class="b-b b-light"><a href="<?php echo base_url('auth'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Admin Accounts</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('/department'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Department</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('salary_grade'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Salary Grade</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('work_on'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work On</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('work_on_rates'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work On rates</a></li>
			</ul>
		</aside>
		<aside>
			<section class="vbox">
				<section class="scrollable wrapper w-f">

						<h4><a href="<?php echo base_url('/deduction/add'); ?>"><i class="fa fa-plus"></i> Add Deduction<a/></h4>
			<br>
	
					<section class="panel panel-default">
						<div class="table-responsive">
							<table class="table table-striped m-b-none">
								<thead>
									<tr>
										<th>Label</th>
										<th>Description</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										if(!isset($item)) {
											?>
									<tr>
										<td colspan='3' align='center' style='padding: 15px'><i>No item to display.</i></td>
									</tr>
									<?php
										} else {
											$table_id = 'id';
											foreach ($item as $i) { ?>
									<tr>
										<td><?php echo $i['label']; ?></td>
										<td><?php echo $i['description']; ?></td>
										<td>
											<a href="<?php echo base_url('deduction/edit/').'/'.$i[$table_id]; ?>" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil-square-o"></i> Edit</a>
											<a href="<?php echo base_url('deduction/delete/').'/'.$i[$table_id]; ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('You are about to delete a record. This cannot be undone. Are you sure?');" data-original-title="Delete"><i class="fa fa-trash-o"></i> Delete</a>


										</td>
									</tr>
									<?php 
										}; //end foreach($item as $i)
										}; //end if(!is_array($item))
										?>
								</tbody>
							</table>
						</div>
					</section>
				</section>
				<footer class="footer bg-white b-t">
					<div style='height: 25px; margin-top: 15px;'>
						<div style='float: left'><?php echo anchor('/admin', 'Back'); ?></div>
					</div>
				</footer>
			</section>
		</aside>
	</section>
	<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
</section>