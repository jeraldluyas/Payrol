<!-- DataTables CSS -->
<link rel="stylesheet" href="<?php echo base_url('common/js/datatables/datatables.css'); ?>" type="text/css" cache="false">
<section id="content">
	<section class="vbox">
		<section class="scrollable padder">
			<ul class="breadcrumb no-border no-radius b-b b-light pull-in">
				<li><i class="fa fa-home"></i> Home</li>
				<li>Employee</li>
				<li>Employee List</li>
			</ul>
			<h4><a href="<?php echo base_url('employee/add'); ?>"><i class="fa fa-plus"></i> Add Employee</a></h4>
			<br>

			
  <div class="row">
                <div class="col-lg-20">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                   Employee List   </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">

                                <table class="table table-striped table-bordered table-hover" id="dataTables-example1">
                                    <thead>
                       		<tr role="row">
								<th class="col-sm-1 sorting_asc" tabindex="0" aria-controls="dataTables-example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="EMP ID: activate to sort column ascending" style="width: 120px; text-align:center;">#</th>
								<th class="col-sm-1 sorting_asc" tabindex="0" aria-controls="dataTables-example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="EMP ID: activate to sort column ascending" style="width: 120px; text-align:center;">EMP ID</th>
								<th class="sorting" tabindex="0" aria-controls="dataTables-example1" rowspan="1" colspan="1" aria-label="Employee: activate to sort column ascending" style="width: 152px; text-align:center;">Employee</th>
								<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1" aria-label="Dept. &amp;gt; Designations: activate to sort column ascending" style="width: 131px; text-align:center;">Department</th>
								<th class="show_print sorting" tabindex="0" aria-controls="dataTables-example1" rowspan="1" colspan="1" aria-label="Email: activate to sort column ascending" style="width: 150px; text-align:center;">Salary Grade</th>
								<th class="sorting" tabindex="0" aria-controls="dataTables-example1" rowspan="1" colspan="1" aria-label="Mobile: activate to sort column ascending" style="width: 150px; text-align:center;">Position</th>
								<th class="sorting" tabindex="0" aria-controls="dataTables-example1" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 56px; text-align:center;">Status</th>
								<th class="col-sm-1 hidden-print sorting" tabindex="0" aria-controls="dataTables-example1" rowspan="1" colspan="1" aria-label="View: activate to sort column ascending" style="width: 73px; text-align:center;">View</th>
								<th class="col-sm-2 hidden-print sorting" tabindex="0" aria-controls="dataTables-example1" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending" style="width: 300px; text-align:center;">Action</th>
							</tr>
                                    </thead>
                                    <tbody> 
                                    </tbody>
                                </table>
									<footer class="panel-footer"></footer>

                            </div>
							</div>
							</div>
								</div>
									</div>
		
			
		</section>
	</section>
	<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
</section>
<!-- jQuery -->
<!-- jQuery -->
<script src="<?php echo base_url('common/asset/js/jquery-1.10.1.min.js'); ?>"></script>
<!-- DataTables JS -->
<script src="<?php echo base_url('common/js/datatables/jquery.dataTables.min.js'); ?>"></script>
<Script>
		jQuery(document).ready(function() {
      var oTable=  $('#dataTables-example1').dataTable({  
		responsive: true,		
      bProcessing: true,
        bServerSide: true,
		deferRender: true,
		sPaginationType : "full_numbers",
        sAjaxSource: "<?php echo base_url(); ?>common/asset/data/employee-list.php",
		 bFilter: true,
		bLengthChange: true,
        bSort: true,
        bAutoWidth: false,
fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {	
var id= aData[0];
var status=aData[5];
var temp_shift="";
var table ="<?php echo base_url()."employee/view/"; ?>";
var table2="<?php echo base_url()."employee/delete/";?>";
var table3="<?php echo base_url()."employee/add_time_record/";?>";
var table4="<?php echo base_url()."employee/edit/";?>";
if(status==1){
temp_shift='<span class="label label-success">Active</span>';
}else{
temp_shift='<span class="label label-success">Inactive</span>';

}


 $('td:eq(6)', nRow).html(temp_shift);


$('td:eq(7)', nRow).html('<a href="'+table+id+'" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><span class="fa fa-list-alt"></span></a>');




 $('td:eq(8)', nRow).html('<a href="'+table3+id+'" class="btn btn-success btn-xs" title="" data-toggle="tooltip" data-placement="top"  data-original-title="Add_time"><i class="fa fa-plus-square"></i> Add Time Record</a> <a href="'+table4+id+'" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil-square-o"></i> Edit</a>	<a href="'+table2+id+'" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top"  data-original-title="Delete"><i class="fa fa-trash-o"></i>Delete</a>');
  return nRow;
			},
aoColumns: [ null,  null,null,  null,  null, null,null,null,null,],   
        });	
});
</script>