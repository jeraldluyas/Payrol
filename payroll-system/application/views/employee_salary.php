<section id="content">
    <section class="vbox">
        <section class="scrollable padder">
            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><i class="fa fa-home"></i> Home</li>
                <li>Employee</li>
                <li><?php echo $title; ?></li>
            </ul>
            <div class="row">
                <div class="wrap-fpanel">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <strong><?php echo $title; ?></strong>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                if(isset($id)) { 
                    echo form_hidden($id['name'], $id['value']);
                };
                ?>
                <!-- ************************ Personal Information Panel End ************************-->       
                <div class="col-sm-12">
                    <!-- ************************** official status column Start  ****************************-->
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">Employee Detail

                            </h4>
                        </div>
                        <div class="panel panel-default">            
                            <!-- main content -->
                            <div class="col-lg-12" style="background: #fff;margin-bottom: 20px;">
                                <div class="row">                            
                                    <div class="col-lg-2 col-sm-2">
                                        <div class="fileinput-new thumbnail" style="width: 144px; height: 125px; margin-top: 14px; margin-left: 16px; background-color: #EBEBEB;">
                                            <img src="<?php echo base_url('common/images/avatar_default.jpg'); ?>" class="img-circle"></a>             
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-sm-1">
                                        &nbsp;
                                    </div>
                                    <div class="col-lg-8 col-sm-8 ">
                                        <div>
                                         <div style="margin-left: 20px;">                                        
                                            <h3><?php echo $last_name['value'].', '. $first_name['value']; ?></h3>
                                            <hr>
                                            <table style="border: none">
                                                <tbody><tr>
                                                    <td><strong>Employee ID</strong></td>
                                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                                    <td><?php echo $employee_id['value']; ?></td>
                                                </tr>                                 
                                                <!-- <tr>
                                                    <td><strong>Salary Grade</strong></td>
                                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                                    <td><?php echo $salary_grade_list[$salary_grade['value']]; ?></td>
                                                </tr> -->
                                                <tr>
                                                    <td><strong>Department</strong></td>
                                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                                    <td><?php echo $department_list[$department['value']] ?></td>
                                                </tr>                                           
                                            </tbody></table>                                                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                
                    </div>
                </div>
            </div>

            <!-- ************************ Personal Information Panel Start ************************-->
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4 class="panel-title">Salary Details</h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="">
                            <label for="field-1" class="col-sm-3 control-label"><strong>Basic Salary :</strong> </label>                    
                            <p class="form-control-static"><?php echo $payslip['gross_total']; ?></p>                    
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">

            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4 class="panel-title">Deduction Details</h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="">
                            <label for="field-1" class="col-sm-3 control-label"><strong>Deduction Total :</strong> </label>                    
                            <p class="form-control-static"><?php echo $payslip['deduction_total']; ?></p>                    
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4 class="panel-title">Add-ons Details</h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="">
                            <label for="field-1" class="col-sm-3 control-label"><strong>Add-ons Total :</strong> </label>                    
                            <p class="form-control-static"><?php echo $payslip['extras_total']; ?></p>                    
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4 class="panel-title">Total Salary Details</h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <dl>
                            <dt>Gross Salary :</dt>
                            <dd><?php echo $payslip['gross_total']; ?></dd>
                        </dl>
                        <dl>
                            <dt>Total Deduction :</dt>
                            <dd><?php echo $payslip['deduction_total']; ?></dd>
                        </dl>
                        <dl>
                            <dt>Net Salary :</dt>
                            <dd><?php echo $payslip['gross_total'] - $payslip['deduction_total']; ?></dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</section>
<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
</section>


    <script type="text/javascript">
        function printDiv(printableArea) {
            var printContents = document.getElementById(printableArea).innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>