
<div class= "content container">
	<div class= "row">
		<h1><?php echo $title;?></h1><hr/>
		<p><?php echo $title_description;?></p><br/>

		<?php if(isset($message)) { ?>
		<div id="infoMessage"><p><?php echo $message;?></p></div>
		<?php }; //end if($message) ?>

		<table cellpadding=0 cellspacing=10 width="100%">
			<tr>
				<th>ID</th>
				<th>Last Name</th>
				<th>First Name</th>
				<th>Salary grade</th>
				<th>Department</th>
				<th>Position</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		<?php 
		if(!isset($item)) {
			?>
			<tr>
				<td colspan='6' align='center' style='padding: 15px'><i>No item to display.</i></td>
			</tr>	
			<?php
		} else {
			$table_id = 'id';
			foreach ($item as $i) { ?>
			<tr>
				<td><?php echo $i['employee_id']; ?></td>
				<td><?php echo $i['last_name']; ?></td>
				<td><?php echo $i['first_name']; ?></td>
				<td><?php echo $i['salary_grade_id']; ?></td>
				<td><?php echo $i['department_id']; ?></td>
				<td><?php echo $i['position']; ?></td>
				<td><?php echo $i['status']; ?></td>
				<td align='center'>
					<?php echo anchor("/$table/edit/".$i[$table_id], 'Edit'); ?> |
					<?php echo anchor("/$table/add_time_record/".$i[$table_id], 'Add Time Record'); ?> |
					<?php echo anchor("/$table/delete/".$i[$table_id], 'Delete'); ?>
				</td>
			</tr>
				<?php 
				}; //end foreach($item as $i)
				}; //end if(!is_array($item))
				?>
		</table>

		<div style='height: 25px; margin-top: 50px;'>
			<div style='float: left'><?php echo anchor('/lookup_table', 'Back'); ?></div>
			<div style='float: right'><?php echo anchor("/$table/add", 'Add'); ?></div>
		</div>

	</div>
</div>	