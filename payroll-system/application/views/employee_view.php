
<script src="<?php echo base_url('common/js/fullcalendar/fullcalendar.min.js'); ?> " cache="false"></script>
<script src="<?php // echo base_url('common/js/fullcalendar/demo.js'); ?>" cache="false"></script>
<script>
$(function(){

    // fullcalendar
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var addDragEvent = function($this){
      // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
      // it doesn't need to have a start or end
      var eventObject = {
        title: $.trim($this.text()), // use the element's text as the event title
        className: $this.attr('class').replace('label','')
    };

      // store the Event Object in the DOM element so we can get to it later
      $this.data('eventObject', eventObject);
      
      // make the event draggable using jQuery UI
      $this.draggable({
      	zIndex: 999,
        revert: true,      // will cause the event to go back to its
        revertDuration: 0  //  original position after the drag
    });
  };
  $('.calendar').each(function() {
  	$(this).fullCalendar({
  		header: {
  			left: 'prev,next',
  			center: 'title',
  			right: 'today,month'
  		},
  		editable: false,
        droppable: false, // this allows things to be dropped onto the calendar !!!
        drop: function(date, allDay) { // this function is called when something is dropped

            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');
            
            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);
            
            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            
            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
            
            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
              // if so, remove the element from the "Draggable Events" list
              $(this).remove();
          }

      }
      ,
      events: [

      			<?php foreach ($report_attendances as $key => $report_attendance) { ?>
      				<?php if(isset($report_attendance['values'])) { ?>
		      		{
		      			title: 'Value : <?php echo $report_attendance["values"]?>',
		      			start: new Date(y, m, <?php echo $report_attendance['day']?>),
		      			className:'bg-primary'
		      		},
		      		<?php } ?>
		      		{
      					title: '<?php echo $work_on[$report_attendance["work_on_id"]] ?>',
      					start: new Date(y, m, <?php echo $report_attendance['day']?>),
      					className:'bg-warning'
      				},
      				<?php if(isset($report_attendance['pay_equals1_hours'])) { ?>
		      			
		      		
      				{
      					title: 'Regular Time: <?php echo $report_attendance["pay_equals1_hours"]?>',
      					start: new Date(y, m, <?php echo $report_attendance['day']?>),
      					className:'bg-primary'
      				},
      				{
      					title: 'Overtime: <?php echo $report_attendance["overtime1_hours"]?>',
      					start: new Date(y, m, <?php echo $report_attendance['day']?>),
      					className:'bg-success'
      				},
      				{
      					title: 'OT(Night): <?php echo $report_attendance["overtime0_hours"]?>',
      					start: new Date(y, m, <?php echo $report_attendance['day']?>),
      					className:'bg-success'
      				},
      				{
      					title: 'Total Hours: <?php echo $report_attendance["pay_equals0_hours"]?>',
      					start: new Date(y, m, <?php echo $report_attendance['day']?>),
      					className:'bg-primary'
      				},

      				<?php } ?>
      				<?php } ?>

      				]
      			});
});
$('#myEvents').on('change', function(e, item){
	addDragEvent($(item));
});

$('#myEvents li').each(function() {
	addDragEvent($(this));
});


});
</script>
<section id="content">
	<section class="vbox">
		<header class="header bg-white b-b b-light">
			<p>


				<?php echo $first_name['value']; ?>'s profile
			</p>
		</header>
		<section class="scrollable">
			<section class="hbox stretch">
				<aside class="aside-lg bg-light lter b-r">
					<section class="vbox">
						<section class="scrollable">
							<div class="wrapper">
								<div class="clearfix m-b">
									<a href="#" class="pull-left thumb m-r"><img src="<?php echo base_url('common/images/avatar_default.jpg'); ?>" class="img-circle"></a>
									<div class="clear">

										<div class="h3 m-t-xs m-b-xs">
											<?php echo $last_name['value'].','.$first_name['value'] ?>
										</div>
										<small class="text-muted"><i class="fa fa-user"></i> IT Developer</small>
									</div>
								</div>
								<div>
									<small class="text-uc text-xs text-muted">Employee ID</small>
									<p>
										<?php echo $employee_id['value']; ?>
									</p>
<!-- 									<small class="text-uc text-xs text-muted">Salary Grade</small>
									<p>
										<?php echo $salary_grade_list[$salary_grade['value']]; ?>
									</p> -->
									<small class="text-uc text-xs text-muted">Department</small>
									<p>
										<?php echo $department_list[$department['value']] ?>
									</p>
									<small class="text-uc text-xs text-muted">birthdate</small>
									<p>
										<?php echo $birthdate['value'] ?>
									</p>
									<small class="text-uc text-xs text-muted">City</small>
									<p>
										<?php echo $city['value'] ?>
									</p>
									<small class="text-uc text-xs text-muted">mobile</small>
									<p>
										<?php echo $mobile['value'] ?>
									</p>
									<small class="text-uc text-xs text-muted">phone</small>
									<p>
										<?php echo $phone['value'] ?>
									</p>
									<small class="text-uc text-xs text-muted">Email</small>
									<p>
										<?php echo $email['value'] ?>
									</p>
									<small class="text-uc text-xs text-muted">Civil Status</small>
									<p>
										<?php echo $civil_status['value'] ?>
									</p>
									<small class="text-uc text-xs text-muted">SSS Number</small>
									<p>
										<?php echo $sss_no['value'] ?>
									</p>
									<small class="text-uc text-xs text-muted">Philhealth Number</small>
									<p>
										<?php echo $philhealth_no['value'] ?>
									</p>
									<small class="text-uc text-xs text-muted">Pagibig Number</small>
									<p>
										<?php echo $pagibig_no['value'] ?>
									</p>
									<small class="text-uc text-xs text-muted">TIN Number</small>
									<p>
										<?php echo $tax_id_no['value'] ?>
									</p><!-- 
									<small class="text-uc text-xs text-muted">has_overtime</small>
									<p>
										<?php if($has_overtime['value'] == 1){
											echo 'Yes';
											} else {
											echo 'No';
											}
										 ?>
									</p>
									<small class="text-uc text-xs text-muted">tardiness</small>
									<p>
										<?php if($tardiness['value'] == 1){
											echo 'Yes';
											}else {
											echo 'No';
											}
										 ?>
									</p>
									<small class="text-uc text-xs text-muted">undertime</small>
									<p>
										<?php if($undertime['value'] == 1){
											echo 'Yes';
											}else {
											echo 'No';	
											}
										 ?>
									</p>
									<small class="text-uc text-xs text-muted">date_hired</small>
									<p>
										<?php echo $date_hired['value'] ?>
									</p> -->
									<div class="line">
									</div>
								</div>
							</div>
						</section>
					</section>
				</aside>
				<aside class="bg-white">
					<section class="vbox">
						<header class="header bg-light bg-gradient">
							<ul class="nav nav-tabs nav-white">
								<li class="active"><a href="#activity" data-toggle="tab">Attendance</a></li>
								<li class=""><a href="#events" data-toggle="tab">Earning Reports</a></li>
							</ul>
						</header>
						<section class="scrollable">
							<div class="tab-content">
								<div class="tab-pane active" id="activity">
									<section class="scrollable wrapper">
										<section class="panel panel-default">
											<header class="panel-heading bg-light"> Calendar </header>
											<div class="calendar" id="calendar"></div>
										</section>
									</section>
								</div>
								<div class="tab-pane" id="events">
									<div class="table-responsive">
										<table class="table table-striped b-t b-light text-sm">
											<thead>
												<tr>
													<th class="th-sortable" data-toggle="class">
														Payroll Period <span class="th-sort"><i class="fa fa-sort-down text"></i><i class="fa fa-sort-up text-active"></i><i class="fa fa-sort"></i></span>
													</th>
													<th>
														Daily Rate
													</th>
													<th>
														Basic Pay
													</th>
													<th>
														13 Month Pay
													</th>
													<th>
														SIL
													</th>
													<th>
														VIEW
													</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($payslips as $key => $payslip) { ?>

												<tr>
													<td><?php echo  date('Y-m-d',strtotime($payslip['date_starts'])) .' to '.date('Y-m-d',strtotime($payslip['date_ends'])) ?></td>
													<td><?php echo  $salary_grade_detail[$salary_grade['value']]['rate_per_day'] ; ?></td>
													<td><?php echo (($payslip['gross_total'] - $payslip['deduction_total']) + $payslip['extras_total']); ?></td>
													<td><?php echo $salary_grade_detail[$salary_grade['value']]['months_13']; ?></td>
													<td><?php echo $salary_grade_detail[$salary_grade['value']]['sil_day_313']; ?></td>

													<td class="hidden-print ">
														<a href="<?php echo base_url('employee/salary_detail/').'/'.$payslip['id']; ?>" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><span class="fa fa-list-alt"></span></a>
													</td>

												</tr>
												<?php }  ?>


											</tbody>
										</table>
									</div>
								</div>
							</section>
						</section>
					</aside>
				</section>
			</section>
		</section>
		<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
	</section>