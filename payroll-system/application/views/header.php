<?php 
$user = $this->ion_auth->user()->row();
$user_id = $user->user_id;
$first_name = $user->first_name;
$last_name = $user->last_name;
$user_id = $user->user_id;
?>
<!DOCTYPE html>
<html lang="en" class="app">
  <head>
    <meta charset="utf-8"/>
    <title>Payroll System</title>
    <meta name="description" content="Payroll System For KBS"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <link rel="stylesheet" href="<?php echo base_url('common/css/app.v2.css'); ?>" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url('common/css/font.css'); ?>" type="text/css" cache="false"/>
    <link rel="stylesheet" href="<?php echo base_url('common/js/fuelux/fuelux.css');?>" type="text/css" cache="false"/>
    <link rel="stylesheet" href="<?php echo base_url('common/js/fullcalendar/fullcalendar.css'); ?>" type="text/css" cache="false"/>
    <link rel="stylesheet" href="<?php echo base_url('common/js/fullcalendar/theme.css'); ?>" type="text/css" cache="false"/>
    <link rel="stylesheet" href="<?php echo base_url('common/js/datepicker/datepicker.css')?>" />
    <link rel="stylesheet" href="<?php echo base_url('common/css/bootstrap-datetimepicker.min.css')?>" />
    <script src="<?php echo base_url('common/js/jquery-1.8.2.min.js'); ?>"></script>

    <script src="<?php echo base_url('common/js/datepicker/bootstrap-datepicker.js')?>" ></script>
    <script src="<?php echo base_url('common/js/bootstrap-datetimepicker.min.js')?>" ></script></head>
    

    <script src="<?php echo base_url('common/js/app.v2.js'); ?>"></script>
    <!-- Bootstrap -->
<!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
  </head>
  <body>
    <section class="vbox">
      <header class="bg-dark dk header navbar navbar-fixed-top-xs">
        <div class="navbar-header aside-md">
          <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav"><i class="fa fa-bars"></i></a><a href="#" class="navbar-brand" data-toggle="fullscreen"><img src="<?php echo base_url('common/images/logo.png'); ?>" class="m-r-sm">KBL Payroll</a><a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user"><i class="fa fa-cog"></i></a>
        </div>
        <ul class="nav navbar-nav navbar-right hidden-xs nav-user">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="thumb-sm avatar pull-left"><img src="<?php echo base_url('common/images/avatar_default.jpg'); ?>"></span> <?php echo $first_name.'.'.$last_name; ?> <b class="caret"></b></a>
            <ul class="dropdown-menu animated fadeInRight">
              <span class="arrow top"></span>
              <li><a href="<?php echo base_url('auth/edit_user/')."/".$user_id; ?>">Profile</a></li>
              <li class="divider"></li> 
              <li><a href="<?php echo base_url('auth/logout'); ?>" >Logout</a></li>
            </ul>
          </li>
        </ul>
      </header>