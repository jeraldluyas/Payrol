<link rel="stylesheet" href="<?php echo base_url('common/js/datatables/datatables.css'); ?>" type="text/css" cache="false">
<script>

$( document ).ready(function() { 
$('.datepicker-input').datepicker()

});
</script>
<section id="content">
    <section class="hbox stretch">
    <aside class="aside-md bg-white b-r hidden-print" id="subNav">
        <div class="wrapper b-b header">
            Menu
        </div>
        <ul class="nav">
            <li class="b-b b-light"><a href="<?php echo base_url('reports/regular_employee'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Employee</a></li>
            <li class="b-b b-light"><a href="<?php echo base_url('reports/payslip'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Payslip</a></li>
        </ul>
    </aside>
        <aside>
            <section class="vbox">
                <section class="scrollable wrapper w-f">
                    
                <section class="panel panel-heading" style="    background-color: #f5f5f5;">

                <div class="col-sm-12">
                            <div class="col-sm-6">
                                <input type="text" class="datepicker-input form-control" value="" name="date_start"  data-date-format="yyyy/mm/dd" placeholder="Date Start" aria-controls="dataTables-example1">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="datepicker-input form-control" value="" name="date_end"  data-date-format="yyyy/mm/dd" placeholder="Date End" aria-controls="dataTables-example1">
                            </div>
                        </div>
                        <br><br>
                </section>
                    <section class="panel panel-default">
                        <div class="panel-heading">
                   Payslip 
                    <button class="btn btn-sm btn-default" style="float:right;" onclick="myFunction()">Print All</button>
                    <div style="clear:both"></div>
                   </div>
                        <div class="table-responsive">
                            <table class="table table-striped m-b-none" id="dataTables-example1">
                                <thead>
                                    <tr>
                                        <th>Payslip ID</th> 
                                        <th>Employee Name</th>
                                        <th>Payroll Period</th>
                                        <th>Daily Rate</th>
                                        <th>Basic Pay</th>
                                        <th>13 Month Pay</th>
                                        <th>SIL</th>
                                        <th class="hidden-print">VIEW</th>
                                    </tr>  
                                 
                                </thead>
                                           <tbody> 
                                    </tbody>
                                </table>
                                    <footer class="panel-footer"></footer>
                                <tbody>
        
                                </tbody>
                            </table>
                        </div>
                    </section>
                </section>
                <footer class="footer bg-white b-t">
                    
        <div style='height: 25px; margin-top: 15px;'>
            <div style='float: left'><?php echo anchor('/lookup_table', 'Back'); ?></div>
            <!-- <div style='float: right'><?php echo anchor("/$table/add", 'Add'); ?></div> -->
        </div>
                </footer>
            </section>
        </aside>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
</section>

<script src="<?php echo base_url('common/js/datepicker/bootstrap-datepicker.js')?>" ></script>
<!-- jQuery -->
<script src="<?php echo base_url(); ?>common/asset/js/jquery-1.10.1.min.js"></script>

<!-- DataTables JS -->
<script src="<?php echo base_url(); ?>common/asset/js/jquery.dataTables.min.js"></script>
<Script>
        jQuery(document).ready(function() {
//////////////////////////////////////////////////////////////////////  

      var oTable=  $('#dataTables-example1').dataTable({  
        responsive: true,       
        bProcessing: true,
        bServerSide: true,
        deferRender: true,
        sPaginationType : "full_numbers",
        sAjaxSource: "<?php echo base_url(); ?>common/asset/data/payslip.php",
         bFilter: true,
        bLengthChange: true,
        bSort: true,
        bAutoWidth: false,
        data_starts: true,
        date_ends: false,
        
fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {  
var id= aData[0];
var dateAr = aData[2].split('00:00:00');
var date_start = dateAr[0].split('-');
var dateAr = aData[3].split('00:00:00');
var date_end = dateAr[0].split('-');
var table="<?php echo base_url()."employee/salary_detail/";?>";
console.log(aData);
$('td:eq(2)', nRow).html(date_start[0]+"-"+date_start[1]+"-"+date_start[2]+"to"+date_end[0]+"-"+date_end[1]+"-"+date_end[2]);
$('td:eq(4)', nRow).html(""+aData[5]);
$('td:eq(3)', nRow).html(""+aData[6]);
$('td:eq(5)', nRow).html(""+aData[7]);
$('td:eq(7)', nRow).html('<a href="'+table+''+id+'" class="btn btn-info btn-xs hidden-print" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><span class="fa fa-list-alt"></span></a>');

  return nRow;
            },
aoColumns: [ /* stud_id*/ null, /* name*/ null,  null,null,null,null,null, { "bSortable": false,}],   
        }); 
        
} );
</script>
<script>
function myFunction() {
    window.print();
}
</script>
