<section id="content">
<section class="hbox stretch">
    <aside class="aside-md bg-white b-r" id="subNav">
        <div class="wrapper b-b header">
            Menu
        </div>
        <ul class="nav">
            <li class="b-b b-light"><a href="<?php echo base_url('reports/regular_employee'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Employee</a></li>
            <li class="b-b b-light"><a href="<?php echo base_url('reports/payslip'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Payslip</a></li>
        </ul>
    </aside>
    <section class="vbox">
        <section class="scrollable wrapper">
            <div class="row">
                <div class="col-lg-12">
                     <?php echo form_open_multipart( base_url('/reports/regular_employee'), array('class' => 'form')); ?>
                        <div class="col-lg-4">
                            <?php echo form_dropdown('month', $months, $month, 'class="form-control m-b"'); ?>

                        </div>
                        <div class="col-lg-4">
                            <?php echo form_dropdown('year', $years, $year, 'class="form-control m-b"'); ?>
                        </div>
                        <div class="col-lg-4">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </form>

                    <br><br><br>
                </div>
                <div class="col-lg-12">

                    <section class="panel panel-default">

                        <div class="wizard clearfix">
                            <ul class="steps">
                                <li data-target="#step1" class="active">Weekly Attendance</li>
                                <li data-target="#step2" class="">Gross Pay</li>
                                <li data-target="#step3" class="">Net Pay</li>
                            </ul>
                            <div class="actions">
                                <button type="button" class="btn btn-default btn-xs btn-prev" disabled="disabled">Prev</button><button type="button" class="btn btn-default btn-xs btn-next" data-last="Next">Next</button>
                            </div>
                        </div>
                        <div class="step-content">
                            <div class="step-pane active" id="step1">
                                <div class="col-sm-4">
                                    <div class="table-responsive"  style="    width: 100%;
                                        position: relative;
                                        padding-top: 37px;
                                        margin-bottom: 30px;">
                                        <table class="table table-striped m-b-none" style="    width: 100%;
                                            overflow-x: auto;
                                            overflow-y: auto;
                                            border: 1px solid #d5d5d5;
                                            overflow: auto;
                                            display: block;">
                                            <tr>
                                                <th  style="text-align:center; vertical-align:bottom; height:124px;">EMP ID</th>
                                                <th  style="text-align:center; vertical-align:bottom;height:124px;    width: 100%;">NAME</th>
                                            </tr>
                                            <?php $counter = array(); ?>
                                            <?php foreach ($employee_lists as $key => $employee_list) {
                                                $counter[] = $employee_list['id'];
                                                ?>
                                            <tr >
                                                <td><?php echo $employee_list['employee_id']; ?></td>
                                                <td><?php echo $employee_list['last_name'].', '.$employee_list['first_name'];?>
                                            </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="table-responsive"  style="    width: 100%;
                                        position: relative;
                                        padding-top: 37px;
                                        margin-bottom: 30px;">
                                        <table class="table table-striped m-b-none" style="    width: 100%;
                                            overflow-x: auto;
                                            overflow-y: auto;
                                            border: 1px solid #d5d5d5;
                                            overflow: auto;
                                            display: block;">
                                            <thead>
                                                <tr>
                                                    <?php $countermonth=30;?>
                                                </tr>
                                                <tr>
                                                    <!-- forloop -->
                                                    <?php for ($i=1; $i <= $day ; $i++) {  ?>
                                                    <th colspan="4" align="center">DAY(<?php echo $i; ?>)</th>
                                                    <?php } ?>
                                                    <!-- forloop -->
                                                </tr>
                                                <tr>
                                                    <!-- forloop -->
                                                    <?php for ($i=1; $i <= $day ; $i++) {  ?>
                                                    <th colspan="4" align="center">HOURS</th>
                                                    <?php } ?>
                                                    <!-- forloop -->
                                                    <!-- forloop -->
                                                </tr>
                                                <tr>
                                                    <!-- forloop -->
                                                    <?php for ($i=1; $i <= $day ; $i++) {  ?>
                                                    <th align="center">RT</th>
                                                    <th align="center">OT</th>
                                                    <th align="center">PCS</th>
                                                    <th align="center">Night Diff</th>
                                                    <?php } ?>
                                                    <!-- forloop -->
                                                </tr>
                                                <tr>
                                                    <?php for ($i=1; $i <= $day ; $i++) {  ?>
                                                    <th align="center" colspan="4"></th>

                                                    <?php } ?>
                                                    <!-- forloop -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($employee_lists as $key => $employee_list) { ?>
                                                    <tr>
                                                    <?php foreach ($report_attendances as $key => $report_attendance) { ?>
                                                        <?php $counter_for_employee=0; ?>
                                                    
                                                        <?php for ($i=1; $i <= $day ; $i++) {  ?>
                                                            <?php if($report_attendance['emp_id'] == $employee_list['id']) { ?>
                                                                <?php if($i == $report_attendance['day']) { ?>
                                                                <td><?php echo $report_attendance['pay_equals1_hours'] ?></td>
                                                                <td><?php echo $report_attendance['pay_equals0_hours'] ?></td>
                                                                <td><?php echo $report_attendance['overtime1_hours']?></td>
                                                                <td><?php echo $report_attendance['overtime0_hours'] ?></td>
                                                                <?php }else { ?>
                                                                <td>0</td>
                                                                <td>0</td>
                                                                <td>0</td>
                                                                <td>0</td>
                                                                <?php  } ?>                                                         
                                                            <?php } else { ?>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <td>0</td>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    
                                                    <?php  $counter_for_employee++;?>
                                                    <?php } ?>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"> </div>
                            </div>
                            <div class="step-pane" id="step2">
                                 <div class="col-sm-4">
                                    <div class="table-responsive"  style="    width: 100%;
                                        position: relative;
                                        padding-top: 37px;
                                        margin-bottom: 30px;">
                                        <table class="table table-striped m-b-none" style="    width: 100%;
                                            overflow-x: auto;
                                            overflow-y: auto;
                                            border: 1px solid #d5d5d5;
                                            overflow: auto;
                                            display: block;">
                                            <tr>
                                                <th  style="text-align:center; vertical-align:bottom; height:124px;">EMP ID</th>
                                                <th  style="text-align:center; vertical-align:bottom;height:124px;    width: 100%;">NAME</th>
                                            </tr>
                                            <?php $counter = array(); ?>
                                            <?php foreach ($employee_lists as $key => $employee_list) {
                                                $counter[] = $employee_list['id'];
                                                ?>
                                            <tr >
                                                <td><?php echo $employee_list['employee_id']; ?></td>
                                                <td><?php echo $employee_list['last_name'].', '.$employee_list['first_name'];?>
                                            </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="table-responsive"  style="    width: 100%;
                                        position: relative;
                                        padding-top: 37px;
                                        margin-bottom: 30px;">
                                        <table class="table table-striped m-b-none" style="    width: 100%;
                                            overflow-x: auto;
                                            overflow-y: auto;
                                            border: 1px solid #d5d5d5;
                                            overflow: auto;
                                            display: block;">
                                            <thead>
                                                <tr>
                                                    <th rowspan="3" align="center">DAILY RATE</th>
                                                    <th  rowspan="3" align="center">BASIC PAY</th>
                                                    <th colspan="3" rowspan="2" align="center">REGULAR TIME</th>
                                                    <th colspan="3" rowspan="2" align="center">7TH DAY/SUNDAY</th>
                                                    <th colspan="3" rowspan="2" align="center">WORKED LEGAL HOLIDAY</th>
                                                    <th colspan="3" rowspan="2" align="center">PAID LEGAL HOLIDAY</th>
                                                    <th colspan="3" rowspan="2" align="center">SPECIAL HOLIDAY</th>
                                                    <th colspan="3" rowspan="2" align="center">TOTAL</th>
                                                    <th colspan="15">OVERTIME</th>
                                                    <th colspan="3" rowspan="3" align="center"> TOTAL OT PAY</th>
                                                    <th  rowspan="3" align="center"> GROSS PAY</th>
                                                </tr>
                                                <tr>
                                                    <th colspan="3" align="center">REGULAR TIME</th>
                                                    <th colspan="3" align="center">7TH DAY/SUNDAY</th>
                                                    <th colspan="3" align="center">LEGAL HOLIDAY</th>
                                                    <th colspan="3" align="center">SPECIAL HOLIDAY</th>
                                                    <th colspan="3" align="center">TOTAL</th>
                                                </tr>
                                                <tr>
                                                    <th align="center">DAY</th>
                                                    <th align="center">NIGHT</th>
                                                    <th align="center">PCS</th>
                                                     <th align="center">DAY</th>
                                                    <th align="center">NIGHT</th>
                                                    <th align="center">PCS</th>
                                                     <th align="center">DAY</th>
                                                    <th align="center">NIGHT</th>
                                                    <th align="center">PCS</th>
                                                     <th align="center">DAY</th>
                                                    <th align="center">NIGHT</th>
                                                    <th align="center">PCS</th>
                                                    <th align="center">DAY</th>
                                                    <th align="center">NIGHT</th>
                                                    <th align="center">PCS</th>
                                                    <th align="center">DAY</th>
                                                    <th align="center">NIGHT</th>
                                                    <th align="center">PCS</th>
                                                     <th align="center">DAY</th>
                                                    <th align="center">NIGHT</th>
                                                    <th align="center">PCS</th>
                                                     <th align="center">DAY</th>
                                                    <th align="center">NIGHT</th>
                                                    <th align="center">PCS</th>
                                                     <th align="center">DAY</th>
                                                    <th align="center">NIGHT</th>
                                                    <th align="center">PCS</th>
                                                     <th align="center">DAY</th>
                                                    <th align="center">NIGHT</th>
                                                    <th align="center">PCS</th>
                                                     <th align="center">DAY</th>
                                                    <th align="center">NIGHT</th>
                                                    <th align="center">PCS</th>
                                                </tr>
                                                
                                                <tr>
                                                    <th>&nbsp;</th>
                                                    <th align="center">Amt.</th>
                                                    <th align="center" colspan="37"></th>

                                                    
                                                </tr>
                                                
                                                
                                                <?php foreach ($employee_lists as $key => $employee_list) { ?>
                                                    <tr>
                                                        <?php foreach ($report_attendances as $key => $report_attendance) { ?>
                                                            <?php for ($i=1; $i < 40 ; $i++) {  ?>
                                                                <td>0</td>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div style="clear:both;"> </div>
                            </div>
                            <div class="step-pane" id="step3">
                               <div class="col-sm-4">
                                    <div class="table-responsive"  style="    width: 100%;
                                        position: relative;
                                        padding-top: 37px;
                                        margin-bottom: 30px;">
                                        <table class="table table-striped m-b-none" style="    width: 100%;
                                            overflow-x: auto;
                                            overflow-y: auto;
                                            border: 1px solid #d5d5d5;
                                            overflow: auto;
                                            display: block;">
                                            <tr>
                                                <th  style="text-align:center; vertical-align:bottom; height:154px;">EMP ID</th>
                                                <th  style="text-align:center; vertical-align:bottom;height:154px;    width: 100%;">NAME</th>
                                            </tr>
                                            <?php $counter = array(); ?>
                                            <?php foreach ($employee_lists as $key => $employee_list) {
                                                $counter[] = $employee_list['id'];
                                                ?>
                                            <tr >
                                                <td><?php echo $employee_list['employee_id']; ?></td>
                                                <td><?php echo $employee_list['last_name'].', '.$employee_list['first_name'];?>
                                            </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="table-responsive"  style="    width: 100%;
                                        position: relative;
                                        padding-top: 37px;
                                        margin-bottom: 30px;">
                                        <table class="table table-striped m-b-none" style="    width: 100%;
                                            overflow-x: auto;
                                            overflow-y: auto;
                                            border: 1px solid #d5d5d5;
                                            overflow: auto;
                                            display: block;">
                                            <thead>
                                                <tr>
                                                    <th colspan="4">ADD</th>
                                                    <th colspan="8">DEDUCTION</th>
                                                    <th rowspan="4" >NET PAY</th>
                                                </tr>
                                                <tr>
                                                    <th rowspan="3">SIL (2nd Payout)</th>
                                                    <th rowspan="2">Salary Adjustment</th>
                                                    <th rowspan="2">Allowance</th>
                                                    <th rowspan="3">PPE's Deposit Refund</th>
                                                    <th colspan="2">LOANS</th>
                                                    <th colspan="3">CONTRIBUTION</th>
                                                    <th rowspan="2">ATM</th>
                                                    <th rowspan="2">CASH ADVANCE</th>
                                                    <th rowspan="2">PPE's DEPOSIT</th>
                                                </tr>
                                                <tr>
                                                    <th>SSS</th>
                                                    <th>HDMF</th>
                                                    <th>PHIC</th>
                                                    <th>SSS</th>
                                                    <th>HDMF</th>
                                                </tr>
                                                <tr>
                                                    <th>RT/OT</th>
                                                    <th>75/day</th>
                                                    <th colspan="8">MONTH YEAR</th>
                                                </tr>
                                                <tr>
                                                    <th>Amt.</th>
                                                    <th>Amt.</th>
                                                    <th>Amt.</th>
                                                    <th>Amt.</th>
                                                    <th>Amt.</th>
                                                    <th>Amt.</th>
                                                    <th>Amt.</th>
                                                    <th>Amt.</th>
                                                    <th>Amt.</th>
                                                    <th>Amt.</th>
                                                    <th>Amt.</th>
                                                    <th>Amt.</th>
                                                    <th>Amt.</th>
                                                </tr>
                                            </thead>
                                                <?php foreach ($employee_lists as $key => $employee_list) { ?>
                                                    <tr>
                                                        <?php foreach ($report_attendances as $key => $report_attendance) { ?>
                                                            <?php for ($i=1; $i < 14 ; $i++) {  ?>
                                                                <td>0</td>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div style="clear:both;"> </div>
                        </div>
                </div>
                </section>
            </div>
            </div>
        </section>
    </section>
    <a href="file:///C:/Users/robby/Desktop/12345/note/rep.html#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
</section>
