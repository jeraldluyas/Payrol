<section>
	<section class="hbox stretch">
		<!-- .aside -->
		<aside class="bg-light lter b-r aside-md hidden-print" id="nav">
			<section class="vbox">
				<section class="w-f scrollable">
					<div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="5px" data-color="#333333">
						<!-- nav -->
						<nav class="nav-primary hidden-xs">
							<ul class="nav">
								<li><a href="<?php echo base_url(''); ?>"><i class="fa fa-dashboard icon"><b class="bg-danger"></b></i><span>Home</span></a></li>
								<li>
									<a href="#layout"><i class="fa fa-users icon"><b class="bg-warning"></b></i><span class="pull-right"><i class="fa fa-angle-down text"></i><i class="fa fa-angle-up text-active"></i></span><span>Employee</span></a>
									<ul class="nav lt">
										<li><a href="<?php echo base_url('employee/'); ?>"><i class="fa fa-angle-right"></i><span>Employee List</span></a></li>
										<li><a href="<?php echo base_url('employee/add'); ?>"><i class="fa fa-angle-right"></i><span>Add Employee</span></a></li>
									</ul>
								</li>
								<li><a href="<?php echo base_url('reports/'); ?>"><i class="fa fa-book icon"><b class="bg-info"></b></i><span>Reports</span></a></li>
							<?php if($this->ion_auth->in_group('admin') AND $this->ion_auth->is_admin()) { ?>

								<li><a href="<?php echo base_url('admin/'); ?>"><i class="fa fa-gear icon"><b class="bg-info"></b></i><span>Admin</span></a></li>
							<?php } ?>
							</ul>
						</nav>
						<!-- / nav -->
					</div>
				</section>
				<footer class="footer lt hidden-xs b-t b-light">
					<div id="invite" class="dropup">
						<section class="dropdown-menu on aside-md m-l-n">
							<section class="panel bg-white">
								<header class="panel-heading b-b b-light"> John <i class="fa fa-circle text-success"></i></header>
								<div class="panel-body animated fadeInRight">
									<p class="text-sm">
										No contacts in your lists.
									</p>
									<p>
										<a href="#" class="btn btn-sm btn-facebook"><i class="fa fa-fw fa-facebook"></i> Invite from Facebook</a>
									</p>
								</div>
							</section>
						</section>
					</div>
					<a href="#nav" data-toggle="class:nav-xs" class="pull-right btn btn-sm btn-default btn-icon"><i class="fa fa-angle-left text"></i><i class="fa fa-angle-right text-active"></i></a>
				</footer>
			</section>
		</aside>
					<!-- /.aside -->