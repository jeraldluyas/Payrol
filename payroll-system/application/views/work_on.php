<link rel="stylesheet" href="<?php echo base_url('common/js/datatables/datatables.css'); ?>" type="text/css" cache="false">
<section id="content">
	<section class="hbox stretch">
		<aside class="aside-md bg-white b-r" id="subNav">
			<div class="wrapper b-b header">
				Admin Menu
			</div>
			<ul class="nav">
				<li class="b-b b-light"><a href="<?php echo base_url('auth'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Admin Accounts</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('/department'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Department</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('salary_grade'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Salary Grade</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('work_on'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work On</a></li>
				<li class="b-b b-light"><a href="<?php echo base_url('work_on_rates'); ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work On rates</a></li>
			</ul>
		</aside>
		<aside>
			<section class="vbox">
				<section class="scrollable wrapper w-f">
					<section class="panel panel-default">
					   <div class="panel-heading">
                   Work On List  		
				   </div>
						<div class="table-responsive">
							<table class="table table-striped m-b-none" id="dataTables-example1">
								<thead>
									<tr>
									<th>ID</th>	
				<th>Label</th>
				<th>Description</th>
				<th>Action</th>
									</tr>
								</thead>
								           <tbody> 
                                    </tbody>
                                </table>
									<footer class="panel-footer"></footer>
								<tbody>
		
								</tbody>
							</table>
						</div>
					</section>
				</section>
				<footer class="footer bg-white b-t">
					
		<div style='height: 25px; margin-top: 15px;'>
			<div style='float: left'><?php echo anchor('/lookup_table', 'Back'); ?></div>
			<div style='float: right'><?php echo anchor("/$table/add", 'Add'); ?></div>
		</div>
				</footer>
			</section>
		</aside>
	</section>
	<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
</section>

<!-- jQuery -->
<script src="<?php echo base_url(); ?>common/asset/js/jquery-1.10.1.min.js"></script>

<!-- DataTables JS -->
<script src="<?php echo base_url(); ?>common/asset/js/jquery.dataTables.min.js"></script>
<Script>
		jQuery(document).ready(function() {
//////////////////////////////////////////////////////////////////////	

      var oTable=  $('#dataTables-example1').dataTable({  
		responsive: true,		
      bProcessing: true,
        bServerSide: true,
		deferRender: true,
		sPaginationType : "full_numbers",
        sAjaxSource: "<?php echo base_url(); ?>common/asset/data/work-on-list.php",
		 bFilter: true,
		bLengthChange: true,
        bSort: true,
        bAutoWidth: false,
fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {	
var id= aData[0];
var table="<?php echo base_url().$table."/edit/";?>";
var table2="<?php echo base_url().$table."/delete/";?>";

$('td:eq(3)', nRow).html(' <a href="'+table+id+'" class="btn btn-primary btn-xs" title="" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil-square-o"></i> Edit</a>	<a href="'+table2+id+'" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top"  data-original-title="Delete"><i class="fa fa-trash-o"></i>Delete</a>');

  return nRow;
			},
aoColumns: [ /* stud_id*/ null, /* name*/ null,  null, { "bSortable": false,}],   
        });	
		
} );
</script>