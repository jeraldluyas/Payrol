<?php
 	error_reporting(0);
require_once("connection.php");

	/*
	 * Script:    DataTables server-side script for PHP and MySQL
	 * Copyright: 2010 - Allan Jardine
	 * License:   GPL v2 or BSD (3-point)
	 */
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */
	
	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
	   
	 ///courseId, courseCode, courseTitle, sem, course, status, year
	$aColumns = array('id','employee_id', 'fullnames','brackets', 'labels','positions','status');
	
	/* Indexed column (used for fast and accurate table cardinality) */
	$sIndexColumn = "id";
	
	/* DB table to use */
	$sTable = "employee";
	
	/* 
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".mysqli_real_escape_string($conn, $_GET['iDisplayStart'] ).", ".
			mysqli_real_escape_string($conn, $_GET['iDisplayLength'] );
	}
	
	
	/*
	 * Ordering
	 */
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
		{
			if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
			{
				$sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
				 	".mysqli_real_escape_string($conn, $_GET['sSortDir_'.$i] ) .", ";
			}
		}
		
		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" )
		{
			$sOrder = "";
		}
	}
	
	
	/* 
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	 

	 
	 $sWhere="";
if ( rtrim($_GET['sSearch']) != "" && $_GET['sSearch'] != " " )
{
    $aWords = preg_split('/\s+/', $_GET['sSearch']);
    $sWhere = "WHERE ( ";
     
    for ( $j=0 ; $j<count($aWords) ; $j++ )
    {
        if ( $aWords[$j] != "" )
        {                       


$sWhere .= " emp.employee_id LIKE '%". mysqli_real_escape_string($conn, $aWords[$j]) ."%' OR ";
$sWhere .= " CONCAT(emp.first_name, ' ', emp.last_name) LIKE '%". mysqli_real_escape_string($conn, $aWords[$j] 	 ) ."%' OR ";

$sWhere .= " emp.position  LIKE '%". mysqli_real_escape_string($conn, $aWords[$j] 	 ) ."%' OR ";
				

        }
    }
    $sWhere = substr_replace( $sWhere, "", -4 );
    $sWhere .= ' )';
}


		/* Individual column filtering */
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{
		if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
		{
			if ( $sWhere == "" )
			{
				$sWhere = "";
			}
			else
			{
				$sWhere .= "";
			}
		                $sWhere .= "emp.fullnames LIKE '%".mysqli_real_escape_string($conn, $aWords[$j] )."%' OR ";
				   $sWhere .= "emp.employee_id LIKE '%".mysqli_real_escape_string($conn, $aWords[$j] )."%' OR ";
				      $sWhere .="sg.brackets LIKE '%".mysqli_real_escape_string($conn, $aWords[$j] )."%' OR ";
					     $sWhere .= "emp.positions LIKE '%".mysqli_real_escape_string($conn, $aWords[$j] )."%' OR ";
		}
	}
	
		/*
	 * SQL queries
	 * Get data to display
	 */
	 
	$sQuery = "SELECT SQL_CALC_FOUND_ROWS CONCAT(emp.first_name, ' ', emp.last_name) as fullnames,emp.id ,emp.status,sg.bracket as brackets,dept.label as labels,emp.position as positions,emp.employee_id FROM  payroll.employee as emp inner join payroll.department as dept on dept.id=emp.department_id inner join payroll.salary_grade as sg on sg.id=emp.salary_grade_id 
		$sWhere
		$sOrder
		$sLimit
	";

	$rResult = mysqli_query($conn, $sQuery ) or die(mysqli_error());
	
	/* Data set length after filtering */
	$sQuery = "
		SELECT FOUND_ROWS()
	";
	$rResultFilterTotal = mysqli_query( $conn, $sQuery ) or die(mysqli_error());
	$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
	$iFilteredTotal = $aResultFilterTotal[0];
	/////////////////////////////////////////////////////////////////////

		/* Total data set length */
	$sQuery = "
		SELECT COUNT(".$sIndexColumn.")
		FROM   $sTable
		
	";
	//////////////////////////////////////////////////////////
	////enf of validation of user

	//////////////////////////////////////////////////////////////////////////
	$rResultTotal = mysqli_query($conn, $sQuery ) or die(mysqli_error());
	$aResultTotal = mysqli_fetch_array($rResultTotal);
	$iTotal = $aResultTotal[0];
	
	
	/*
	 * Output
	 */
	$output = array(
		"sEcho" => intval($_GET['sEcho']),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData" => array()
	);
	
	while ( $aRow = mysqli_fetch_array( $rResult ) )
	{
		$row = array();
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( $aColumns[$i] == "version" )
			{
				/* Special output formatting for 'version' column */
				$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
			
			}
			else if ( $aColumns[$i] != ' ' )
			{
				/* General output */
				$row[] = $aRow[ $aColumns[$i] ];
			}
		}
		$row[] .= "";
$row[] .= "";	
$row[] .= "";		
		$output['aaData'][] = $row;
	
	}

	echo json_encode( $output);
?>