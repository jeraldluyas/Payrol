<?php
 	error_reporting(0);
require_once("connection.php");

	/*
	 * Script:    DataTables server-side script for PHP and MySQL
	 * Copyright: 2010 - Allan Jardine
	 * License:   GPL v2 or BSD (3-point)
	 */
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */
	
	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
	   
	 ///courseId, courseCode, courseTitle, sem, course, status, year
	$aColumns = array('id','fullnames', 'date_starts','date_ends','rate_per_day', 'gross_total','sil_day_313', 'months_13');
	
	/* Indexed column (used for fast and accurate table cardinality) */
	$sIndexColumn = "id";
	
	/* DB table to use */
	$sTable = "employee";
	
	/* 
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".mysqli_real_escape_string($conn, $_GET['iDisplayStart'] ).", ".
			mysqli_real_escape_string($conn, $_GET['iDisplayLength'] );
	}
	
	
	/*
	 * Ordering
	 */
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
		{
			if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
			{
				$sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
				 	".mysqli_real_escape_string($conn, $_GET['sSortDir_'.$i] ) .", ";
			}
		}
		
		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" )
		{
			$sOrder = "";
		}
	}
	
	
	/* 
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	 

	 
	 $sWhere="";
if ( rtrim($_GET['sSearch']) != "" && $_GET['sSearch'] != " " )
{
    $aWords = preg_split('/\s+/', $_GET['sSearch']);
    $sWhere = "WHERE ( ";
     
    for ( $j=0 ; $j<count($aWords) ; $j++ )
    {
        if ( $aWords[$j] != "" )
        {                       


$sWhere .= " employee.employee_id LIKE '%". mysqli_real_escape_string($conn, $aWords[$j]) ."%' OR ";
$sWhere .= " CONCAT(employee.first_name, ' ', employee.last_name) LIKE '%". mysqli_real_escape_string($conn, $aWords[$j] 	 ) ."%' ";

        }
    }
    $sWhere = substr_replace( $sWhere, "", -4 );
    $sWhere .= ' )';
}


		/* Individual column filtering */
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{
		if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
		{
			if ( $sWhere == "" )
			{
				$sWhere = "";
			}
			else
			{
				$sWhere .= "";
			}
		              $sWhere .= "emp.fullnames LIKE '%".mysqli_real_escape_string($conn, $aWords[$j] )."%' ";
		}
	}
	
		/*
	 * SQL queries
	 * Get data to display
	 */
	 
	$sQuery = "SELECT SQL_CALC_FOUND_ROWS temp_payslip.id, employee.employee_id, CONCAT(employee.first_name, ' ', employee.last_name) as fullnames, salary_grade.rate_per_day, salary_grade.months_13, salary_grade.sil_day_313, temp_payslip.gross_total, temp_payslip.deduction_total, temp_payslip.extras_total, gross_for_pieces.value1, gross_for_pieces.value2, gross_for_pieces.date_starts, gross_for_pieces.date_ends, gross_for_piece_for_day.day, gross_for_piece_for_day.values, gross_for_day.day, gross_for_day.pay_equals1_hours, gross_for_day.pay_equals1, gross_for_day.pay_equals0_hours, gross_for_day.pay_equals0, gross_for_day.overtime1_hours, gross_for_day.overtime1, gross_for_day.overtime0_hours, gross_for_day.pay_equals0, gross_for_day.gross_day, gross_for_day.date_starts, gross_for_day.date_ends, gross_for_day.date_ends, gross_for_day.total_pay_for_day, gross_for_day.time_starts, gross_for_day.time_ends FROM temp_payslip INNER JOIN employee ON employee.id = temp_payslip.emp_id LEFT JOIN gross_for_pieces ON temp_payslip.id = gross_for_pieces.temp_payslip_id LEFT JOIN gross_for_piece_for_day ON gross_for_pieces.id = gross_for_piece_for_day.gross_for_pieces_id INNER JOIN gross_for_day ON temp_payslip.id = gross_for_day.temp_payslip_id INNER JOIN salary_grade ON employee.salary_grade_id = salary_grade.id group by temp_payslip.id

		$sWhere
		$sOrder
		$sLimit
	"; 
	$rResult = mysqli_query($conn, $sQuery ) or die(mysqli_error());
	
	/* Data set length after filtering */
	$sQuery = "
		SELECT FOUND_ROWS()
	";
	$rResultFilterTotal = mysqli_query( $conn, $sQuery ) or die(mysqli_error());
	$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
	$iFilteredTotal = $aResultFilterTotal[0];
	/////////////////////////////////////////////////////////////////////

		/* Total data set length */
	$sQuery = "
		SELECT COUNT(".$sIndexColumn.")
		FROM   $sTable
		
	";
	//////////////////////////////////////////////////////////
	////enf of validation of user

	//////////////////////////////////////////////////////////////////////////
	$rResultTotal = mysqli_query($conn, $sQuery ) or die(mysqli_error());
	$aResultTotal = mysqli_fetch_array($rResultTotal);
	$iTotal = $aResultTotal[0];
	
	
	/*
	 * Output
	 */
	$output = array(
		"sEcho" => intval($_GET['sEcho']),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData" => array()
	);
	
	while ( $aRow = mysqli_fetch_array( $rResult ) )
	{
		$row = array();
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( $aColumns[$i] == "version" )
			{
				/* Special output formatting for 'version' column */
				$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
			
			}
			else if ( $aColumns[$i] != ' ' )
			{
				/* General output */
				$row[] = $aRow[ $aColumns[$i] ];
			}
		}
		$row[] .= "";
$row[] .= "";	
$row[] .= "";		
		$output['aaData'][] = $row;
	
	}

	echo json_encode( $output);
?>