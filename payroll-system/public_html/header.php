<!DOCTYPE html>
<html lang="en">
<head>
<title>Payroll</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php echo base_url('common/css/bootstrap.css')?>">
<?php basename('common/css/style.css')?>
<link rel="stylesheet" href="<?php echo base_url('common/css/common.css')?>">
<link rel="stylesheet" href="<?php echo base_url('common/css/style.css')?>">
<link rel="stylesheet" href="<?php echo base_url('common/css/bootstrap.min.css')?>" />
<link rel="stylesheet" href="<?php echo base_url('common/js/datepicker/datepicker.css')?>" />
<link rel="stylesheet" href="<?php echo base_url('common/css/bootstrap-datetimepicker.min.css')?>" />



<script src="<?php echo base_url('common/js/jquery-1.8.2.min.js')?>" ></script>
<script src="<?php echo base_url('common/js/datepicker/bootstrap-datepicker.js')?>" ></script>
<script src="<?php echo base_url('common/js/bootstrap-datetimepicker.min.js')?>" ></script></head>
<script src="<?php echo base_url('common/js/bootstrap.js')?>" ></script></head>

<body>
  <header>
  <div class="container"> 
    <div class="row">   
      
      <nav class="navbar navbar-default">
  <div class="container-fluid">

    <div class="navbar-header"><img src="<?php echo base_url('common/img/logo.png')?>"></div>
  </div>
</nav>
      <ul class="nav nav-tabs">
        <li><a href="<?php echo base_url('search/'); ?>">Search</a></li>
        <li><a href="<?php echo base_url('employee/search'); ?>">Employee Details</a></li>
        <li><a href="<?php echo base_url('employee/'); ?>">Date Time Record</a></li>
        <li><a href="<?php echo base_url('report/'); ?>">Weekly Reports</a></li>
        <li><a href="<?php echo base_url('reports/'); ?>">Individual Reports</a></li>
      </ul>   </div>
  </div>
  </header>